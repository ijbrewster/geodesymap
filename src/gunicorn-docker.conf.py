import os

script_dir = os.path.dirname(__file__)

wsgi_app = "GeodesyGPS:app"
chdir = script_dir

# user = "nobody"
# group = "nobody"
bind = [
    "0.0.0.0:5000",
]
workers = 4
threads = 50
worker_connections = 202
accesslog = "-"
errorlog = "-"
preload_app = True
timeout = 300
