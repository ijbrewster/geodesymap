"""Load GPS data from local pfiles into database"""

import psycopg2
import os
import glob
from datetime import datetime

if __name__ == "__main__":
    DATA_DIR = "/geodesy/data/pfiles"
    files = glob.glob(os.path.join(DATA_DIR, "*.pfiles"))
    SQL = """
    INSERT INTO gps_data
    (station,lon,lat,alt,lat_e,lon_e,alt_e,read_date,rapid)
    VALUES
    (%s,%s,%s,%s,%s,%s,%s,%s,%s)
    ON CONFLICT (station,read_date) DO UPDATE
    SET rapid=EXCLUDED.rapid,
        lat=EXCLUDED.lat,
        lon=EXCLUDED.lon,
        alt=EXCLUDED.alt,
        lat_e=EXCLUDED.lat_e,
        lon_e=EXCLUDED.lon_e,
        alt_e=EXCLUDED.alt_e
    """
    conn = psycopg2.connect(
        host="akutan.snap.uaf.edu", database="geodesy", user="geodesy"
    )
    cur = conn.cursor()

    for file in files:
        station = file.split("/")[-1].split(".")[0]

        cur.execute("SELECT id FROM stations WHERE name=%s", (station,))
        station_id = cur.fetchone()
        if station_id is None:
            continue
        else:
            print("Updating", station)
            station_id = station_id[0]

        with open(file, "r") as f:
            for row in f:
                row = row.split()
                try:
                    line_date = datetime.strptime(row[12].split("/")[-1][:7], "%y%b%d")
                except ValueError:
                    print("***ERROR***")
                    continue  # Can't get the date for this point. Move on.

                rapid = True if "AVOx08" in row[12] else False

                # Fixup the longitude to REAL values
                row[3] = float(row[3])
                if row[3] > 180:
                    row[3] -= 360

                # Don't think this one will happen, but check
                if row[3] < -180:
                    row[3] += 360

                args = (station_id, *row[3:9], line_date, rapid)
                cur.execute(SQL, args)

    conn.commit()
    conn.close()
    print("Done")
