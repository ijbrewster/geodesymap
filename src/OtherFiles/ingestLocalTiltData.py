import glob
import math
import gzip
import os
import time

from collections import defaultdict
from datetime import date, datetime, timedelta, timezone
from concurrent.futures import ThreadPoolExecutor

import psycopg2


class db_cursor:
    def __init__(self, cursor_factory=None, user="geodesy", password="G30dE$yU@F"):
        self._cursor_factory = cursor_factory
        self._user = user
        self._password = password

    def __enter__(self):
        self._conn = psycopg2.connect(
            host="localhost",
            database="geodesy",
            cursor_factory=self._cursor_factory,
            user=self._user,
            password=self._password,
        )
        self._cursor = self._conn.cursor()
        return self._cursor

    def __exit__(self, *args, **kwargs):
        try:
            self._conn.rollback()
        except AttributeError:
            return  # No connection
        self._conn.close()


def refresh_materialized_view(view):
    SQL = f"REFRESH MATERIALIZED VIEW CONCURRENTLY {view}"
    with db_cursor(user="israel") as cursor:
        print(f"{datetime.now()} - Refreshing view {view}")
        cursor.execute(SQL)
        cursor.connection.commit()
        print(f"{datetime.now()} - View {view} Refreshed")


def recreate_view(view, time_range):
    create_sql = f"""
    CREATE MATERIALIZED VIEW {view}_new AS SELECT
        time_bucket('{time_range}',read_time) as readtime,
        station,
        first(tilt_x,read_time) as tilt_x,
        first(tilt_y,read_time) as tilt_y,
        first(rot_x,read_time) as rot_x,
        first(rot_y,read_time) as rot_y,
        first(temperature,read_time) as temperature
    FROM tilt_data
    GROUP BY readtime,station
    """
    # We need a certain amount of randomness for the index name
    # so we don't conflict with the curent table.
    index_name = f"{view}_station_readtime_idx_{random.randint(0,99)}"
    index_sql = f"CREATE UNIQUE INDEX {index_name} ON {view}_new(station,readtime)"
    with db_cursor(user="israel") as cursor:
        print(f"{datetime.now()} - Creating new {view}")
        cursor.execute(create_sql)
        cursor.execute(f"GRANT SELECT on {view}_new TO geodesy")
        print(f"{datetime.now()} - {view}_new created. Creating index")
        cursor.execute(index_sql)
        print(f"{datetime.now()} - Index Created. Moving into place.")
        cursor.execute(f"ALTER MATERIALIZED VIEW {view} RENAME TO {view}_old")
        cursor.execute(f"ALTER MATERIALIZED VIEW {view}_new RENAME TO {view}")
        cursor.connection.commit()
        cursor.execute(f"DROP MATERIALIZED VIEW {view}_old")
        cursor.connection.commit()


if __name__ == "__main__":
    t_start = time.time()
    SQL = """INSERT INTO tilt_data
    (station,read_time,tilt_x,tilt_y,temperature,rot_x,rot_y)
    VALUES
    (%s,%s,%s,%s,%s,%s,%s)
    ON CONFLICT (station,read_time) DO UPDATE
    SET tilt_x=excluded.tilt_x, tilt_y=excluded.tilt_y,
    temperature=excluded.temperature
    """

    GET_ORIENTATION = """SELECT DISTINCT ON (station)
        y_orientation*-1,
        station
    FROM tilt_orientation
    WHERE y_orientation IS NOT NULL
    ORDER BY station, seton DESC;
    """
    station_data_starts = {}
    with db_cursor() as cursor:
        cursor.execute(
            "SELECT stations.name, data_doy, data_year,"
            "stations.id "
            "FROM tilt_last_data "
            "RIGHT JOIN stations "
            "ON tilt_last_data.station=stations.name"
        )
        for entry in cursor:
            sta_year = entry[2] or 2000
            sta_doy = entry[1] or 2
            sta_start_year = sta_year if sta_doy != 1 else sta_year - 1
            sta_start_doy = (
                sta_doy - 1
                if sta_doy != 1
                else date(sta_start_year, 12, 31).timetuple().tm_yday
            )
            station_data_starts[entry[0]] = (sta_start_year, sta_start_doy, entry[3])

        cursor.execute(GET_ORIENTATION)
        orientations = {x[1]: x[0] for x in cursor}

    # DEBUG
    # DATA_DIR = '/Users/israel/tilt_data'
    DATA_DIR = "/data/geodesy"

    # get a list of stations for which we have data
    stations = [
        x for x in os.listdir(DATA_DIR) if os.path.isdir(os.path.join(DATA_DIR, x))
    ]
    new_records = []
    new_ends = defaultdict(lambda: datetime.min.replace(tzinfo=timezone.utc))
    for station in stations:
        t_sta = time.time()
        sta_files = os.path.join(DATA_DIR, station, "*.gz")
        try:
            start_year, start_doy, station_id = station_data_starts[station]
        except KeyError:
            # If we have no data for this station, get it all (back to 2000 at least)
            start_year = 2000
            start_doy = 1

        station_orientation = math.radians(orientations.get(station_id, 0))
        COS = math.cos(station_orientation)
        SIN = math.sin(station_orientation)

        sta_start = date(start_year, 1, 1) + timedelta(days=start_doy)
        print(station, sta_start)
        for file in sorted(glob.glob(sta_files)):
            file_date = file.split("/")[-1].split("_")[2]
            file_date = datetime.strptime(file_date, "%Y%j0000").date()
            if file_date < sta_start:
                continue  # this file is too old, we already have it.

            with gzip.open(file, "rb") as data_file:
                for record in data_file:
                    record = record.decode("utf-8").split()
                    r_date = record[0]
                    r_time = record[1]
                    tilt_y = float(record[3])
                    if tilt_y > 360 or tilt_y < -360:
                        print("Invalid y value detected:", tilt_y)
                        continue
                    tilt_x = float(record[5])
                    if tilt_x > 360 or tilt_x < -360:
                        print("Invalid x value detected:", tilt_x)
                        continue

                    tilt_x_cor = COS * tilt_x - SIN * tilt_y
                    tilt_y_coor = SIN * tilt_x + COS * tilt_y

                    if (
                        -999 > tilt_x_cor
                        or tilt_x_cor > 999
                        or -999 > tilt_y_coor
                        or tilt_y_coor > 999
                    ):
                        raise ValueError(
                            f"Value out of range! {tilt_x_cor},{tilt_y_coor},{file},{record},{station}"
                        )

                    r_temp = record[7]
                    read_date = datetime.strptime(
                        f"{r_date}T{r_time}-0000", "%Y-%m-%dT%H:%M:%S.%f%z"
                    )
                    db_record = (
                        station_id,
                        read_date,
                        tilt_x,
                        tilt_y,
                        r_temp,
                        tilt_x_cor,
                        tilt_y_coor,
                    )
                    if read_date > new_ends[station]:
                        new_ends[station] = read_date

                    new_records.append(db_record)
        print("Processed station in", time.time() - t_sta)
    print(
        "Processed all stations in", time.time() - t_start, len(new_records), "records"
    )

    LAST_UPDATE_SQL = """INSERT INTO tilt_last_data
    (station, data_doy, data_year)
    VALUES
    (%s,%s,%s)
    ON CONFLICT (station) DO UPDATE SET
    data_doy=excluded.data_doy,
    data_year=excluded.data_year
    """

    # Process last dates for each station into station, doy, year VALUES
    last_reads = [
        (sta, dte.timetuple().tm_yday, dte.year) for sta, dte in new_ends.items()
    ]

    with db_cursor(user="israel", password=None) as cursor:
        cursor.execute("ALTER TABLE tilt_data DISABLE TRIGGER USER;")
        cursor.executemany(SQL, new_records)
        cursor.executemany(LAST_UPDATE_SQL, last_reads)
        cursor.connection.commit()

    with ThreadPoolExecutor() as executor:
        for view in ["tilt_1_hour", "tilt_8_hour"]:
            executor.submit(refresh_materialized_view, view)
        executor.submit(recreate_view, "tilt_5_min_sample", "5 minute")

    print("Complete in", time.time() - t_start)
