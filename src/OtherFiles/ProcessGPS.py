import os
import time

import pandas
import paramiko
import psycopg2


class DBCursor:
    def __init__(self, cursor_factory=None, user="geodesy", password=None):
        self._cursor_factory = cursor_factory
        self._user = user
        self._password = password

    def __enter__(self):
        self._conn = psycopg2.connect(
            host="akutan.snap.uaf.edu",
            database="geodesy",
            cursor_factory=self._cursor_factory,
            user=self._user,
            password=self._password,
        )
        self._cursor = self._conn.cursor()
        return self._cursor

    def __exit__(self, *args, **kwargs):
        try:
            self._conn.rollback()
        except AttributeError:
            return  # No connection
        self._conn.close()


# ssh_key = '/Users/israel/.ssh/id_rsa'
ssh_key = "/root/.ssh/id_rsa"

FILE_DIR = "/gps/standard-solutions/timeseries"
col_names = [
    "dec_year",
    "dE",
    "dN",
    "dU",
    "Se",
    "Sn",
    "Su",
    "Ren",
    "Reu",
    "Rnu",
    "sec_past_J2000",
    "year",
    "month",
    "day",
    "hour",
    "minute",
    "second",
    "soln",
]

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

ssh.connect("katmai.giseis.alaska.edu", username="avo_harvester", key_filename=ssh_key)

sftp = paramiko.SFTPClient.from_transport(ssh.get_transport())

query_args = []

with DBCursor(password="G30dE$yU@F") as cursor:
    cursor.execute("SELECT id,name FROM stations")
    station_ids = {x[1]: x[0] for x in cursor}

all_data = None

for file_name in sftp.listdir(FILE_DIR):
    if not file_name.endswith(".series"):
        continue

    try:
        station = station_ids[file_name.split("_")[0]]
    except KeyError:
        continue  # Station not in DB

    file = sftp.open(os.path.join(FILE_DIR, file_name))
    print("Retrieving file", file_name)
    gps_df = pandas.read_csv(file, sep="\s+", names=col_names)
    print("File received and parsed")
    file.close()
    gps_df["read_date"] = pandas.to_datetime(
        gps_df[["year", "month", "day", "hour", "minute", "second"]], utc=True
    )
    gps_df["rapid"] = gps_df["soln"].str.contains("_fid")
    gps_df["station"] = station
    if all_data is None:
        all_data = gps_df[
            [
                "station",
                "dE",
                "dN",
                "dU",
                "Se",
                "Sn",
                "Su",
                "Ren",
                "Reu",
                "Rnu",
                "read_date",
                "rapid",
            ]
        ]
    else:
        all_data = pandas.concat(
            [
                all_data,
                gps_df[
                    [
                        "station",
                        "dE",
                        "dN",
                        "dU",
                        "Se",
                        "Sn",
                        "Su",
                        "Ren",
                        "Reu",
                        "Rnu",
                        "read_date",
                        "rapid",
                    ]
                ],
            ],
            ignore_index=True,
        )
#        all_data = all_data.append(gps_df[['station', 'dE', 'dN', 'dU', 'Se', 'Sn', 'Su',
#                                           'Ren', 'Reu', 'Rnu', 'read_date', 'rapid']],
#                                   ignore_index = True)
ssh.close()

print(len(all_data.index), "Records to insert/update")
query_args = all_data.to_numpy().flatten().tolist()
arg_string = ["(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"] * all_data.index.size
arg_string = """,
""".join(
    arg_string
)

SQL = """
INSERT INTO gps_data (
    station,
    de,
    dn,
    dalt,
    se,
    sn,
    su,
    ren,
    reu,
    rnu,
    read_date,
    rapid
)
VALUES
"""
SQL += arg_string
SQL += """
ON CONFLICT (station, read_date) DO UPDATE
SET de=EXCLUDED.de, dn=EXCLUDED.dn, dalt=EXCLUDED.dalt,
    se=EXCLUDED.se, sn=EXCLUDED.sn, su=EXCLUDED.su,
    ren=EXCLUDED.ren, reu=EXCLUDED.reu, rnu=EXCLUDED.rnu,
    rapid=EXCLUDED.rapid
"""

query_start = time.time()
print("Begining UPSERT")
with DBCursor(password="G30dE$yU@F") as cursor:
    # cursor.execute("TRUNCATE gps_data")
    cursor.execute(SQL, query_args)
    cursor.connection.commit()

print("UPSERT COMPLETE after", time.time() - query_start)
