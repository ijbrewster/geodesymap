import os

script_dir = os.path.dirname(__file__)

wsgi_app = "GeodesyGPS:app"
chdir = script_dir

user = "geodesy"
group = "www-data"
bind = ["0.0.0.0:5050", "unix:/run/geodesy/gunicorn.sock"]
raw_env = ["SCRIPT_NAME=/geodesy"]
workers = 4
threads = 50
worker_connections = 202
accesslog = "/var/log/geodesy/access.log"
errorlog = "/var/log/geodesy/error.log"
capture_output = True
preload_app = True
timeout = 300
