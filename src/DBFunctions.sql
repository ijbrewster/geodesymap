-- Column location of stations postgis ST_POINT

-- MATERIALIZED VIEWS

-- tilt_1_hour
 SELECT time_bucket('01:00:00'::interval, tilt_data.read_time) AS readtime,
    tilt_data.station,
    avg(tilt_data.tilt_x::double precision) AS tilt_x,
    avg(tilt_data.tilt_y::double precision) AS tilt_y,
    avg(tilt_data.rot_x::double precision) AS rot_x,
    avg(tilt_data.rot_y::double precision) AS rot_y,
    avg(tilt_data.temperature::double precision) AS temperature
   FROM tilt_data
  GROUP BY (time_bucket('01:00:00'::interval, tilt_data.read_time)), tilt_data.station;

  "tilt_1_hour_station_readtime_idx" UNIQUE, btree (station, readtime)

-- tilt_5_min_sample
SELECT time_bucket('00:05:00'::interval, tilt_data.read_time) AS readtime,
    tilt_data.station,
    first(tilt_data.tilt_x, tilt_data.read_time) AS tilt_x,
    first(tilt_data.tilt_y, tilt_data.read_time) AS tilt_y,
    first(tilt_data.rot_x, tilt_data.read_time) AS rot_x,
    first(tilt_data.rot_y, tilt_data.read_time) AS rot_y,
    first(tilt_data.temperature, tilt_data.read_time) AS temperature
   FROM tilt_data
  GROUP BY (time_bucket('00:05:00'::interval, tilt_data.read_time)), tilt_data.station;

  "tilt_5_min_sample_station_readtime_idx_56" UNIQUE, btree (station, readtime)

-- tilt_8_hour
SELECT time_bucket('08:00:00'::interval, tilt_data.read_time) AS readtime,
    tilt_data.station,
    avg(tilt_data.tilt_x::double precision) AS tilt_x,
    avg(tilt_data.tilt_y::double precision) AS tilt_y,
    avg(tilt_data.rot_x::double precision) AS rot_x,
    avg(tilt_data.rot_y::double precision) AS rot_y,
    avg(tilt_data.temperature::double precision) AS temperature
   FROM tilt_data
  GROUP BY (time_bucket('08:00:00'::interval, tilt_data.read_time)), tilt_data.station;

   "tilt_8_hour_station_time_idx" UNIQUE, btree (station, readtime)



-- Apply using:
-- CREATE TRIGGER station_location_default
-- BEFORE INSERT OR UPDATE
-- ON stations
-- FOR EACH ROW
-- WHEN (NEW.latitude IS NOT NULL and NEW.longitude IS NOT NULL)
-- EXECUTE PROCEDURE set_station_location()

CREATE OR REPLACE FUNCTION set_station_location()
    RETURNS trigger AS
$func$
BEGIN
NEW.location:=ST_SetSRID(ST_MakePoint(NEW.longitude,NEW.latitude,NEW.elevation),4326);
RETURN NEW;

END
$func$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION set_reading_location()
    RETURNS trigger AS
$func$
BEGIN
NEW.loc:=ST_SetSRID(ST_MakePoint(NEW.lon,NEW.lat),4326);
RETURN NEW;

END
$func$ LANGUAGE plpgsql;


-- Function to calculate the "corrected" rotation of the plot
-- CREATE TRIGGER rotate_tilt_data BEFORE UPDATE OR INSERT ON tilt_data
-- FOR EACH ROW WHEN (NEW.tilt_x IS NOT NULL and NEW.tilt_y IS NOT NULL)
-- EXECUTE PROCEDURE correct_rotation();
CREATE OR REPLACE function correct_rotation()
RETURNS trigger
AS $func$
import math
from decimal import Decimal

tilt_x=TD["new"]['tilt_x']
tilt_y=TD["new"]["tilt_y"]
station=TD["new"]["station"]
read_date=TD["new"]["read_time"]

#Get the rotation values
SQL="""
SELECT y_orientation*-1 as orientation --Use the negitive here to rotate the clockwise
FROM tilt_orientation
WHERE station=$1
AND seton<=$2
AND y_orientation IS NOT NULL
ORDER BY seton DESC
LIMIT 1
"""
plan=plpy.prepare(SQL,["uuid","timestamp"])
res=plpy.execute(plan,[station,read_date],1)
if res:
    rot=math.radians(res[0]['orientation'])
else:
    rot=0

COS=Decimal(math.cos(rot))
SIN=Decimal(math.sin(rot))
tilt_x_coor=COS*tilt_x-SIN*tilt_y
tilt_y_coor=SIN*tilt_x+COS*tilt_y
TD["new"]["rot_x"]=round(tilt_x_coor,3)
TD["new"]["rot_y"]=round(tilt_y_coor,3)

return "MODIFY"
$func$ LANGUAGE plpython3u;


-- Function to calculate the "corrected" rotation of the plot
-- CREATE TRIGGER rotate_tilt_data BEFORE UPDATE OR INSERT ON tilt_data
-- FOR EACH ROW WHEN (NEW.tilt_x IS NOT NULL and NEW.tilt_y IS NOT NULL)
-- EXECUTE PROCEDURE correct_rotation();
CREATE OR REPLACE function get_cor_rot(tilt_x numeric, tilt_y numeric, rot numeric)
RETURNS numeric[]
AS $func$
import math
from decimal import Decimal

float_rot=float(rot)
COS=Decimal(math.cos(float_rot))
SIN=Decimal(math.sin(float_rot))
tilt_x_coor=COS*tilt_x-SIN*tilt_y
tilt_y_coor=SIN*tilt_x+COS*tilt_y
rot_x=round(tilt_x_coor,3)
rot_y=round(tilt_y_coor,3)

return [rot_x,rot_y]
$func$ LANGUAGE plpython3u;


-- Procedure to create a "new" tilt_data table with updated corrected angles based on
-- revised tilt_orientations
CREATE TABLE new_tilt AS
SELECT
        id,
        station,
        read_time,
        tilt_x,
        tilt_y,
        temperature,
        round( ((cos_y*tilt_x)-(sin_y*tilt_y))::numeric,3)::numeric(6,3) as rot_x,
        round( ((sin_y*tilt_x)+(cos_y*tilt_y))::numeric,3)::numeric(6,3) as rot_y
FROM
        (SELECT distinct on (tilt_data.station,tilt_data.read_time)
                tilt_data.id,
                tilt_data.station,
                read_time,
                tilt_x,
                tilt_y,
                temperature,
                sin(radians(coalesce(y_orientation,0)*-1)) as sin_y,
                cos(radians(coalesce(y_orientation,0)*-1)) as cos_y
         FROM tilt_data
         LEFT JOIN tilt_orientation
         ON tilt_orientation.station=tilt_data.station
         AND tilt_orientation.seton<=tilt_data.read_time
         ORDER BY tilt_data.station, tilt_data.read_time, tilt_orientation.seton DESC) s1;

ALTER TABLE new_tilt ADD PRIMARY KEY (id);
ALTER TABLE new_tilt ALTER COLUMN station SET not null;
ALTER TABLE new_tilt ALTER COLUMN id SET DEFAULT uuid_generate_v1();
CREATE UNIQUE INDEX new_data_station_time_idx ON new_tilt(station,read_time);
CREATE INDEX new_data_station_idx ON new_tilt(station);
ALTER TABLE new_tilt ADD CONSTRAINT new_tilt_data_station_fkey FOREIGN KEY (station) REFERENCES stations(id);
