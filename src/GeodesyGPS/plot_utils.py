import math
import os

from datetime import timedelta

import flask
import plotly.graph_objects as go
import plotly.io as pio

from dateutil.parser import parse
from PIL import Image


def scalePolarPlot(date_data, polar_data, dfrom, dto):
    """Given an array of dates and a plotly formatted data object for a
    matching polar plot, figure out 1) what segment of the polar data
    fall within our date range, 2) where to place the first point such
    that it is in the center of the plot, and 3) the max radius.

    NOTE: polar_data will be modified in-place to only include data within the
    specified date range, shifted to place the first data point at or around r=0.

    ARGUMENTS
    ---------
    date_data: list | tuple
        list of dates associated with the points in the polar plot.
    polar_data: dict
        A plot data dictionary formated as expected by plotly
    dfrom: date
        The date where we want to start showing data.
    dto: date
        the date where we want to stop showing data.

    RETURNS
    -------
    r_max: float
        The maximum r-value of the filtered data.

    """
    # Use the two hours prior to the start time to average the zero point
    # We do this rather than just using the first point because the first
    # point may be significantly offset.
    avg_from = dfrom - timedelta(hours=2)

    full_r = polar_data["r"]
    full_theta = polar_data["theta"]

    # Find data indexes for desired date range
    start_idx = None  # Index at which data within the desired range starts
    avg_start_idx = None  # Start index for the "averaging" data to find zero.
    end_idx = None  # Index at which desired data ends.

    for idx, dte in enumerate(date_data):
        dte = parse(dte)
        if avg_start_idx is None and dte > avg_from:
            avg_start_idx = idx
        if start_idx is None and dte >= dfrom:
            start_idx = idx
        if end_idx is None and dte >= dto:
            end_idx = idx

    avg_start_idx = avg_start_idx or 0
    end_idx = end_idx or -1

    colors = []
    r_max = None
    if start_idx is not None:
        # We have some data within the desired range. Procede with calculations.
        startDate = parse(date_data[start_idx])
        endDate = parse(date_data[end_idx])

        # Filter the data to just the range of interest.
        new_r = full_r[start_idx:end_idx]
        new_theta = full_theta[start_idx:end_idx]

        # Pull out the data to be used to figure out "zero r"
        # +1 so we get at least one point
        avg_r = full_r[avg_start_idx : start_idx + 1]  # noqa: E203
        avg_theta = full_theta[avg_start_idx : start_idx + 1]  # noqa:E203

        # We color the dots based on age, so idx/color_basis,
        # such that the first point is 0 and the last point is 1.
        color_basis = len(new_r) - 1

        avg_x_val = 0
        avg_y_val = 0
        num_avg_vals = len(avg_r)

        for r, t in zip(avg_r, avg_theta):
            avg_x_val += r * math.cos(t)
            avg_y_val += r * math.sin(t)

        x0 = avg_x_val / num_avg_vals
        y0 = avg_y_val / num_avg_vals

        for i in range(len(new_r)):
            colors.append(i / color_basis)

            xi = (new_r[i] * math.cos(new_theta[i])) - x0
            yi = (new_r[i] * math.sin(new_theta[i])) - y0

            r_new = math.sqrt((xi * xi) + (yi * yi))
            t_new = math.atan2(yi, xi)

            if r_max is None or r_new > r_max:
                r_max = r_new

            new_r[i] = r_new
            new_theta[i] = t_new
    else:
        # No data in desired range
        startDate = dfrom
        endDate = dto
        new_r = []
        new_theta = []

    # Replace the data in the polar_data dictionary with the filtered data, potentially empty.
    polar_data["r"] = new_r
    polar_data["theta"] = new_theta
    polar_data["marker"]["color"] = colors
    polar_data["marker"]["colorbar"]["ticktext"] = [
        startDate.strftime("%m/%d/%y"),
        endDate.strftime("%m/%d/%y"),
    ]

    return r_max


def makePolarLayout(layout, data, station):
    """
    Modify the layout dictionary to be correct for our tilt data polar plot

    ARGUMENTS
    ---------
    layout: dict
        A plotly layout dictionary that will be modified for our polar tilt plots
    data: dict
        A plotly data dictionary containing the data to be plotted.
    station: str
        The station that the plot is for, will be used to set labels.

    RETURNS
    -------
    layout: dict
        The modified layout dictionary.
    """
    # Modify the layout to work for our tilt traces with polar graph
    layout["grid"]["rows"] = 4

    # Lots of magic numbers here, hopefully they all work together to make things look good.
    layout["annotations"] = [
        {
            "xref": "paper",
            "yref": "paper",
            "x": 0.02,
            "xanchor": "left",
            "y": 0.2125,
            "yanchor": "top",
            "text": "<b>Temperature</b>",
            "showarrow": False,
            "bgcolor": "rgba(255, 255, 255, 0.25)",
            "font": {"size": 12},
        },
        {
            "xref": "paper",
            "yref": "paper",
            "x": 0.5,
            "xanchor": "center",
            "y": 0.984,
            "yanchor": "bottom",
            "text": "<b>" + station + " Tilt (microradians)</b>",
            "showarrow": False,
            "bgcolor": "rgba(255, 255, 255, 0.25)",
            "font": {"size": 12},
        },
        {
            "xref": "paper",
            "yref": "paper",
            "x": 0.02,
            "xanchor": "left",
            "y": 0.4325,
            "yanchor": "top",
            "text": "<b>Y Tilt</b>",
            "showarrow": False,
            "bgcolor": "rgba(255, 255, 255, 0.25)",
            "font": {"size": 12},
        },
        {
            "xref": "paper",
            "yref": "paper",
            "x": 0.02,
            "xanchor": "left",
            "y": 0.6525,
            "yanchor": "top",
            "text": "<b>X Tilt</b>",
            "showarrow": False,
            "bgcolor": "rgba(255, 255, 255, 0.25)",
            "font": {"size": 12},
        },
    ]

    layout["polar"] = {
        "domain": {
            "y": [0.69, 0.964],
            "x": [0, 1],
        },
        "angularaxis": {
            "tickvals": [90, 0, 270, 180, data["x_axis"], data["y_axis"]],
            "ticktext": [
                "<b>North</b>",
                "<b>E</b>",
                "<b>South<b>",
                "<b>W</b>",
                "<b>+X</b>",
                "<b>+Y<b>",
            ],
            "linewidth": 1,
        },
    }

    row_height = 0.22  # Base height of a full segment, in percent
    seg_sep = 0.015  # Gap between graphs
    for i, yaxis in enumerate(["yaxis4", "yaxis3", "yaxis2"]):
        start = row_height * i + (seg_sep / 2)
        stop = row_height * (i + 1) - (seg_sep / 2)
        layout[yaxis]["domain"] = [start, stop]

    return layout


def makePolarTrace(data):
    """
    Create plotly data dictionaries from the tilt data.
    Creates three plot data dictionaries: one with the tilt data, and two showing
    the orientations of the X and Y axis respectively.

    ARGUMENTS
    ---------
    data: dict
        A raw data object containing tilt and polar plot data

    RETURNS
    -------
    polar_trace1: dict
        plotly data dictionary with values of radius and theta from the supplied data
    x_axis: dict
        a plotly data dictionary that plots a line showing the orientation of the X axis.
    y_axis: dict
        a plotly data dictionary that plots a line showing the orientation of the Y axis.

    """
    plot_start = parse(data["dates"][0]).strftime("%m/%d/%y")
    plot_end = parse(data["dates"][-1]).strftime("%m/%d/%y")

    polar_trace1 = {
        "type": "scatterpolar",
        "r": data["radius"],
        "theta": data["theta"],
        "thetaunit": "radians",
        "mode": "markers",
        "marker": {
            "color": data["timestamps"],
            "colorscale": [["0.0", "#ACD6E6"], ["1.0", "#1A0391"]],
            "size": 7,
            "colorbar": {
                "thickness": 15,
                "len": 0.3,
                "ypad": 0,
                "x": 1,
                "xanchor": "right",
                "y": 0.685,
                "yanchor": "bottom",
                "tickvals": [0, 1],
                "ticktext": [plot_start, plot_end],
                "tickmode": "array",
                "tickfont": {"size": 9.6},
            },
        },
        "cliponaxis": False,
    }

    x_axis = {
        "type": "scatterpolar",
        "r": data["x_radius"],
        "theta": data["x_theta"],
        "mode": "lines",
        "line": {
            "color": "black",
        },
    }

    y_axis = {
        "type": "scatterpolar",
        "r": data["y_radius"],
        "theta": data["y_theta"],
        "mode": "lines",
        "line": {
            "color": "black",
        },
    }

    return (polar_trace1, x_axis, y_axis)


def gen_tilt_data_dict(x, y, color, idx=None):
    """
    Generic function to generate a plotly data dictionary sutable for
    displaying tilt data time-series plots.

    ARGUMENTS
    ---------
    x: list
        The X, or time, data to be plotted.
    y: list
        the Y, or tilt, data to be plotted.
    color: str
        The color to be used for the plot markers, in any format recognized by plotly.
    idx: int, optional
        If specified, the index of the plot. Used to specify X and Y axis to plot on.

    RETURNS
    -------
    trace: dict
        A plotly data dictonary containing the data to be plotted.
    """
    trace = {
        "x": x,
        "y": y,
        "type": "scatter",
        "mode": "markers",
        "marker": {"size": 2, "color": color},
    }

    if idx is not None:
        trace["yaxis"] = f"y{idx}"
        trace["xaxis"] = f"x{idx}"

    return trace


def gen_plot_data_dict(x, y, error, color, idx=None):
    """
    Generic function to generate a plotly data dictionary sutable for
    displaying GPS time-series plots.

    ARGUMENTS
    ---------
    x: list
        The X, or time, data to be plotted.
    y: list
        the Y, or GPS offset, data to be plotted.
    error: list
        the error data to be plotted as error bars on the data points.
    color: list
        list of colors to be used for the plot markers. Each entry must be either 0 for a
        normal point, or 1 for a rapid solution. Other values may work, but will give odd results.
    idx: int, optional
        If specified, the index of the plot. Used to specify X and Y axis to plot on.

    RETURNS
    -------
    trace: dict
        A plotly data dictonary containing the data to be plotted.
    """
    trace = {
        "x": x,
        "y": y,
        "error_y": {
            "type": "data",
            "array": error,
            "visible": True,
            "color": "rgb(100,50,50)",
        },
        "type": "scatter",
        "mode": "lines+markers",
        "marker": {
            "size": 4,
            "cmin": 0,
            "cmax": 1,
            "colorscale": [[0, "rgb(55,128,256)"], [1, "rgb(256,128,55)"]],
            "color": color,
        },
        "line": {"color": "rgba(55,128,191,.25)"},
    }

    if idx is not None:
        trace["yaxis"] = f"y{idx}"
        trace["xaxis"] = f"x{idx}"

    return trace


def gen_subgraph_layout(titles, date_from, date_to):
    """
    Generate a plotly layout dictionary containing subplots for GPS or Tilt plots.

    ARGUMENTS
    ---------
    titles: list
        List of titles for each of the sub-plots to be plotted.
    date_from: date
        The start date to scale the X axis to
    date_to: date
        The end date to scale the X axis to

    RETURNS
    -------
    layout: dict
        A plotly formatted layout dictionary with subplots for each title specified.
    """
    if not isinstance(titles, (list, tuple)):
        titles = [
            titles,
        ]

    script_path = os.path.realpath(os.path.dirname(__file__))
    UAF_GI_LOGO_PATH = os.path.join(script_path, "static/img/uaf_gi_logo.png")
    uaf_gi_logo = Image.open(UAF_GI_LOGO_PATH)

    AVO_LOGO_PATH = os.path.join(script_path, "static/img/avo_logo.png")
    avo_logo = Image.open(AVO_LOGO_PATH)

    layout = {
        "paper_bgcolor": "rgba(255,255,255,1)",
        "plot_bgcolor": "rgba(255,255,255,1)",
        "showlegend": False,
        "margin": {"l": 50, "r": 25, "b": 25, "t": 63, "pad": 0},
        "grid": {
            "rows": 3,
            "columns": 1,
            "pattern": "independent",
            "ygap": 0.05,
        },
        "font": {"size": 12},
        "images": [
            {
                "source": uaf_gi_logo,
                "xref": "paper",
                "yref": "paper",
                "x": 1,
                "y": 1.008,
                "sizex": 0.27,
                "sizey": 0.27,
                "xanchor": "right",
                "yanchor": "bottom",
            },
            {
                "source": avo_logo,
                "xref": "paper",
                "x": 0.72,
                "y": 1.008,
                "sizex": 0.1062,
                "sizey": 0.1062,
                "xanchor": "right",
                "yanchor": "bottom",
            },
        ],
    }

    try:
        x_range = [date_from, date_to]
    except IndexError:
        x_range = None

    for i, title in enumerate(titles):
        if not title:
            continue

        i = i + 1  # We want 1 based numbering here
        y_axis = f"yaxis{i}"
        x_axis = f"xaxis{i}"

        layout[y_axis] = {
            "zeroline": False,
            "title": title,
            "gridcolor": "rgba(0,0,0,.3)",
            "showline": True,
            "showgrid": False,
            "linecolor": "rgba(0,0,0,.5)",
            "mirror": True,
            "ticks": "inside",
        }

        layout[x_axis] = {
            "automargin": True,
            "autorange": False,
            "range": x_range,
            "type": "date",
            "tickformat": "%Y.%m.%d",
            "hoverformat": "%m/%d/%Y",
            "gridcolor": "rgba(0,0,0,.3)",
            "showline": True,
            "showgrid": False,
            "mirror": True,
            "linecolor": "rgba(0,0,0,.5)",
            "ticks": "inside",
        }

        if i != len(titles):
            layout[x_axis]["matches"] = f"x{len(titles)}"
            layout[x_axis]["showticklabels"] = False

    return layout


def gen_graph_image(gen_height, data, layout, fmt="pdf", disposition="download", width=900):
    """
    Generate a plotly image (png,pdf,etc) using the specified data and layout

    ARGUMENTS
    ---------
    gen_height: int
        The final height of the generated image, in pixels
    data: list
        A list of plotly data dictionarys containing the graphs to be plotted.
    layout: dict
        A Plotly layout dict containing the layout parameters for the plot(s)
    fmt: str
        Output image format to be created (pdf, png, etc)
    dispostion: str (optional, default: download)
        Whether to instruct the browser to download the image, or view it in the browser.
        Will be used to set the Content-Disposition header.
    width: int
        The width of the final image to be created.

    RETURNS
    -------
    response: flask.Response
        A flask response object containing the generated image for viewing or download.

    """

    # We don't use mathjax so disable it here. Otherwise it adds
    # annoying "loading" messages to the output.
    pio.kaleido.scope.mathjax = None

    # Change plot types to scatter instead of scattergl. Bit slower, but works
    # properly with PDF output
    for plot in data:
        # We want regular plots so they come out good
        if "type" in plot and plot["type"].endswith("gl"):
            plot["type"] = plot["type"][:-2]

        if "marker" in plot and "colorscale" in plot["marker"]:
            cs = plot["marker"]["colorscale"]
            for entry in cs:
                # The first parameter comes in as a string, but needs to be a
                # number
                entry[0] = float(entry[0])

    # Use the plot title to generate a pretty file name
    file_name = layout["title"]["text"]
    file_name = file_name.replace(" ", "_")
    file_name = file_name.replace("/", "_")

    args = {
        "data": data,
        "layout": layout,
    }

    fig = go.Figure(args)

    # TEMPORARY DEBUG
    #    filename = f"{uuid.uuid4().hex}.pdf"
    #     fig.write_image(os.path.join('/tmp', filename), width = 600, height = 900,
    #                     scale = 1.75)

    # Since we chose 600 for the "width" parameter of the to_image call
    scale = min(width / 600, 22)
    fig_bytes = fig.to_image(format=fmt, width=600, height=gen_height, scale=scale)

    response = flask.make_response(fig_bytes)

    # Set the HTML Response headers for the content type and download/view
    content_type = "application/pdf" if fmt == "pdf" else f"image/{fmt}"
    response.headers.set("Content-Type", content_type)
    if disposition == "download":
        response.headers.set("Content-Disposition", "attachment", filename=f"{file_name}.pdf")
    else:
        response.headers.set("Content-Disposition", "inline")

    return response
