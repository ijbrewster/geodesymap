import glob
import json
import multiprocessing
import os
import pickle
import re
import ssl
import time

import certifi
import cv2
import flask
import geopandas
import h5py
import numpy
import pandas
import pygmt
import redis
import requests
import shapely
import xmltodict

from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
from io import BytesIO
from pathlib import Path
from urllib.request import urlopen

from shapely.geometry import box, Polygon
from cachetools.func import ttl_cache
from dateutil.parser import parse
from PIL import Image

try:
    from . import app, config, utils
    from .redis_cache import RedisCache, redis_cached
except ImportError:
    # Running as script
    from __init__ import app
    import config
    import utils
    from redis_cache import RedisCache, redis_cached


def get_insar_volcs():
    file_path = Path(config.DATA_PATH)
    insar_files = file_path.glob("*/insar/timeseries_*.h5")
    insar_polys = []
    volcs = utils.get_volcs()  # All AVO volcs
    volcs = geopandas.GeoDataFrame(volcs, columns=["id", "name", "latitude", "longitude"])
    volcs["location"] = geopandas.points_from_xy(volcs["longitude"], volcs["latitude"])
    volcs.set_geometry("location", inplace=True)

    for file in insar_files:
        try:
            lons, lats = get_lon_lats(str(file))
        except OSError:
            app.logger.warning(f"Unable to parse insar file {file}. Skipping.")
            continue
        north = lats.max()
        south = lats.min()
        east = lons.max()
        west = lons.min()
        geom = box(east, south, west, north)
        insar_polys.append(geom)

    insar_polys = geopandas.GeoDataFrame(insar_polys, columns=["geometry"])
    included_volcs = volcs.sjoin(insar_polys, predicate="within").drop_duplicates("name")
    # covered_volcs = sorted(inlcluded_volcs['name'])
    included_volcs.drop(columns="index_right", inplace=True)

    return included_volcs


def process_kml_file(kml_url):
    file_polys = []
    context = ssl.create_default_context(cafile=certifi.where())
    kml = xmltodict.parse(urlopen(kml_url, context=context))
    days = kml["kml"]["Document"]["Folder"]
    file_date = days[0]["name"]
    file_date = datetime.strptime(file_date, "%Y-%m-%d")
    for day_folder in days:
        day_date = day_folder["name"]
        day_date = datetime.strptime(day_date, "%Y-%m-%d").date()
        if day_date < datetime.utcnow().date():
            continue  # Don't parse past dates

        placemarks = day_folder["Folder"]["Placemark"]
        for placemark in placemarks:
            begin = placemark["TimeSpan"]["begin"]
            end = placemark["TimeSpan"]["end"]
            style = placemark["styleUrl"]
            # Parse extended data
            ext_data = {}
            for item in placemark["ExtendedData"]["Data"]:
                ext_data[item["@name"]] = item["value"]

            take_id = ext_data["DatatakeId"]
            # Process the coordinates into a polygon object
            coords = placemark["LinearRing"]["coordinates"]
            # Split the string into a list, discarding the elevation
            coords = [x.split(",")[:-1] for x in coords.split()]
            coords = numpy.asarray(coords).astype(float)  # convert to numbers
            # And create a polygon!
            coords = Polygon(coords)
            entry = (begin, end, style, ext_data, coords, file_date, take_id)
            file_polys.append(entry)

    file_polys = geopandas.GeoDataFrame(
        file_polys,
        columns=["begin", "end", "style", "data", "geometry", "file_date", "id"],
    )
    file_polys["startTime"] = pandas.to_datetime(file_polys["begin"])
    file_polys["file_date"] = pandas.to_datetime(file_polys["file_date"])
    return file_polys


def get_insar_acq_segments():
    app.logger.info("Getting insar acq segments")
    BASE_URL = "https://sentinels.copernicus.eu"
    URL = (
        f"{BASE_URL}/web/sentinel/missions/sentinel-1/observation-scenario/acquisition-segments"
    )
    resp = requests.get(URL)
    if resp.status_code != 200:
        app.logger.error(
            "Unable to load acquisition page. Status",
            resp.status_code,
            "response:",
            resp.text,
        )
        raise FileNotFoundError("Unable to load acquisition page")

    REDIS_HOST = getattr(config, "REDIS_HOST", "localhost")
    redis_db = redis.Redis(host=REDIS_HOST, db=2)
    kml_keys = set(redis_db.keys("kml_*"))

    # https://sentinels.copernicus.eu/documents/d/sentinel/s1a_mp_user_20230412t174000_20230502t194000
    search = re.compile(r'((https:/)?\/[^\s]+\/s1a_mp_user_[^\s">]+)')  # noqa: W605
    polys = []
    used_keys = set()
    for kml_file in search.findall(resp.text):
        kml_path = kml_file[0]
        kml_name = os.path.basename(kml_path)
        kml_url = kml_path if kml_path.startswith("http") else f"{BASE_URL}{kml_path}"
        kml_redis_key = f"kml_{kml_name}".encode()
        used_keys.add(kml_redis_key)
        if kml_redis_key in kml_keys:
            file_polys = pickle.loads(redis_db.get(kml_redis_key))
        else:
            # Retrieve the kml file
            file_polys = process_kml_file(kml_url)

        # Filter out any past acquisitions
        file_polys = file_polys[file_polys["startTime"] > datetime.utcnow()]
        # Update the cache to not have any old entries
        redis_db.set(kml_redis_key, pickle.dumps(file_polys))
        polys.append(file_polys)

    upcoming = geopandas.GeoDataFrame(pandas.concat(polys, ignore_index=True), crs=polys[0].crs)

    upcoming = (
        upcoming.sort_values(["id", "file_date"], ascending=[True, False], ignore_index=True)
        .drop_duplicates(["id"])
        .sort_values(["begin", "end", "file_date"], ascending=[True, True, False])
        .drop_duplicates(["begin", "end"])
        .reset_index(drop=True)
    )

    # Clean up the cache
    old_caches = kml_keys - used_keys
    for key in old_caches:
        redis_db.delete(key)

    return upcoming


def get_volcano_next_acq():
    volcs = get_insar_volcs()
    volcs.set_geometry("location", inplace=True)

    acquisitions = get_insar_acq_segments()
    acquisitions.set_geometry("geometry")
    # Make a copy of the geometry column so it is saved in the join
    acquisitions["area"] = acquisitions.geometry

    volc_acq = volcs.sjoin(acquisitions, predicate="within")
    volc_acq = (
        volc_acq.drop(columns="index_right")
        .sort_values(["name", "begin"], ignore_index=True)
        .drop_duplicates(["name", "begin", "end"])
        .reset_index(drop=True)
    )

    return volc_acq


def poly_to_leaflet(poly):
    if poly is None:
        return None

    return [[x[1], x[0]] for x in shapely.get_coordinates(poly)]


@app.route("/getNextAcq")
def get_next_acquisitions():

    acqs = get_volcano_next_acq()

    # Remove duplicated areas
    acqs.loc[acqs["area"].duplicated(), "area"] = None

    acqs["polys"] = acqs["area"].apply(poly_to_leaflet)
    data = acqs[["name", "data", "polys", "begin", "style"]]

    return data.to_json(orient="records")


lonlat_cache = RedisCache(db=3, ttl=86400)


@redis_cached(lonlat_cache, key=utils.h5_file_key)
def get_lon_lats(file):
    """
    Generate a lon/lat list from the specified file using FIRST, STEP and LENGTH/WIDTH values
    """

    if isinstance(file, str):
        h5_file = h5py.File(file)
    else:
        h5_file = file

    lon_start = float(h5_file.attrs["X_FIRST"])
    lon_step = float(h5_file.attrs["X_STEP"])
    lon_count = int(h5_file.attrs["WIDTH"])

    if lon_start > 0:
        lon_start -= 360

    lat_start = float(h5_file.attrs["Y_FIRST"])
    lat_step = float(h5_file.attrs["Y_STEP"])
    lat_count = int(h5_file.attrs["LENGTH"])

    lons = numpy.arange(lon_start, lon_start + (lon_step * lon_count), step=lon_step)
    lats = numpy.arange(lat_start, lat_start + (lat_step * lat_count), step=lat_step)

    # Make sure we have the right number of values. I have occasionally seen
    # the above generation process produce an extra value - probably due to rounding issues.
    lons = lons[:lon_count]
    lats = lats[:lat_count]

    return (lons, lats)


def find_closest_idx(lon_grid, lat_grid, lng, lat, zero_data=None):
    """
    Find the closest grid point with data to a specified point by comparing
    the distances from the clicked point to each point on the grid.

    NOTES
    -----
    This is HARDLY the most efficient method. Since the data is on a regular lat/lon
    grid, you could simply find the difference between the grid start and the
    clicked point, and divide by the grid step to find the grid point. This does,
    however, have the advantage of enabling you to eliminate any points with no data
    from contention, and seems to be fast enough.
    """
    # Compute a grid of distances, so we can set any all-zero cells to large distances
    dists = utils.haversine_np(lon_grid, lat_grid, lng, lat)

    # Set the dist for any grid points that are all zeros to a high value so it doesn't match
    # any finds slices that have non-zero values in them. We use ~ to
    # invert the logic (slices with ONLY zeros).
    if len(zero_data.shape) > 2:
        all_zero = ~zero_data.any(axis=0)
    else:
        all_zero = zero_data == 0

    dists[all_zero] = 999

    closest_idx = numpy.unravel_index(dists.argmin(), dists.shape)
    return closest_idx


@ttl_cache(ttl=86400)
def process_insar_timeseries(filename, point):
    """Given an InSAR timeseries file and user-selected point, return
    the timeseries data closest to the selected point."""
    file = h5py.File(filename)

    # Create a 2D grid of lons and lats matching the data
    lons, lats = get_lon_lats(file)

    lon_grid, lat_grid = numpy.meshgrid(lons, lats)

    # The point the user clicked on in the map view
    point = json.loads(point)

    ts_data = numpy.asarray(file["timeseries"])

    closest_idx = find_closest_idx(lon_grid, lat_grid, point["lng"], point["lat"], ts_data)

    ts = ts_data[:, closest_idx[0], closest_idx[1]]

    cust_ref = flask.session.get("custom_ref")
    if cust_ref:
        ref_ts = ts_data[:, cust_ref[0], cust_ref[1]]
        ts -= ref_ts

    ts *= 100  # Meters/year to cm/year

    try:
        date_list = numpy.asarray(file["date"]).astype(str)  # convert from bytes
    except UnicodeDecodeError as e:
        app.logger.error(f"Unable to load date list: {e} \nRaw data:")
        app.logger.error(numpy.asarray(file["date"]))
        raise

    # Parse dates to a list of actual datetime objects. Format argument isn't explicitly
    # needed, but given anyway to prevent any potential confusion.
    date_objs = pandas.to_datetime(date_list, format="%Y%m%d")

    # To get the dates into a more standardized format that plotly likes
    dates = date_objs.astype(str)

    # get a linefit. Start by calculating the number of
    # days from the first data point for each point.
    try:
        # Pandas <2.0
        ddays = (date_objs - date_objs[0]).astype("timedelta64[D]").astype(int)
    except ValueError:
        # Pandas >=2.0
        ddays = (date_objs - date_objs[0]).days
    averagev, intercept = numpy.polyfit(ddays, ts, 1)
    lastval = averagev * ddays[-1] + intercept
    averagev *= 365.25

    return (
        {
            "dates": dates.tolist(),
            "ts": ts.tolist(),
            "intercept": intercept,
            "averagev": round(averagev, 2),
            "lastval": lastval,
        },
        closest_idx,
    )


# COLORMAP_DICT = {
# "red": [[0, 0, 0], [0.5, 1, 1], [1, 1, 1]],
# "green": [[0, 0, 0], [0.5, 1, 1], [1, 0, 0]],
# "blue": [[0, 1, 1], [0.5, 1, 1], [1, 0, 0]],
# "alpha": [[0, 0.7, 0.7], [0.5, 0.7, 0.7], [1, 0.7, 0.7]],
# }


def create_custom_colormap():
    """
    Create a colormap to use with a numpy array of values.
    Values must be scaled 0-255, and uint8
    """
    colormap = numpy.zeros((256, 4), dtype=numpy.uint8)

    # Blue channel
    colormap[:128, 2] = numpy.linspace(0, 255, 128)  # 0 to 0.5
    colormap[128:, 2] = 255  # 0.5 to 1

    # Green channel
    colormap[:128, 1] = numpy.linspace(0, 255, 128)  # 0 to 0.5
    colormap[128:, 1] = numpy.linspace(255, 0, 128)  # 0.5 to 1

    # Red channel
    colormap[:128, 0] = 255  # 0 to 0.5
    colormap[128:, 0] = numpy.linspace(255, 0, 128)  # 0.5 to 1

    # Alpha channel
    colormap[:, 3] = int(0.7 * 255)  # Constant alpha value of 0.7

    return colormap


def process_insar_velocities(
    filepath, raw=False, ref_point=-1, limit=None, raw_img=False, dates=None
):
    """Create a colored grid representing average InSAR velocities"""
    t1 = time.time()

    filename = os.path.join(config.DATA_PATH, filepath)

    # If the above isn't an h5 file, then this WILL throw an error,
    # without damage to the specified file. So the worst someone could
    # accomplish by sending a "bad" path in filepath is reading the wrong h5 file.
    h5_file = h5py.File(filename)

    velocities = gen_vel_field(h5_file, dates)

    if ref_point == -1:
        ref_point = flask.session.get("custom_ref")
    if ref_point is not None:
        print("Generating image with custom reference point", ref_point)
        velocities -= velocities[ref_point]
    else:
        ref_x = int(h5_file.attrs["REF_X"])
        ref_y = int(h5_file.attrs["REF_Y"])
        ref_point = (ref_y, ref_x)

    ZEROS = velocities == 0
    HEADING = float(h5_file.attrs["HEADING"])

    h5_file.close()

    vmin = velocities.min()
    vmax = velocities.max()

    # Normalize vector data 0-1, with original 0 => 0.5
    if limit is None:
        norm_max = max(abs(vmin), abs(vmax))
    else:
        norm_max = abs(limit / 100)  # Shouldn't need abs, but I don't trust people.

    norm_min = -1 * norm_max

    velocities = (velocities - norm_min) / (2 * norm_max)

    if raw:
        return (velocities, ref_point, HEADING, norm_max)

    colormap = create_custom_colormap()
    scaled_velocities = (velocities * 255).astype(numpy.uint8)

    colors = colormap[scaled_velocities]

    app.logger.info("Assigned values to colors after: %s", time.time() - t1)

    colors[ZEROS] = (0, 0, 0, 0)

    # Color the reference point (and the points around it) black
    colors[
        ref_point[0] - 2 : ref_point[0] + 2,  # noqa: #203
        ref_point[1] - 2 : ref_point[1] + 2,  # noqa: #203
    ] = (
        0,
        0,
        0,
        255,
    )  # Solid black

    # "Save" the image to a BytesIO "file" to send to the client
    app.logger.info("Ready to save in: %s", time.time() - t1)
    _, buffer_cv2 = cv2.imencode('.png', colors)
    fp = BytesIO(buffer_cv2)

    fp.seek(0)

    app.logger.error("Ready to send in: %s", time.time() - t1)

    if raw_img:
        return fp

    return flask.send_file(fp, mimetype="image/png")


def scale_square_image_for_projection(square_img, latitude):
    # Calculate the scaling factor based on latitude
    scaling_factor = numpy.cos(numpy.radians(latitude))

    # Get the original dimensions of the square image
    original_width, original_height = square_img.size

    # Calculate the new height to maintain the square appearance after projection
    new_height = int(original_height * scaling_factor)

    # Resize the image to the new dimensions
    scaled_img = square_img.resize(
        (round(original_width / 6), round(new_height / 6)), Image.LANCZOS
    )

    return scaled_img


def get_insar(file):
    """Client request handler for the InSAR velocity field.
    This is actually a two-step process. We figure out the parameters for
    the display here, along with which velocities file to use, and send those
    back to the client. The client then requests the actual velocity field image
    in a seperate request.
    """

    file_req_path = file.replace(config.DATA_PATH + "/", "")

    if not file.startswith(config.DATA_PATH) or not os.path.isfile(file):
        return flask.abort(404)

    h5_file = h5py.File(file)

    start_Date = datetime.strptime(h5_file.attrs["START_DATE"], "%Y%m%d")
    end_date = datetime.strptime(h5_file.attrs["END_DATE"], "%Y%m%d")

    lons, lats = get_lon_lats(h5_file)
    velocities = gen_vel_field(h5_file)
    HEADING = float(h5_file.attrs["HEADING"])

    cust_point = flask.request.args.get("ref_point")
    if cust_point == "undefined":  # Javascript issue sometimes
        cust_point = None

    cust_point = json.loads(cust_point) if cust_point else None

    if cust_point is None:
        flask.session["custom_ref"] = None
        del flask.session["custom_ref"]
    else:
        lon_grid, lat_grid = numpy.meshgrid(lons, lats)
        closest_idx = find_closest_idx(
            lon_grid, lat_grid, cust_point["lng"], cust_point["lat"], velocities
        )
        closest_idx = tuple((int(x) for x in closest_idx))  # Get rid of weird numpy types
        flask.session["custom_ref"] = closest_idx
        ref_vel = velocities[closest_idx]
        velocities -= ref_vel

    h5_file.close()

    vmin = velocities.min()
    vmax = velocities.max()

    cust_limit = flask.request.args.get("insar_limit")
    if cust_limit == "":  # Javascript empty value
        cust_limit = None

    if cust_limit is None:
        flask.session["custom_limit"] = None
        del flask.session["custom_limit"]
    else:
        flask.session["custom_limit"] = cust_limit

    data_max = max(abs(vmin), abs(vmax))
    url = f"getInSARImage?file={file}"

    if cust_limit is None:
        cust_limit = data_max
    else:
        url += f"&insar_limit={cust_limit}"
        cust_limit = float(cust_limit) / 100.0

    norm_min = -1 * cust_limit

    bounds = {
        "SW": {
            "lat": lats.min(),
            "lng": lons.min(),
        },
        "NE": {
            "lat": lats.max(),
            "lng": lons.max(),
        },
    }

    insar_info = {
        "data_lim": data_max,
        "vmin": norm_min,
        "vmax": cust_limit,
        "img_path": file_req_path,
        "img_name": os.path.basename(file),
        "coordinates": bounds,
        "start": start_Date.strftime("%-m/%-d/%y"),
        "end": end_date.strftime("%-m/%-d/%y"),
        "type": "img",
        "heading": HEADING,
    }

    return insar_info


def get_insar_png(sensor, file):
    file_path = os.path.join(config.DATA_PATH, sensor, "insar", file)
    insar_info = get_insar(file_path)
    location = insar_info["coordinates"]
    png = process_insar_velocities(insar_info["img_path"], raw_img=True)
    return png, location


@app.route("/downloadInSARTS")
def download_insar_file():
    filename = flask.request.args["file"]
    sensor = flask.request.args["sensor"]
    filename = os.path.join(sensor, "insar", filename)
    return flask.send_from_directory(config.DATA_PATH, filename)


def get_insar_image(file):
    """Client request handler for generating and returning the average velocity field image"""
    limit = flask.session.get("custom_limit")
    dates = flask.request.args.get('dates')
    if limit is not None:
        limit = float(limit)
    if dates is not None:
        dates = json.loads(dates)
        dates[0] = parse(dates[0])
        dates[1] = parse(dates[1])
        dates = tuple(dates)
    return process_insar_velocities(file, limit=limit, raw_img=True, dates=dates)


@app.route("/getInSarTS")
def get_timeseries():
    """Client request handler for retrieving the time-series data at the specified point"""
    point = flask.request.args["point"]
    file = flask.request.args["file"]
    sensor = flask.request.args["sensor"]
    file_path = os.path.join(config.DATA_PATH, sensor, "insar", file)
    file_path = os.path.realpath(file_path)
    if not file_path.startswith(config.DATA_PATH) or not os.path.isfile(file_path):
        return flask.abort(404)

    ts_data, index = process_insar_timeseries(file_path, point)

    # Get the path/frame information
    parts = file.split("_")
    parts[3] = parts[3][4:].replace(".h5", "")
    path = parts[3]

    ts_data["path"] = path

    return flask.jsonify(ts_data)


def get_insar_location(file: Path) -> dict:
    lons, lats = get_lon_lats(str(file))
    min_lat = round(lats.min(), 4)
    max_lat = round(lats.max(), 4)
    min_lon = round(lons.min(), 4)
    max_lon = round(lons.max(), 4)

    location = {
        "SW": {
            "lat": min_lat,
            "lng": min_lon,
        },
        "NE": {
            "lat": max_lat,
            "lng": max_lon,
        },
    }

    return location


def _gmt_map_insar(fig, sid, params):
    utils.setDiskValue("GenStat", "Drawing InSAR...", sid)
    insar_file = os.path.join(config.DATA_PATH, params["sensor"], "insar", f"{params['file']}")

    # With raw=True, we get a normalized array of velocities
    (values, (ref_y, ref_x), heading, norm_max) = process_insar_velocities(
        insar_file, raw=True, ref_point=params["ref"], limit=params["limit"]
    )

    values = values.flatten()

    # Values are normalized 0-1, so .5 is "no movement"
    mask = values != 0.5

    # Filter out any "no movement" data points
    values = values[mask]
    # pin out-of-range values to 0/1 so the color scale works properly
    values[values > 1] = 1
    values[values < 0] = 0

    # We have a list of x and y references along the edge of the data grid, we need a full grid
    # of x,y coordinates so there is a 1-1 correlation between x and y values and data values.
    lons, lats = get_lon_lats(insar_file)
    ref_lat = lats[ref_y]
    ref_lon = lons[ref_x]

    x, y = numpy.meshgrid(lons, lats)

    # Apply the same filter to the coordinate "grid" as we did to the data.
    x = x.flatten()[mask]
    y = y.flatten()[mask]

    # create the colormap for the insar velocities
    cm = "0/0/255,255/255/255,255/0/0"
    alpha = 30
    series = (0, 1)
    pygmt.makecpt(cmap=cm, background="i", transparency=alpha, continuous=True, series=series)

    fig.plot(x=x, y=y, fill=values, cmap=True, style="J0/.115k/.065k", pen="0p+c")

    # Plot the reference point
    fig.plot(x=ref_lon, y=ref_lat, fill="black", style="c0.125c", pen="0p")

    # Plot select points, if any
    for select in params["selects"]:
        select_lng = select["lng"]
        select_lat = select["lat"]
        select_color = select["color"]
        fig.plot(x=select_lng, y=select_lat, style="c0.21875c", pen=f"1.5p,{select_color}")

    # Plot the line-of-sight vectors
    # Plot vectors rotate counterclockwise from north. The heading is clockwise from north,
    # so we need the calculation
    look_dir = heading + 90  # 90 degrees to the right - or clockwise - from the heading.

    fig.plot(
        x=[lons[0], lons[0]],
        y=[lats[0], lats[0]],
        style="V0.25c+e+gblack+a40+h0+p1p",
        pen="1p",
        direction=([heading, look_dir], [1, 0.7]),
    )

    fig.text(
        x=[
            lons[0],
        ],
        y=[
            lats[0],
        ],
        text="LOS",
        font="6p,Helvetica-Bold,black",
        offset="j0p/3p",
        justify="TL",
        angle=360 - heading,  # This rotation is counter-clockwise, so we need to adjust
    )

    # Plot the InSar Color Scale bar
    with pygmt.config(
        FONT_ANNOT_PRIMARY="+8p,Helvetica,black",
        FONT_LABEL="10p,Helvetica,black",
        MAP_LABEL_OFFSET="2p",
        MAP_ANNOT_OFFSET="0p",
        MAP_FRAME_PEN=".5p",
    ):
        # Remake the colormap to go from -1 to 1 for the scale bar
        pygmt.makecpt(cmap=cm, transparency=alpha, continuous=True, series=(-1, 1))

        # Norm max is m/year. Convert to cm/year for display and round to nearest 10th
        norm_max = round(100.0 * norm_max, 1)

        fig.colorbar(position="jML+w3c/6%", box="+c5p/0p", scale=norm_max)
        # I can't just add a label to the colorbar without messing up the colorbar display,
        # so add/position the label seperately.
        fig.text(
            position="LM",
            text="cm/yr",
            angle=90,
            font="10p,Helvetica,black",
            justify="CT",
            offset="j0p/30p",
        )


# vel_field_cache = cachetools.TTLCache(maxsize=15, ttl=86400)
vel_field_cache = RedisCache(db=3, ttl=86400)


@redis_cached(vel_field_cache, key=utils.h5_file_key)
def gen_vel_field(file, filter_dates=None):
    """
    Generate a velocity field by calculating the slope of time-series data stored in an HDF5 file.

    PARAMETERS
    ----------
    ts : h5py.File
        An h5 file containing time-series data. Time series data should be stored
        in the 'timeseries' dataset, while date data should be stored in the 'date'
        dataset

    RETURNS
    -------
    numpy.ndarray
        A 2D NumPy array of the same dimensionality as the input data containing velocity values
        for each spatial coordinate of the input.
    """

    close = False
    #  If we are given a string to work with, open the specified H5 file
    # and close it when we are done with it. Otherwise, just use the
    # provided file and don't close it.
    if isinstance(file, str):
        close = True
        file = h5py.File(file)

    t1 = time.time()
    dates = numpy.asarray(file['date']).astype(str)
    date_objs = pandas.to_datetime(dates)
    t2 = time.time()

    ddays = (date_objs - date_objs[0]).days.to_numpy()

    # Some fun reshaping so we can run this calulation fully vectorized
    timeseries_ds = file['timeseries']
    if filter_dates is not None:
        if not isinstance(filter_dates, (list, tuple)) or len(filter_dates) != 2:
            raise ValueError("filter_dates must be a list or tuple with exactly two elements.")
        dfrom = pandas.Timestamp(filter_dates[0])
        dto = pandas.Timestamp(filter_dates[1])
        start_index = date_objs.searchsorted(dfrom, side="left")
        stop_index = date_objs.searchsorted(dto, side="right")
        timeseries_ds = timeseries_ds[start_index:stop_index, ...]
        ddays = ddays[start_index:stop_index]

    ts_data = numpy.asarray(timeseries_ds)
    ts_data_reshaped = ts_data.reshape(ts_data.shape[0], -1)
    t3 = time.time()
    num_processors = multiprocessing.cpu_count()
    num_slices = ts_data_reshaped.shape[1]
    chunk_size = (num_slices + num_processors - 1) // num_processors

    mean_ddays = numpy.mean(ddays)
    denominator = numpy.sum((ddays - mean_ddays) ** 2)
    slopes = numpy.empty(num_slices, dtype=float)

    futures = []
    with ThreadPoolExecutor() as executor:
        for i in range(0, num_slices, chunk_size):
            start = i
            stop = min(i + chunk_size, num_slices)
            future = executor.submit(
                calculate_slope,
                ddays,
                mean_ddays,
                denominator,
                ts_data_reshaped[:, start:stop],
                slopes,
                start,
                stop,
            )
            futures.append(future)

        for future in futures:
            future.result()

    slopes = slopes.reshape(ts_data.shape[1:])

    t4 = time.time()
    app.logger.warning(
        f"Loaded dates in: {t2 - t1} Loaded ts data in: {t3 - t2} Calculated slopes in: {t4 - t3}"
    )

    if close:
        file.close()

    return slopes


def calculate_slope(ddays, mean_ddays, denominator, ts_data, result, start, stop):
    mean_ts_data = numpy.mean(ts_data, axis=0)
    numerator = numpy.sum((ddays[:, None] - mean_ddays) * (ts_data - mean_ts_data), axis=0)
    result[start:stop] = (numerator / denominator) * 365.25


if __name__ == "__main__":
    """Arbitrary test code. Can be changed at will."""
    os.chdir('/Users/israel/Development/Geodesy/data/Sentinel-1/insar')
    import time

    for file in glob.glob("timeseries_*.h5"):
        t1 = time.time()
        ts = h5py.File(file)
        vel_file = file.replace("timeseries", "velocity")
        vel = h5py.File(vel_file)
        vel_calc = gen_vel_field(ts)
        t2 = time.time()
        print("Calculated in:", t2 - t1)

        vel_orig = numpy.array(vel['velocity'])
        diff = vel_orig - vel_calc
        diff_nonzero = diff[vel_orig != 0]
