import os
import re
import shutil
import tempfile
import threading

import flask

from flask_apispec import doc, use_kwargs, marshal_with
from marshmallow import Schema, fields, validates_schema, validate
from werkzeug.utils import secure_filename

from . import app, spec, docs, overlays, utils


def list_overlays(overlay_path, dir_only=True):
    exclusions = ["archive", ".DS_Store"]
    try:
        if dir_only:
            files = [x for x in overlay_path.iterdir() if x.is_dir()]
        else:
            files = overlay_path.iterdir()

        files = [
            {"name": "ifr" if x.name == "kml" else x.name}
            for x in files
            if x.name not in exclusions
        ]
    except FileNotFoundError:
        files = []

    ret_obj = {
        "items": files,
        "count": len(files),
    }

    return ret_obj


############# Dummy response schema. Stupid that we need this...#########
class ResponseSchema(Schema):
    message = fields.Str()
    error = fields.Str()


########### parser for overlay POST requests #########
class FileSchema(Schema):
    file = fields.Raw(
        type="file",
        description="GeoTIFF overlay image",
        required=True,
    )


class OverlaySchema(Schema):
    date = fields.Str(
        required=True,
        validate=validate.Regexp(r"^\d{8}(_\d{8})?$"),
        description="The date for this image, formatted as YYYYMMDD. Different start and end dates "
        "can be specified by seperating with an underscore (_)",
    )

    type = fields.Str(
        required=True,
        validate=validate.OneOf(["ifr", "amp"]),
        labels=["Interferogram", "Amplitude"],
        description="The type of this image, one of 'ifr' for an interferogram or 'amp' for an "
        "amplitude image",
    )

    identifier = fields.Str(
        required=True,
        validate=validate.Regexp(r"^[\w.\s-]+$"),
        description="Identifier for this image, such as path/frame or orbit/direction. "
        "Example: path42frame50",
    )
    sensor = fields.Str(
        required=True,
        validate=validate.Regexp(r"^[\w.\s-]+$"),
        description="The sensor that captured this image, such as TerraSAR-X or Sentinel-1",
    )

    post = fields.Boolean(
        missing=True,
        description="Should the image be posted to the mattermost channel, default is True",
    )

    @validates_schema
    def validate_and_sanitize_paths(self, data, **kwargs):
        # Sanitize 'identifier' and 'sensor' fields
        data["identifier"] = re.sub(r"[^\w.\s-]", "", data["identifier"])
        data["sensor"] = re.sub(r"[^\w.\s-]", "", data["sensor"])


#########################################################
############ Items Schema ############
class ItemSchema(Schema):
    name = fields.Str(description="The name of the item")


class ItemListSchema(Schema):
    items = fields.Nested(ItemSchema, many=True)
    count = fields.Int(description="The number of items returned")


class ErrorSchema(Schema):
    error = fields.Str(description="Description of the error")


spec.components.schema("Overlay", schema=OverlaySchema)
spec.components.schema("OverlayResponse", schema=ResponseSchema)
spec.components.schema("ItemList", schema=ItemListSchema)
spec.components.schema("ErrorSchema", schema=ErrorSchema)


@app.route("/api/overlay/sensors")
@marshal_with(ItemListSchema, code=200, description="List of overlay sensors")
@doc(
    tags=["Overlay"],
    description="List all overlay sensors",
)
def overlay_sensors():
    sensors = list_sensors()

    ret_obj = {
        "items": sensors,
        "count": len(sensors),
    }

    return ret_obj


def list_sensors():
    sensor_path = overlays.make_overlay_path()

    # Get a list of files
    sensors = [x for x in sensor_path.iterdir() if utils.has_overlay_hierarchy(x)]

    return sensors


@app.route("/api/overlay/sensors/<sensor>")
@marshal_with(ItemListSchema, code=200, description="Sensor Info")
@doc(
    tags=["Overlay"],
    description="List specific sensor",
    params={
        "sensor": {
            "description": "Name of the sensor/satelite",
            "in": "path",
            "type": "string",
            "required": True,
        }
    },
    responses={
        400: {
            "description": "Sensor not found",
        },
    },
)
def overlay_sensor_info(sensor):
    sensors = [x.name for x in list_sensors()]
    if sensor not in sensors:
        return {"error": "Invalid sensor specified"}, 400

    sensor_dir = overlays.make_overlay_path(sensor)
    types = list_overlays(sensor_dir)

    return types


@app.route("/api/overlay/sensors/<sensor>/types")
@marshal_with(ItemListSchema, code=200, description="List of types for sensor")
@doc(
    tags=["Overlay"],
    description="List overlay types for a sensor",
    params={
        "sensor": {
            "description": "Name of the sensor/satelite",
            "in": "path",
            "type": "string",
            "required": True,
        }
    },
    responses={
        400: {
            "description": "Sensor not found",
            # 'content': {'application/json': {'schema': ErrorSchema}},
        },
    },
)
def sensor_type_list(sensor):
    return overlay_sensor_info(sensor)


@app.route("/api/overlay/sensors/<sensor>/types/<type_>")
@marshal_with(ItemListSchema, code=200, description="List of images for sensor and type")
@doc(
    tags=["Overlay"],
    description="List overlay images for a given sensor and type",
    params={
        "sensor": {
            "description": "Name of the sensor/satelite",
            "in": "path",
            "type": "string",
            "required": True,
        },
        "type_": {
            "description": "The type of the image",
            "in": "path",
            "type": "string",
            "required": True,
        },
    },
)
def type_ident_list(sensor, type_):
    type_dir = overlays.make_overlay_path(sensor, type_)
    images = list_overlays(type_dir)
    return images


@app.route("/api/overlay/sensors/<sensor>/types/<type_>/images")
@marshal_with(ItemListSchema, code=200, description="List of images for sensor and type")
@doc(
    tags=["Overlay"],
    description="List images (path/frame, orbit, etc) for a given sensor and type",
    params={
        "sensor": {
            "description": "Name of the sensor/satelite",
            "in": "path",
            "type": "string",
            "required": True,
        },
        "type_": {
            "description": "The type of the image",
            "in": "path",
            "type": "string",
            "required": True,
        },
    },
)
def type_images_list(sensor, type_):
    type_dir = overlays.make_overlay_path(sensor, type_)
    images = list_overlays(type_dir)
    return images


@app.route("/api/overlay/sensors/<sensor>/types/<type_>/images/<image>")
@marshal_with(ItemListSchema, code=200, description="List of dates/date pairs")
@doc(
    tags=["Overlay"],
    description="List dates/date pairs for a given sensor,type,and image (path/frame, orbit, etc)",
    params={
        "sensor": {
            "description": "Name of the sensor/satelite",
            "in": "path",
            "type": "string",
            "required": True,
        },
        "type_": {
            "description": "The type of the image",
            "in": "path",
            "type": "string",
            "required": True,
        },
        "image": {
            "description": "The image identifier (path/frame, Orbit, etc)",
            "in": "path",
            "type": "string",
            "required": True,
        },
    },
)
def image_detail(sensor, type_, image):
    img_dir = overlays.make_overlay_path(sensor, type_, image)
    dates = list_overlays(img_dir)

    return dates


@app.route("/api/overlay/sensors/<sensor>/types/<type_>/images/<image>/dates")
@marshal_with(ItemListSchema, code=200, description="List of dates/date pairs")
@doc(
    tags=["Overlay"],
    description="List dates/date pairs for a given sensor,type,and image (path/frame, orbit, etc)",
    params={
        "sensor": {
            "description": "Name of the sensor/satelite",
            "in": "path",
            "type": "string",
            "required": True,
        },
        "type_": {
            "description": "The type of the image",
            "in": "path",
            "type": "string",
            "required": True,
        },
        "image": {
            "description": "The image identifier (path/frame, Orbit, etc)",
            "in": "path",
            "type": "string",
            "required": True,
        },
    },
)
def date_list(sensor, type_, image):
    img_dir = overlays.make_overlay_path(sensor, type_, image)
    dates = list_overlays(img_dir)

    return dates


@app.route("/api/overlay/<sensor>/<type_>/<ident>/<date>/", methods=["DELETE"])
@doc(
    tags=["Overlay"],
    params={
        "sensor": {
            "description": "Name of the sensor/satelite",
            "in": "path",
            "type": "string",
            "required": True,
        },
        "type_": {
            "description": "Type of the overlay image",
            "in": "path",
            "type": "string",
            "required": True,
        },
        "ident": {
            "description": "Identifier for path/frame or orbit",
            "in": "path",
            "type": "string",
            "required": True,
        },
        "date": {
            "description": "date or date_pair for the overlay",
            "in": "path",
            "type": "string",
            "required": True,
        },
    },
    responses={
        204: {
            "description": "The overlay was deleted succesfully",
        },
        403: {
            "description": "Forbidden",
        },
    },
    description="Delete an overlay",
)
def delete_overlay(sensor, type_, ident, date):
    return "Forbidden", 403  # Everyone is forbidden from deleting overlays until

    # authentication is enabled.
    overlay_path = overlays.make_overlay_path(sensor, type_, ident, date)
    if not overlay_path.exists():
        return flask.jsonify(error="Overlay not found"), 404

    try:
        if overlay_path.is_file():
            overlay_path.unlink()
        else:
            shutil.rmtree(overlay_path)
    except FileNotFoundError:
        return flask.jsonify(error="Overlay not found"), 404
    except PermissionError:
        return "Forbidden", 403
    except OSError as e:
        # Log the exception details for debugging
        print(f"An error occurred: {e}")
        return flask.jsonify(error="Internal server error"), 500

    return "", 204


@app.route("/api/overlay", methods=["POST"])
@doc(
    tags=["Overlay"],
    description="Upload a new overlay",
    consumes=["multipart/form-data"],
)
@use_kwargs(FileSchema, location="files")
@use_kwargs(OverlaySchema, location="form")
@marshal_with(ResponseSchema, description="Acknoledgement and error (if any)", code=200)
def upload_overlay(**args):
    """Upload GeoTIFF files to be used as overlays

    Upload GeoTIFF files to be displayed in the overlay menu of the AVO Geodesy page
    """
    file = args["file"]
    del args["file"]

    if file.filename == "":
        return flask.jsonify({"error": "No selected file"}), 400

    filename = secure_filename(file.filename)
    tempdir = tempfile.TemporaryDirectory()
    filepath = os.path.join(tempdir.name, filename)
    args["filepath"] = filepath

    with open(filepath, "wb") as f:
        while chunk := file.stream.read(4096):
            f.write(chunk)

    threading.Thread(target=utils.tiff_to_tiles, args=(filepath, args, tempdir)).start()

    return {
        "message": "File uploaded successfully. Overlay generation will continue in the background."
    }


docs.register(upload_overlay)
docs.register(delete_overlay)
docs.register(overlay_sensors)
docs.register(overlay_sensor_info)
docs.register(sensor_type_list)
docs.register(type_ident_list)
docs.register(type_images_list)
docs.register(date_list)
docs.register(image_detail)
