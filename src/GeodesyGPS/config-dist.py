stations = {}

DATA_PATH = "/geodesy/data"
STATION_IMG_PATH = "/Users/israel/Development/geodesymap-master/img_stationphotos"

############# GEODIVA MySQL Database####### # noqa: E266
GDDB_USER = "user_for_geodiva"
GDDB_PASS = "password_for_geodiva"
GDDB_HOST = "augustine.snap.uaf.edu"


############## Geodesy Postgresql Database ############ # noqa: E266
PSQL_USER = "user_for_postgresql"
PSQL_PASS = "password_for_postgresql"
PSQL_DB = "Postgresql_database_name"
PSQL_HOST = "postgresql_host"
