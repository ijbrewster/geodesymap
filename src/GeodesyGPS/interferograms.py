import json
import os
import zipfile

import flask

from pathlib import Path

from xml.etree import ElementTree

from . import app
from .config import DATA_PATH
from GeodesyGPS import utils


def parse_sentinel_kmz(overlay):
    # Namespace declerations for the KML file
    ns = {
        "default": "http://www.opengis.net/kml/2.2",
        "gx": "http://www.google.com/kml/ext/2.2",
    }

    png = None
    viewBoundScale = None

    icon = overlay.find("default:Icon", ns)
    for item in icon:
        if item.tag.endswith("href"):
            png = item.text
        if item.tag.endswith("viewBoundScale"):
            viewBoundScale = item.text

    coordinates = overlay.find("gx:LatLonQuad/default:coordinates", ns)
    SW, SE, NE, NW = coordinates.text.split()
    SW = SW.split(",")
    NE = NE.split(",")

    # We don't currently use these, but might as well process them so they're available.
    SE = SE.split(",")
    NW = NW.split(",")
    location = {
        "SW": {
            "lat": float(SW[1]),
            "lng": float(SW[0]),
        },
        "NE": {
            "lat": float(NE[1]),
            "lng": float(NE[0]),
        },
    }

    return (png, location, viewBoundScale)


def parse_csk_kmz(overlay):
    ns = {"default": "http://earth.google.com/kml/2.1"}

    png = None
    viewBoundScale = None

    icon = overlay.find("default:GroundOverlay/default:Icon", ns)
    for item in icon:
        if item.tag.endswith("href"):
            png = item.text

    coordinates = overlay.find("default:GroundOverlay/default:LatLonBox", ns)
    north = coordinates.find("default:north", ns).text
    south = coordinates.find("default:south", ns).text
    east = coordinates.find("default:east", ns).text
    west = coordinates.find("default:west", ns).text

    location = {
        "SW": {
            "lat": float(south),
            "lng": float(west),
        },
        "NE": {
            "lat": float(north),
            "lng": float(east),
        },
    }

    return (png, location, viewBoundScale)


def process_kmz_file(sensor, kmz_file, png_as_file=False):
    """
    Load a kml document from a file object and return the parsed contents
    """

    parsers = [parse_sentinel_kmz, parse_csk_kmz]

    filename = os.path.realpath(os.path.join(DATA_PATH, sensor, "kml", kmz_file))

    if not filename.startswith(DATA_PATH):
        flask.abort(404)

    with zipfile.ZipFile(filename) as kmz:
        kml_filename = next(x for x in kmz.namelist() if x.endswith("kml"))
        with kmz.open(kml_filename) as contents:
            kml_tree = ElementTree.parse(contents)
            kml_root = kml_tree.getroot()
            overlay = kml_root[0]
            for parser in parsers:
                try:
                    png, location, viewBoundScale = parser(overlay)
                    if location["SW"]["lng"] > 0:
                        location["SW"]["lng"] -= 360
                    if location["NE"]["lng"] > 0:
                        location["NE"]["lng"] -= 360
                except Exception as e:
                    app.logger.debug(e)
                    continue
                else:
                    # Good parse. Move on.
                    break
            else:
                # No parsers work with this file.
                flask.abort(404)

            if png_as_file:
                png = kmz.open(png)

    return (png, location, viewBoundScale)
