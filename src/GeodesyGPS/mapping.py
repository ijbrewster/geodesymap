"""
Mapping.py

Functions for the geodesy web app. These functions handle most of the general
functioning/back end processing of the geodesy page.
"""

# This import has to be first, otherwise it is broken
# by the osgeo import in utils.
from shapely.geometry import Polygon
import shapely_geojson

import glob
import json
import logging
import math
import multiprocessing
import os
import shutil
import tempfile
import uuid
import zipfile

from datetime import datetime, timedelta, timezone
from io import BytesIO
from pathlib import Path

import flask
import flask.helpers
import numpy
import osgeo.gdal
import pandas as pd
import geopandas
import pygmt
import requests

from dateutil.parser import parse, ParserError
from flask_apispec import doc
from flask_cors import cross_origin
from PIL import Image
from pyproj import Geod
from werkzeug.utils import secure_filename
from osgeo_utils.gdal_merge import main as gdal_merge

from . import app, TEMP_DIR, utils, insar, interferograms, config, docs, data_utils
from .config import STATION_IMG_PATH, DATA_PATH
from .utils import stations
from .data_utils import load_db_data
from .plot_utils import (
    gen_tilt_data_dict,
    makePolarTrace,
    scalePolarPlot,
    gen_subgraph_layout,
    makePolarLayout,
    gen_plot_data_dict,
    gen_graph_image,
)


@app.context_processor
def inject_globals():
    """The following variables will be injected into the context of every template,
    allowing them to be used without having to explicitly specify them each time."""

    return {
        # The public variable defines if we are looking at the public page or internal
        "public": flask.session.get("public", False)
    }


@app.route("/")
def index():
    """Render the index page. This is a landing page
    with direct links to a number of the stations"""

    # Set to *not* public when visiting index.html
    flask.session["public"] = False

    # Create a session ID for this session
    if "_sid" not in flask.session:
        flask.session["_sid"] = uuid.uuid4().hex

    return flask.render_template("index.html")


##################
# The next two functions are responsible for rendering of the main
# "map" page, the first for internal users, and the second for public
# users. No attempt is made to segregate the two, either group can
# access either page. If this proves to be undesirable, additional
# checks can be added such as authentication on the internal page
# or IP address checking.
####################


@app.route("/map")
def _map():
    """This is the main *internal* "map" page"""

    # Since we are internal, clear the public flag if set
    flask.session["public"] = False
    return map_page()


@app.route("/public")
def public():
    """The main *public* map page. Note that the "public" attribute in the session is
    set true here. This is used on the templates to hide/remove/change various components.
    """
    flask.session["public"] = True

    return map_page()


# The actual map page function, now that public is set as desired
def map_page():
    if "_sid" not in flask.session:
        flask.session["_sid"] = uuid.uuid4().hex

    try:
        insar_opts = sorted(os.listdir(DATA_PATH))
    except FileNotFoundError:
        insar_opts = []

    insar_files = [x[9:-3] for x in insar_opts if x.startswith("timeseries_")]

    locations = utils.load_locations()

    overlay = flask.request.args.get("overlay")
    sensor = flask.request.args.get("sensor")
    o_type = flask.request.args.get("type")
    dfrom = flask.request.args.get("dfrom")
    dto = flask.request.args.get("dto")

    # Format date into proper format for website use
    try:
        dfrom = parse(dfrom)
    except (TypeError, ParserError):
        overlay_date = None
    else:
        try:
            dto = parse(dto)
        except (TypeError, ParserError):
            dto = None
        from_str = dfrom.strftime("%m/%d/%Y")
        to_str = dto.strftime('%m/%d/%Y') if dto is not None else from_str
        overlay_date = f"{from_str} - {to_str}"

    # Make sure type is kosher
    if o_type == 'ifr':  # Interferograms are kmz type due to historical reasons.
        o_type = 'kml'

    args = {
        "locations": locations,
        "insar": insar_files,
        "overlay": overlay,
        "sensor": sensor,
        "type": o_type,
        "overlay_date": overlay_date,
        "station_volcs": json.dumps(utils.station_volcs),
    }

    return flask.render_template("map.html", **args)


@app.route("/get_volc")
def get_volc_for_point():
    center = flask.request.args['center']
    center = json.loads(center)
    volc_name = utils.point_to_volc(center['lat'], center['lng'])
    volc_id = utils.VOLC_IDS[volc_name]

    return flask.jsonify((volc_name, volc_id))


@app.route('/stationVolcs')
def get_volcs_for_station():
    station = flask.request.args['station']
    sta_info = utils.stations[station]  # Will raise an error if user is bad
    volc = utils.point_to_volc(sta_info['lat'], sta_info['lng'])
    opts: pd.DataFrame = utils.BY_PARENT.loc[utils.VOLC_IDS[volc]]
    opts = opts.dropna(subset=['latitude', 'longitude'])
    result = {
        row['name']: json.dumps([row['latitude'], row['longitude']])
        for _, row in opts.iterrows()
    }

    return flask.jsonify(result)


@app.route("/gen_graph", methods=["GET"])
def get_graph_image():
    """
    Download an image of the currently displayed time-series plots (GPS or Tilt).
    This function gathers the data and generates the parameters, while the
    gen_graph_image plot does the actual image generation.

    Arguments are provide via HTTP request.

    ARGUMENTS
    ---------
    tilt: bool
        'true' or '1' if the plots to be generated are tilt plots,
        otherwise GPS plots will be generated.
    dfrom: date str (optional, default -365 days)
        If specified, the start date to generate the plots from
    dto: date str (optional, default today)
        The end date to generate plots to
    station: str
        The station for which to generate plots
    fmt: str (optional, default png)
        The format to create the output as, such as pdf. Defaults to png.
    baseline: str (optional, default: site default)
        The baseline station to reference GPS readings against. Can be an empty
        string to not use a baseline station.
    width: int (optional, default 900)
        The width, in pixels, to create the ouput plot at.

    """
    is_tilt = flask.request.args["tilt"]

    # Convert to a real boolean (HTML arguments all come in as strings)
    is_tilt = True if is_tilt == "true" or is_tilt == "1" else False

    dfrom = flask.request.args.get("dfrom")

    try:
        # First, see if we have a date provided
        date_from = parse(dfrom)
    except Exception:
        # If not a parsable date, see if it is an integer -1
        try:
            dfrom = int(dfrom)
            if dfrom == -1:
                date_from = None
            else:
                raise ValueError("Unknown date from value. Using 365 days")
        except (TypeError, ValueError):
            # Not -1 either, so get the past year of data by default
            date_from = datetime.now().replace(
                hour=0, minute=0, second=0, tzinfo=timezone.utc
            ) - timedelta(days=365)

    try:
        date_to = parse(flask.request.args.get("dto"))
    except Exception:
        date_to = datetime.now().replace(hour=23, minute=59, second=59, tzinfo=timezone.utc)

    station = flask.request.args["station"]
    fmt = flask.request.args.get("fmt", "png")
    baseline = flask.request.args.get("baseline")
    width = int(flask.request.args.get("width", 900))

    # Plot title to be displayed at the top.
    title = station

    title += f'{date_from.strftime("%Y-%m-%d")} to {date_to.strftime("%Y-%m-%d")}'
    if is_tilt:
        # Tilt data plots
        title += " Tilt "
        data = data_utils._get_tilt_data(date_from, date_to, station, raw=True)

        if date_from is None:
            date_from = parse(data["search_start"] + "Z")

        x_trace = gen_tilt_data_dict(data["dates"], data["x_tilt"], "red", 2)
        y_trace = gen_tilt_data_dict(data["dates"], data["y_tilt"], "blue", 3)
        temp_trace = gen_tilt_data_dict(data["dates"], data["temps"], "black", 4)

        date_span = date_to - date_from

        if date_span.total_seconds() < 60 * 60 * 24:
            # One day (or less)
            line_dict = {"width", 0.5}
            for trace in (x_trace, y_trace, temp_trace):
                trace["mode"] = "lines"
                trace["line"] = line_dict

        polar_graphs = makePolarTrace(data)
        r_max = scalePolarPlot(x_trace["x"], polar_graphs[0], date_from, date_to)

        plot_data = [x_trace, y_trace, temp_trace]

        layout = gen_subgraph_layout(
            ["", "X (Microradians)", "Y (Microradians)", "Degrees C"],
            date_from,
            date_to,
        )

        plot_data += list(polar_graphs)
        layout = makePolarLayout(layout, data, station)

        if r_max is not None:
            layout["polar"]["radialaxis"] = {"range": [0, math.ceil(r_max)]}

        # Change the tick style depending on time span
        minute_span = date_span.total_seconds() / 60
        if minute_span < 5:
            tickformat = "%H:%M:%S"
        elif minute_span <= 36 * 60:  # 36 hours
            tickformat = "%m/%d %H:%M"
        else:
            tickformat = "%Y-%m-%d"

        x_range = [date_from, date_to]
        for axis in ["xaxis2", "xaxis3", "xaxis4"]:
            layout[axis]["tickformat"] = tickformat
            layout[axis]["range"] = x_range

    else:
        # GPS plots
        title += " - "
        if baseline is None:
            baseline_sql = """
            SELECT s2.name FROM sites
            INNER JOIN stations s1 ON sites.id=s1.siteref
            INNER JOIN stations s2 ON s2.id=sites.baselines[1]
            WHERE s1.name=%s"""
            with utils.db_cursor() as cursor:
                cursor.execute(baseline_sql, (station,))
                try:
                    baseline = cursor.fetchone()[0]
                except TypeError:
                    pass  # Leave baseline as None

        data = get_graph_data(
            False,
            station=station,
            baseline_station=baseline,
            date_from=date_from,
            date_to=date_to,
        )

        if not data["dates"]:
            return "no data found", 404

        if date_from is None:
            date_from = parse(data["dates"][0] + "T00:00:00Z")

        ew_trace = gen_plot_data_dict(data["dates"], data["EW"], data["EWE"], data["rapid"])
        ns_trace = gen_plot_data_dict(data["dates"], data["NS"], data["NSE"], data["rapid"], 2)
        ud_trace = gen_plot_data_dict(data["dates"], data["UD"], data["UDE"], data["rapid"], 3)

        plot_data = [ew_trace, ns_trace, ud_trace]
        layout = gen_subgraph_layout(
            ["EAST (cm)", "NORTH (cm)", "VERTICAL (cm)"], date_from, date_to
        )

        ref_label = "Reference" if baseline in ('ITRF14', 'NOAM (geodvel)') else "Baseline"

        layout["annotations"] = [
            {
                "xref": "paper",
                "yref": "paper",
                "x": 0.004,
                "xanchor": "left",
                "y": 1.00,
                "yanchor": "bottom",
                "text": f"{ref_label}: {baseline}",
                "showarrow": False,
                "font": {"size": 11},
            }
        ]

    layout["title"] = {
        "text": title,
        "x": 0.115,
        "y": 0.939,
        "xanchor": "left",
        "yanchor": "bottom",
        "font": {
            "size": 16,
        },
    }
    height = 800 if is_tilt else 700
    return gen_graph_image(height, plot_data, layout, fmt, "inline", width=width)


@app.route("/map/gen_graph", methods=["POST"])
def gen_graph_from_web():
    """
    Create an PDF file for tilt or GPS time-series plots using
    plotly data and layout objects from the web front-end.

    Arguments are provided via the POST form object.

    ARGUMENTS
    ---------
    height: str
        The overall height of the output to produce, in pixels.
    data: str
        List of plotly data dicts. Supplied in JSON format
    layout: str
        Plotly layout dictionary, Supplied in JSON format

    RETURNS
    -------
    response: flask.response
        Flask response object containing the requested image, in PDF format.
    """
    graph_height = flask.request.form.get("height", 700)
    # Convert to an integer
    graph_height = int(graph_height)

    data = json.loads(flask.request.form["data"])
    layout = json.loads(flask.request.form["layout"])

    # Fix up images in layout (using a URL doesn't seem to work in my testing)
    static_path = os.path.join(app.static_folder, "img")

    for img in layout.get("images", []):
        img_name = img["source"].split("/")[-1]
        img_path = os.path.join(static_path, img_name)
        img_file = Image.open(img_path)
        img["source"] = img_file

    # Shift the title over a bit
    if "title" in layout:
        layout["title"]["x"] = 0.09
        layout["title"]["y"] = 0.92

    return gen_graph_image(graph_height, data, layout)


@app.route("/map/getGenStat")
def gen_status():
    """
    Get the current status of plot generation

    RETURNS
    -------
    stat: str
        current plot/map generation status
    """
    stat = utils.getDiskValue("GenStat", "Unknown")
    if stat is None:
        flask.abort(404)
    return stat


@app.route("/map/resetGenStat")
def reset_status():
    """Reset the generation status to idle (Waiting...)"""
    utils.setDiskValue("GenStat", "Waiting...")
    return "", 201


@app.route("/map/download", methods=["POST"])
def request_map_gen():
    """
    Request generation of a high-quality PDF version of the currently displayed map.
    Generation will take place in a seperate process, allowing this process to return
    and flask to continue processing other requests while the map image is being created.

    Arguments are provided via flask form POST. As such, all arguments come in as strings.

    ARGUMENTS
    ---------
    baseline: str
        The baseline station to use when generating vectors from GPS data
    date_from: date
        The start date to use for vector generation
    date_to: date
        The end date to use for vector generation
    scale: float
        Scaling factor to use for vector generation
    station: str
        Representative station. Vectors will be generated for all stations at this "site"
    zoom: int
        The zoom level of the displayed map
    quakes: dict
        JSON encoded dictionary of earthquakes to display on the map
    quality: int
        An integer, roughly coresponding to zoom level, indicating the resolution
        of basemap imagery to use.
    insar: list
        JSON encoded two-item list of (filename,sensor) indicating if the InSAR
        overlay should be drawn. Empty strings indicate no overlay.
    ifr: list
        JSON encoded two-item list of (filename,sensor) indicating if the Interferogram
        overlay should be drawn. Empty strings indicate no overlay.
    custom_ref: int
        Index reference into the InSAR velocities data array to be used as a reference
        point when drawing the InSAR overlay. Only used if an InSAR file/sensor is specified.
    insar_limit: float
        Upper limit to use on InSAR velocity fields when drawing the InSAR overlay, if specified.
    insar_selects: list (optional, default: "[]")
        JSON encoded list of InSAR points that have been selected for plotting. May be omitted
        if no points are selected, only applies if the InSAR overlay is specified.
    map_bounds: dict
        JSON encoded dictionary of map bounds (North, South, East, West)

    RETURNS
    -------
    response: flask.response
        JSON encoded dictionary containing the file name under which the generated
        image will be saved once generation is complete.
    """

    # Set the generation status value to processing request.
    utils.setDiskValue("GenStat", "Processing Request...")

    # old file path should be safe, since it is never sent to the user
    # See if we have an exsiting file we have generated previously.
    # If so, remove it.
    old_file_path = utils.getDiskValue("MapFilePath")
    old_file_name = flask.session.get("MapImage")
    if old_file_path is not None and old_file_name is not None:
        file_name = secure_filename(old_file_name)
        file_path = os.path.join(old_file_path, file_name)

        app.logger.info(f"Removing old file: {file_path}")
        try:
            os.remove(file_path)
        except FileNotFoundError:
            pass  # file already gone

    # just use a UUID for the filename
    save_file = f"{uuid.uuid4().hex}.pdf"
    flask.session["MapImage"] = save_file

    file_path = os.path.join(TEMP_DIR.name, save_file)
    utils.setDiskValue("MapFilePath", TEMP_DIR.name)

    # These arguments are used for generating the motion vectors displayed on the map.
    vector_args = {
        arg: flask.request.form[arg]
        for arg in (
            "baseline",
            "date_from",
            "date_to",
            "scale",
            "station",
            "zoom",
            "quakes",
        )
    }

    map_quality: str = flask.request.form["quality"]
    show_insar = flask.request.form["insar"]
    show_ifr = flask.request.form["ifr"]
    show_amp = flask.request.form["amp"]
    ref_point = flask.session.get("custom_ref")
    insar_limit = flask.request.form.get("insar_limit")
    if insar_limit is not None:
        insar_limit = float(insar_limit)
    insar_selects = json.loads(flask.request.form.get("insar_selects", "[]"))
    sta_types = json.loads(flask.request.form.get('station_types', "['continuous']"))

    # We can choose between a color and a grayscale hillshade. The 'c'
    # at the end of the quality argument indicates color.

    # "Quality is an integer that roughly coresponds to map zoom level, with 9 being a 'low'
    # quality/resolution basemap, and 11 being 'high' quality. If a quality of 11 is specified,
    # the actual map zoom level is used to automaticaly determine the resolution of basemap
    # image needed."
    if map_quality.endswith("c"):
        map_quality = int(map_quality[:-1])
        color = True
    else:
        map_quality = int(map_quality)
        color = False

    if map_quality == 11:
        # High quality
        map_quality = float(flask.request.form["zoom"])

    map_bounds = json.loads(flask.request.form["map_bounds"])
    sid = flask.session.get("_sid")

    # spawn a new process to handle this request
    insar_params = None
    if show_insar:
        insar_file, insar_sensor = json.loads(show_insar)
        if insar_file and insar_file != "No InSAR":
            insar_params = {
                "file": insar_file,
                "sensor": insar_sensor,
                "ref": ref_point,
                "limit": insar_limit,
                "selects": insar_selects,
            }

    gen_args = (
        file_path,
        vector_args,
        map_quality,
        color,
        map_bounds,
        sid,
        insar_params,
        show_ifr,
        show_amp,
        sta_types,
    )

    # We need to make sure we spawn here, not fork, so no open file pointers
    # are transfered to the new process.
    mp = multiprocessing.get_context("spawn")
    utils.setDiskValue("GenStat", "Initializing Generation Process...")
    logging.info("Initializing generator process")
    proc = mp.Process(target=gen_map_image, args=gen_args)
    proc.start()  # fire and forget

    # Send the filename back to the client so they know what to request.
    return flask.jsonify(
        {
            "MapImage": save_file,
        }
    )


def gen_map_image(
    file_path,
    vector_args,
    map_quality,
    color,
    map_bounds,
    sid,
    insar_params=None,
    ifr_info='["", ""]',
    amp_info='["",""]',
    sta_types=None,
):
    """
    Use GMT/pygmt to generate a PDF map image using the specified parameters.
    Will run as a seperate process in response to a web request. The setDiskValue function
    is used to update the progress of generation in such a manner that a seperate
    flask request process can check on the status.

    ARGUMENTS
    ---------
    file_path: str
        The filename and path at which to save the generated image.
    vector_args: dict
        A dictionary of arguments to be passed to the vector generation function
    map_quality: int
        An integer roughly equivalent to zoom level (but not neccesrily the same) used to
        determine the resolution of basemap imagery to use.
    color: bool
        Flag indicating if the hillshade basemap should be drawn in color
        (using the 'geo' colormap), or grayscale.
    map_bounds: dict
        Dictionary of map bounds, in latitude/longitude (north, south, east, west)
    sid: str
        The session ID for which this process is generating a map image.
    insar_params: dict, optional
        Dictionary of parameters for InSAR overlay generation, or None for no insar.
    ifr_info: str
        JSON encoded two-item list of (filename, sensor) for the interferogram to be
        displayed, or empty strings if no interferogram is to be displayed.
    """

    # These lists come in as JSON encoded, so we need to parse them out to individual variables.
    ifr_file, ifr_sensor = json.loads(ifr_info)
    amp_file, amp_sensor = json.loads(amp_info)

    # has to be imported at time of use to work with uwsgi (and perhaps others)
    try:
        import pygmt
    except Exception:
        os.environ["GMT_LIBRARY_PATH"] = "/usr/local/lib"
        import pygmt

    # Unpack the bounds dictionary into a list sutable for GMT use.
    bounds = [
        map_bounds["west"],
        map_bounds["east"],
        map_bounds["south"],
        map_bounds["north"],
    ]

    date_from = parse(vector_args["date_from"]).date()
    date_to = parse(vector_args["date_to"]).date()

    # Create a GMT basemap using the mercator projection, with a width of 8 inches
    # (set in the fig_width variable below) and a frame on all four sides.
    fig = pygmt.Figure()
    fig_width = 8
    fig.basemap(projection=f"M{fig_width}i", region=bounds, frame=("WeSn", "afg"))

    # Figure out how many inches on the output image correspond to one degree of longitude
    # (roughly) for the purposes of drawing the vector scale, which is specified in units
    # of degress length.
    in_per_deg_lon = fig_width / (bounds[1] - bounds[0])

    grid = get_hillshade(bounds, sid, map_quality)
    utils.setDiskValue("GenStat", "Drawing basemap...", sid)
    if not isinstance(grid, (list, tuple)):
        grid = [
            grid,
        ]

    if color:
        pygmt.makecpt(cmap="geo")
        water_color = "#d4f1f9"
    else:
        pygmt.makecpt(cmap="gray", series=(-10000, 20000), background="i")
        water_color = "#FFFFFF"

    # There may be multiple hillshade images to plot, especially if using high-resolution
    # imagery from elevation.alaska.gov
    img_count = len(grid)
    for idx, image in enumerate(grid):
        if img_count > 1:
            # Percent complete with this stage
            pc = int(round(((idx + 1) / img_count) * 100))
            utils.setDiskValue("GenStat", f"Drawing basemap ({pc}%)", sid)

        fig.grdimage(
            image,
            dpi=150,
            shading=True,
            # nan_transparent = True # No longer works with shading specified,
            # even though it is needed.
        )

    # Cleanup basemap files
    for image in grid:
        if os.path.exists(image):
            img_dir = os.path.dirname(image)
            try:
                shutil.rmtree(img_dir)
            except Exception:
                try:
                    os.unlink(image)
                except Exception as e:
                    # Oh well, we tried!
                    print(f"Unable to remove {image}. {e}")

    fig.coast(rivers=f"r/2p,{water_color}", water=water_color, resolution="f")

    # Plot Interferogram (if specified)
    if ifr_file or amp_file:
        utils.setDiskValue("GenStat", "Drawing Overlay...", sid)

        file = ifr_file or amp_file
        sensor = ifr_sensor or amp_sensor
        img_type = 'kml' if ifr_file else 'amp'

        file_req_path = os.path.join(sensor, img_type, file)
        source = Path(os.path.join(config.DATA_PATH, file_req_path)).resolve()

        if not str(source).startswith(config.DATA_PATH):
            raise FileNotFoundError

        file_type = source.suffix

        # Load the specified interferogram and bounds.
        with tempfile.NamedTemporaryFile("wb", suffix=".tiff", delete=False) as tiff:
            tiff_filename = tiff.name
            raw_tiff = None

            if file_type == '.kmz':
                png, location, _ = interferograms.process_kmz_file(ifr_sensor, ifr_file, True)
                bounds = [
                    location["SW"]["lng"],
                    location["NE"]["lat"],
                    location["NE"]["lng"],
                    location["SW"]["lat"],
                ]
                tiff_bytesio = utils.png_to_tiff(png, bounds)
                tiff.write(tiff_bytesio.read())
            elif file_type in ['.tif', '.tiff']:
                raw_tiff = source
            elif source.is_dir():
                raw_tiff = next(source.glob("*.tif*"))

            tiff.close()

            if raw_tiff is not None:
                utils.process_tiff(
                    raw_tiff, tiff_filename, "+proj=longlat +datum=WGS84 +lon_wrap=180"
                )

        # Draw the now-georeferenced interferogram on our map image.
        fig.grdimage(grid=tiff_filename, nan_transparent="black")

        try:
            os.unlink(tiff_filename)
        except FileNotFoundError:
            pass  # Nothing to remove

    # Plot insar velocities field (if specified)
    if insar_params is not None:
        insar._gmt_map_insar(fig, sid, insar_params)

    # Plot the earthquakes (if any)
    quake_data = json.loads(vector_args["quakes"])
    quake_df = pd.DataFrame.from_dict(quake_data)

    if quake_df.size > 0:
        utils.setDiskValue("GenStat", "Plotting Earthquakes...", sid)
        quake_df["x"] = quake_df.apply(lambda x: x.location[1], axis=1)
        quake_df["y"] = quake_df.apply(lambda x: x.location[0], axis=1)
        quake_df["sizes"] = quake_df.apply(lambda x: 0.125 * math.exp(0.5 * x.magnitude), axis=1)

        fig.plot(
            x=quake_df.x,
            y=quake_df.y,
            size=quake_df.sizes,
            style="cc",
            color="white",
            pen="black",
        )

    utils.setDiskValue("GenStat", "Plotting Stations...", sid)
    if not stations:
        stations.load()

    from . import wingdbstub

    station_data = pd.DataFrame.from_dict(stations, orient="index")

    # Filter by station type
    station_data = station_data[station_data['type'].isin(sta_types)]

    # Figure out circle color and pen color
    try:
        station_data["color"] = station_data.apply(
            lambda row: 0 if row.type == "continuous" else 1, axis=1
        )
    except Exception as e:
        app.logger.exception(str(e))
        pass

    # Split the data frame into two so we can vary the pen color
    # seperately from the fill color
    gnss_stations = station_data[station_data['type'] != 'seismic']
    seis_stations = station_data[station_data['type'] == 'seismic']
    sdata_red = gnss_stations[gnss_stations.has_tilt]
    sdata_white = gnss_stations[~gnss_stations.has_tilt]

    if len(sdata_red) > 0:
        fig.plot(
            x=sdata_red.lng,
            y=sdata_red.lat,
            style="c0.25c",
            fill=sdata_red.color,
            cmap="73/0/244,12/218/59",
            pen="1p,red",
        )

    if len(sdata_white) > 0:
        fig.plot(
            x=sdata_white.lng,
            y=sdata_white.lat,
            style="c0.25c",
            fill=sdata_white.color,
            cmap="73/0/244,12/218/59",
            pen="1p,white",
        )

    if len(seis_stations) > 0:
        fig.plot(
            x=seis_stations.lng,
            y=seis_stations.lat,
            style="t0.25c",  # 't' for triangle, size of 0.25 cm
            fill="black",  # Color the triangle black
        )

    fig.text(
        x=station_data.lng,
        y=station_data.lat,
        text=station_data.disp_name.tolist(),
        font="7p,Helvetica-Bold,black",
        fill="white@35",
        offset="j2.5p",
        justify="BL",
    )

    # Plot the vectors
    if 'continuous' in sta_types:
        utils.setDiskValue("GenStat", "Drawing vectors (calculating)...", sid)
        vector_data = data_utils._get_vector_data(
            vector_args["station"],
            vector_args["baseline"],
            date_from,
            date_to,
            float(vector_args["zoom"]),
            int(vector_args["scale"]),
        )

        xy_vectors = pd.DataFrame.from_dict(
            [
                {
                    "x": numpy.array((x[0]["lng"], x[1]["lng"]), dtype=numpy.float32),
                    "y": numpy.array((x[0]["lat"], x[1]["lat"]), dtype=numpy.float32),
                }
                for x in vector_data["xy"]
            ]
        )
        z_vectors = pd.DataFrame.from_dict(
            [
                {
                    "x": numpy.array((x[0]["lng"], x[1]["lng"]), dtype=numpy.float32),
                    "y": numpy.array((x[0]["lat"], x[1]["lat"]), dtype=numpy.float32),
                }
                for x in vector_data["z"]
            ]
        )

        utils.setDiskValue("GenStat", "Drawing vectors (xy)...", sid)
        for idx, row in xy_vectors.iterrows():
            fig.plot(x=row.x, y=row.y, pen="1.75p,51/0/255+ve0.12i+g51/0/255+a50")

        utils.setDiskValue("GenStat", "Drawing vectors (z)...", sid)
        for idx, row in z_vectors.iterrows():
            fig.plot(x=row.x, y=row.y, pen='1.75p,254/80/0+ve0.12i+bt')

        utils.setDiskValue("GenStat", "Drawing vector scale...", sid)

        scale_deg_len = vector_data["scale"] / (111320 * numpy.cos(bounds[3] * (numpy.pi / 180)))
        scale_in_len = scale_deg_len * in_per_deg_lon

        # some labels
        date_range = f"{date_from.strftime('%m/%d/%y')} - {date_to.strftime('%m/%d/%y')}"
        x_offset_cm = 0.025
        y_offset_p = 5
        fig.text(
            position="TL",
            text=date_range,
            fill="white@35",
            font="10p,Helvetica",
            justify="TL",
            offset=f"{x_offset_cm}c/-{y_offset_p}p",
            clearance="40p/142%",
        )

        # And the scale bar itself
        # Convert inches to cm, then add appropriate margins.
        key_offset_x = scale_in_len * 2.54 + 3 * x_offset_cm
        fig.text(
            position="TL",
            justify="ML",
            text="1 cm/year",
            offset=f"{key_offset_x}c/-19p",
            font="10p,Helvetica",
        )

        # Plot the scale bar as an inset map of the proper size so we can position it
        # more easily
        fig_top = y_offset_p + 10
        with fig.inset(
            position=f"jTL+o{x_offset_cm}c/{fig_top}p",
            region=[0, 1, -0.5, 0.5],
            projection=f"X{scale_in_len}i/10p",
            box=False,
        ):
            fig.plot(x=[0, 1], y=[0, 0], pen="2.5p,51/0/255+ve0.12i+g51/0/255+a50")

    # fig.show(method = "external")
    fig.savefig(file_path)
    app.logger.debug(f"Saved image to: {file_path}")
    utils.setDiskValue("GenStat", "DONE", sid)


@app.route("/map/get_image")
def download_map_image():
    """
    Download the map image previously generated in a seperate process.

    ARGUMENTS
    ---------
    MapImage: str (optional, default: use session value)
        The filename of the image to be downloaded.

    RETURNS
    -------
    image: flask.response
        Flask response object containing the generated PDF for download.
    """

    file_name = flask.request.args.get("file", flask.session.get("MapImage"))

    if file_name is None:
        app.logger.error("No file name provided, and nothing in session")
        return "No file name provided", 404

    # Just in case the user is trying to send us something...bad
    file_name = secure_filename(file_name)

    file_dir = utils.getDiskValue("MapFilePath", default=TEMP_DIR.name)
    app.logger.debug(f"Loading image from: {file_dir}/{file_name}")
    return flask.send_from_directory(
        file_dir, file_name, as_attachment=True, download_name="MapImage.pdf"
    )


@app.route("/list_stations")
def list_stations():
    """
    Get a list of station information

    ARGUMENTS
    ---------
    age: int
        Only return stations that have received data within this many years

    RETURNS
    -------
    stns: flask.response
        JSON encoded dictionary of station information dictionaries.
    """
    max_age = int(flask.request.args.get("age", -1))
    if max_age < 0:
        max_age = 2 if flask.session.get("public", False) else None

    if max_age is None:
        if not utils.stations:
            utils.stations.load()
        stns = utils.stations
    else:
        stns = utils.load_stations(max_age)

    return flask.jsonify(stns)


@app.route("/downloadGPSData")
def download_gps_data():
    """
    Download a CSV file of the full GPS data.

    ARGUMENTS
    ---------
    station: str
        The station to download data for
    baseline: str
        The baseline station to reference the data against.
    from: date
        The start date for the data
    to: date
        The end date for the data

    RETURNS
    -------
    csv_file: flask.response
        Flask response object containing the CSV download.
    """
    station = flask.request.args["station"]
    baseline = flask.request.args["baseline"]
    date_from = flask.request.args["from"]
    date_to = flask.request.args["to"]
    fmt = flask.request.args.get('format', 'NEU')
    rtu_lat = flask.request.args.get('RTULat')
    rtu_lon = flask.request.args.get('RTULon')

    data = get_graph_data(False, station, baseline, date_from, date_to, fmt, rtu_lat, rtu_lon)
    df = pd.DataFrame(data)

    # This column makes no sense, and wouldn't work, in a CSV file
    df.drop(columns="date_obj", inplace=True)
    column_renames = {'dates': 'date'}
    if fmt == 'RTU':
        column_renames.update(
            {
                'NS': 'Radial',
                'EW': 'Transverse',
                'NSE': 'Radial Error',
                'EWE': 'Transverse Error',
            }
        )

    df = df.rename(columns=column_renames)

    csv_file = BytesIO()
    df.to_csv(csv_file, index=False)

    csv_file.seek(0)
    filename = f"{station}_{baseline}_GPS_{date_from}-{date_to}.csv"
    return flask.send_file(
        csv_file, mimetype="text/csv", as_attachment=True, download_name=filename
    )


@app.route("/get_graph_data")
def web_get_graph_data():
    station = flask.request.args.get("station")
    baseline_station = flask.request.args.get("baseline")
    plot_format = flask.request.args.get('format', 'NEU')
    rtu_lat = flask.request.args.get('RTULat')
    rtu_lon = flask.request.args.get('RTULon')

    return get_graph_data(
        station=station,
        baseline_station=baseline_station,
        fmt=plot_format,
        rtu_lat=float(rtu_lat),
        rtu_lon=float(rtu_lon),
    )


def get_graph_data(
    as_json=True,
    station=None,
    baseline_station=None,
    date_from=None,
    date_to=None,
    fmt='NEU',
    rtu_lat=None,
    rtu_lon=None,
):
    """
    Retreive and format GPS data for plotting

    ARGUMENTS
    ---------
    as_json: bool
        Determines if the output should be JSON encoded for web request responses
        or returned "raw" as python objects.
    station: str
        The station to retrieve GPS data for
    baseline: str
        Baseline station to reference GPS against
    date_from: date
        Start date for the data
    date_to: date
        End date for the data.

    RETURNS
    -------
    results: flask.response | dict
        The GPS movement data, either as a dictionary for use in other functions, or a
        flask.response object containing a JSON encoded version of the dictionary, depending
        on the value of the as_json argument.
    """

    # Deprecated usage: call directly from the web.
    if station is None or baseline_station is None:
        station = flask.request.args.get("station")
        baseline_station = flask.request.args.get("baseline")

    geodvel = baseline_station == "NOAM (geodvel)"
    if not baseline_station or geodvel:
        # get a "base" location for this station. Plot relative to the "offical" location
        bpd = stations[station]
        base_point = (bpd["lng"], bpd["lat"], 0, 0, 0, 0)
        baseline_station = None
    else:
        base_point = None

    # in km/year
    data = load_db_data(
        station,
        ref_station=baseline_station,
        ref_point=base_point,
        date_from=date_from,
        date_to=date_to,
    )

    if geodvel and len(data['date_obj']) > 0:
        # In mm/year
        neu_plate_vel = utils.calc_neu_vel(bpd["lat"], bpd["lng"], 0, 'noam')
        neu_plate_vel /= 1000000  # convert to km/year to be consistant
        # get an array of relative plate position (km offset) for the same timeperiod
        # as the data, based on its velocity.
        # First we need dates as decimal years so we can multiply by the km/y value
        # to get the offset value
        dates_series = pd.Series(data['date_obj']).astype('datetime64[ns]')
        decimal_years = dates_series.dt.year + (dates_series.dt.dayofyear - 1) / (
            365 + dates_series.dt.is_leap_year
        )

        # Start the series from zero - we only need to know how much
        # time has passed, not what the actual date is here.
        decimal_years -= decimal_years[0]

        # Calculate plate offset
        plate_n = neu_plate_vel[0, 0] * decimal_years
        plate_e = neu_plate_vel[1, 0] * decimal_years

        # Remove plate velocity for N and East components
        data['NS'] -= plate_n
        data['EW'] -= plate_e

    if fmt == 'RTU':
        #  Convert from NEU to RTU
        sta_info = utils.stations[station]
        sta_lat = sta_info['lat']
        sta_lon = sta_info['lng']

        geod = Geod(ellps="WGS84")
        forward_azimuth, back_azimuth, dist = geod.inv(rtu_lon, rtu_lat, sta_lon, sta_lat)
        r, t = data_utils.rotate_ne_rt(
            numpy.asarray(data['NS']), numpy.asarray(data['EW']), back_azimuth % 360
        )

        data['NS'] = r.tolist()
        data['EW'] = t.tolist()

    # Remove average of each component to get deviation from average
    try:
        avg_ns = sum(data["NS"]) / len(data["NS"])
        avg_ew = sum(data["EW"]) / len(data["EW"])
        avg_ud = sum(data["UD"]) / len(data["UD"])
        data["EW"] = [(x - avg_ew) * 100000 for x in data["EW"]]
        data["NS"] = [(x - avg_ns) * 100000 for x in data["NS"]]
        # UP/Down is in meters, not kilometers, so only 100 to convert.
        data["UD"] = [(x - avg_ud) * 100 for x in data["UD"]]
    except ZeroDivisionError:
        pass  # No data, just return an empty array

    if as_json:
        return flask.jsonify(data)
    else:
        return data


@app.route("/get_vector_data")
def get_vector_data():
    """
    Format and return the data needed to draw vectors on the map

    ARGUMENTS
    ---------
    station: str
        The station for which to draw vectors for
    baseline: str
        Baseline station to reference GPS data against
    date_from: date
        Start date for data of interest
    date_to: date
        End date for data of interest
    zoom: int
        Map zoom level (used to determine lat/lon length of vectors)
    scale: int (optional, default 10)
        Scaling factor to shrink/enlarge vectors by

    RETURNS
    -------
    data: flask.response
        The vector data, encoded in JSON format.
    """
    station = flask.request.args["station"]
    baseline_station = flask.request.args["baseline"]

    try:
        date_from = parse(flask.request.args["date_from"]).date()
    except ParserError:
        date_from = None

    try:
        date_to = parse(flask.request.args["date_to"]).date()
    except ParserError:
        date_to = datetime.now().date()

    zoom_level = float(flask.request.args["zoom"])
    scale = int(flask.request.args.get("scale", 10))

    data = data_utils._get_vector_data(
        station, baseline_station, date_from, date_to, zoom_level, scale
    )
    return flask.jsonify(data)


@app.route("/downloadTiltData")
def download_tilt_data():
    """
    Download tilt data as a CSV

    ARGUMENTS
    ---------
    from: date
        Date to get tilt data from
    to: date
        Date to get tilt data to
    station: str
        The station to get tilt data for

    RETURNS
    -------
    csv: flask.response
        CSV of the retrieved tilt data for download.
    """
    date_from = parse(flask.request.args["from"])
    date_to = parse(flask.request.args["to"])
    station_name = flask.request.args["station"]

    data = data_utils._get_tilt_data(date_from, date_to, station_name, raw=True)

    df = pd.DataFrame()
    for key in ["dates", "x_tilt", "y_tilt", "temps", "radius", "theta"]:
        df[key] = data[key]

    csv_file = BytesIO()
    df.to_csv(csv_file, index=False)

    csv_file.seek(0)
    filename = f"{station_name}_Tilt_{date_from}-{date_to}.csv"
    return flask.send_file(
        csv_file, mimetype="text/csv", as_attachment=True, download_name=filename
    )


@app.route("/get_tilt_data")
def get_tilt_data():
    """
    Web request endpoint to get tilt data

    ARGUMENTS
    ---------
    from: date
        Date to get tilt data from
    to: date
        Date to get tilt data to
    station: str
        Station to get tilt data for

    RETURNS
    -------
    tilt_data: JSON
        JSON encoded dictionary containing the requested tilt data
    """
    date_from = parse(flask.request.args["from"])
    date_to = parse(flask.request.args["to"])
    station_name = flask.request.args["station"]

    return data_utils._get_tilt_data(date_from, date_to, station_name)


######## Hillshade code ############ # noqa: E266


def get_hillshade(map_bounds, sid, zoom=9):
    """
    Figure out what hillshade data to use, based on "zoom".
    0-7: Low resolution, @earth_relief_15s
    8-10: Medium resolution, @strm_relief_01s
    >10: High resolution, download from elevation.alaska.gov

    ARGUMENTS
    ---------
    map_bounds: list
        Map bounds (W,E,S,N). Used if downloading high-resolution DEM's from elevation.alaska.gov
    sid: str
        The session ID to associate this hillshade request with.
    zoom: int
        A "zoom level" value indicating the resolution of imagery we want.

    RETURNS
    -------
    hillshade_files: list
        list of one or more hillshade files to be used by GMT grdimage to draw the basemap.
    """
    app.logger.info("Getting hillshade info for zoom", zoom)
    utils.setDiskValue("GenStat", "Getting Hillshade files", sid)

    resolution = None
    hillshade_files = None
    if zoom <= 7:
        resolution = "15s"
    elif zoom < 10:
        resolution = "01s"

    if resolution is not None:
        data = pygmt.datasets.load_earth_relief(
            resolution, region=map_bounds
        )  # Check availability
        if not (data == 0).all():
            hillshade_files = [f"@earth_relief_{resolution}"]
        else:
            app.logger.warning(
                "Unable to get lower resolution data for the selected area. "
                "Falling back to high-resolution data."
            )

    if hillshade_files is None:
        # For higher zooms, use elevation.alaska.gov data
        utils.setDiskValue("GenStat", "Downloading Hillshade Data (0%)", sid)

        # reformat bounds to [xmin,ymin,xmax,ymax] for download
        download_bounds = [map_bounds[0], map_bounds[2], map_bounds[1], map_bounds[3]]

        all_files = _download_wcs(download_bounds, sid)

        out_files = _process_files(all_files, download_bounds)

        hillshade_files = out_files

    return hillshade_files


def _download_wcs(bounds, sid):
    URL_BASE = 'https://geoportal.dggs.dnr.alaska.gov/arcgis/services/elevation/IFSAR_DSM/ImageServer/WCSServer'

    x_min = bounds[0]
    x_max = bounds[2]
    y_min = bounds[1]
    y_max = bounds[3]

    # How many rows and columns should we split the image into?
    # More=higher resolution, but slower
    # Each image will be the specified width and height
    num_rows = 4
    num_cols = 4

    y_nodes = numpy.linspace(y_min, y_max, num_cols + 1)
    x_nodes = numpy.linspace(x_min, x_max, num_rows + 1)

    xv, yv = numpy.meshgrid(x_nodes, y_nodes)
    xv2 = numpy.roll(xv, -1, 1)
    yv2 = numpy.roll(yv, -1, 0)

    grids = numpy.stack([xv, yv, xv2, yv2], axis=2)[:num_rows, :num_cols, :]
    grids = grids.reshape(-1, grids.shape[2])

    ARGS = {
        'COVERAGE': 'IFSAR_DSM_1',
        'SERVICE': 'WCS',
        'REQUEST': 'GetCoverage',
        'CRS': 'EPSG:4326',
        'RESPONSE_CRS': 'EPSG:4326',
        'VERSION': '1.0.0',
        'WIDTH': 2000,
        'HEIGHT': 1500,
        'FORMAT': 'geoTIFF',
    }

    img_dir = os.path.join(tempfile.gettempdir(), 'hillshadeTemp')
    os.makedirs(img_dir, exist_ok=True)
    files = []
    total_loops = grids.shape[0]
    for idx, bound in enumerate(grids):
        # pc = ((idx + 1) / total_loops) * 100
        utils.setDiskValue(
            'GenStat', f"Downloading hillshade files ({idx + 1}/{total_loops})...", sid
        )
        # self._update_status({
        # 'status': f"Downloading hillshade files ({idx + 1}/{total_loops})...",
        # 'progress': pc
        # })

        ARGS['BBOX'] = ",".join((str(x) for x in bound))
        filename = os.path.join(img_dir, f"segment_{idx}.tiff")
        logging.info("Downloading hillshade files")

        resp = requests.get(URL_BASE, params=ARGS)
        if resp.status_code != 200:
            logging.warning(
                f"Unable to fetch hillshade files for region. Server returned {resp.status_code}"
            )
            print(resp.status_code)
            print(resp.text)
            return []

        with open(filename, 'wb') as f:
            f.write(resp.content)

        files.append(filename)

    return files


def _download_elevation(bounds, sid):
    """
    Download High-resolution DEM files from elevation.alaska.gov.
    These come in as a zip file containing all images needed for the requested area.
    As there is no way to know what images are included in the zip file prior to download,
    we have to do the download every time - there is no obvious way to have a local cache.

    As the files are rather large, this download can be fairly slow.

    TODO: investigate the possiblility of caching the files along with their bounds, and
    somehow using this data to see if we have full coverage, or if a new download is needed.

    ARGUMENTS
    ---------
    bounds: list
        The map bounds for which we need to get DEM coverage.
    sid: str
        The session ID this request is associated with, to store progress information.

    RETURNS
    -------
    tiff_dir: str
        The directory in which the downloaded and decompressed GeoTIFF files are stored in.
    """

    if bounds[0] < -180 or bounds[2] > 180 or bounds[0] > bounds[2]:
        # Crossing dateline. Need to split request.
        bounds = list(bounds)
        bounds2 = bounds.copy()
        bounds3 = bounds.copy()
        # Make bounds be only west of dateline
        if bounds3[0] < 0:
            bounds3[0] += 360
        bounds3[2] = 180

        bounds2[0] = -180
        if bounds2[2] > 0:
            bounds2[2] -= 360

        poly2 = Polygon.from_bounds(*bounds2)
        poly3 = Polygon.from_bounds(*bounds3)
        bounds_list = [shapely_geojson.dumps(poly3), shapely_geojson.dumps(poly2)]
    else:
        poly = Polygon.from_bounds(*bounds)
        bounds_list = [
            shapely_geojson.dumps(poly),
        ]

    ids = 151  # DSM hillshade
    URL_BASE = "https://elevation.alaska.gov"
    list_url = f"{URL_BASE}/query.json"
    download_url = f"{URL_BASE}/download"  # for download
    est_size = 0  # So we can show progress
    logging.info("Downloading hillshade files")
    tempdir = tempfile.gettempdir()  # Where to store the downloaded files
    zf_path = os.path.join(tempdir, "custom_download.zip")
    tiff_dir = os.path.join(tempdir, "tiffs")  # Where to store the decompressed TIFF files
    os.makedirs(tiff_dir, exist_ok=True)

    loaded_bytes = 0

    for geojson in bounds_list:  # Only one or two items, so not bad.
        # get file listings
        try:
            req = requests.post(
                list_url,
                data={
                    "geojson": geojson,
                },
            )
        except requests.exceptions.ConnectionError:
            logging.warning("Connection error attempting to get file listings")
            continue

        if req.status_code != 200:
            logging.warning(f"Unable to get file listings. Server returned {req.status_code}")
        else:
            files = req.json()
            try:
                file_info = next((x for x in files if x["dataset_id"] == ids))
            except StopIteration:
                logging.warning("Requested dataset info not found in server response")
            else:
                logging.debug(str(file_info))
                est_size += file_info.get("bytes", -1)

    # Yes, this is the same loop again, but this time we are actually downloading the files,
    # not just getting information about them. Small loop, so repeating it isn't bad, though
    # it is a bit of a code smell. What it does allow us to do is have the complete size of all
    # files before we start the download, so we know the total progress.
    for geojson in bounds_list:
        req = requests.get(download_url, params={"geojson": geojson, "ids": ids}, stream=True)
        if req.status_code != 200:
            logging.warning(
                f"Unable to fetch DEM files for region. Server returned {req.status_code}"
            )
            print(req.status_code)
            print(req.text)
            continue

        with open(zf_path, "wb") as zf:
            for chunk in req.iter_content(chunk_size=None):
                if chunk:
                    bytes_written = zf.write(chunk)
                    loaded_bytes += bytes_written
                    pc = int(round((loaded_bytes / est_size) * 100))
                    if pc % 5 == 0:
                        utils.setDiskValue("GenStat", f"Downloading Hillshade Data ({pc}%)", sid)

        logging.info(f"Downloaded {loaded_bytes} bytes of hillshade files")
        utils.setDiskValue("GenStat", "Hillshade Data Downloaded. Decompressing...", sid)

        # Pull out the various tiff files needed
        logging.info("Extracting tiffs")

        with zipfile.ZipFile(zf_path, "r") as zf:
            for file in (x for x in zf.namelist() if x.endswith("zip")):
                logging.info(f"Reading {file}")
                zf_data = BytesIO(zf.read(file))
                with zipfile.ZipFile(zf_data, "r") as zf2:
                    for tiffile in (x for x in zf2.namelist() if x.endswith(".tif")):
                        if os.path.isfile(os.path.join(tiff_dir, tiffile)):
                            continue  # already extracted, move on
                        logging.info(f"Extracting {tiffile}")
                        zf2.extract(tiffile, path=tiff_dir)

    return tiff_dir


def _process_files(all_files, merge_bounds, proj=None):
    """
    Apply gdal_merge (if needed) to merge multiple files into one, and trim/warp to our output area.

    ARGUMENTS
    ---------
    all_files: list
        List of original/raw files to be processed
    warp_bounds: list
        Output map bounds, in gdal_merge compatible format (ul_lr)
    proj: str (optional)
        The EPSG srs code of the source files, if not specified in the input files.

    RETURNS
    -------
    files: list
        List of processed files to be used for the output map
    """
    osgeo.gdal.AllRegister()  # Why? WHY!?!? But needed...

    in_path, in_ext = os.path.splitext(all_files[0])
    in_path = os.path.dirname(in_path)
    if len(all_files) > 1:
        merged_file = os.path.join(in_path, "combined_image.tiff")
        out_file = os.path.join(in_path, "combined_warped_image.tiff")
        merge_args = ["myScript.py", "-o", merged_file]
        merge_args += all_files
        gdal_merge(merge_args)
    else:
        merged_file = all_files[0]

    kwargs = {
        "dstSRS": "EPSG:4326",
        "multithread": True,
        "warpOptions": ["NUM_THREADS=ALL_CPUS"],
        "creationOptions": ["NUM_THREADS=ALL_CPUS"],
        "outputBounds": merge_bounds,
    }

    if proj is not None:
        kwargs["srcSRS"] = proj

    osgeo.gdal.Warp(out_file, merged_file, **kwargs)
    return [out_file]


@app.route("/image/<station>")
def get_station_image(station):
    """
    Get the image for the specified station

    ARGUMENTS
    ---------
    station: str
        The station for which to get an image

    RETURNS
    -------
    img: flask.response
        A flask response containing the requested station image, or 404 if not found.
    """
    station_name = secure_filename(station).upper()
    IMG_PATH = os.path.join(STATION_IMG_PATH, f"*/*{station_name}*")
    img_files = glob.glob(IMG_PATH)
    if not img_files:
        return flask.abort(404)

    img_file = None
    for file in img_files:
        if "0_misc" not in file:
            img_file = file
            break

    if img_file is None:
        return flask.abort(404)

    return flask.send_file(img_file)


@app.route("/image/<station>", methods=["PUT"])
def put_station_image(station):
    if "image" not in flask.request.files:
        return flask.abort(400)
    file = flask.request.files["image"]
    if file.filename == "":
        return flask.abort(400)

    file_ext = file.filename.rsplit(".", 1)[1].lower()
    station_name = secure_filename(station).upper()

    # Check for existing image
    IMG_PATH = os.path.join(STATION_IMG_PATH, f"*/*{station_name}*")
    existing_files = glob.glob(IMG_PATH)
    if existing_files:
        for cur_file in existing_files:
            os.unlink(cur_file)

    upload_dir = os.path.join(STATION_IMG_PATH, "Uploads")
    os.makedirs(upload_dir, exist_ok=True)

    IMG_PATH = os.path.join(upload_dir, f"{station_name}.{file_ext}")
    file.save(IMG_PATH)

    return "OK"


@app.route("/sensors")
@app.route("/api/gnss/sensors")
@doc(
    tags=["GNSS"],
    description="GeoJSON list of GNSS stations",
    params={
        "volcano": {
            "description": "GeoDIVA volcano ID, used to limit returned stations "
            "to a single volcano. Optional",
            "in": "query",
            "type": "string",
            "required": False,
        },
    },
    responses={
        200: {
            "description": "GeoJSON feature collection of stations with data in the past 7 days",
        },
    },
)
@cross_origin()
def get_sensors():
    volcano = flask.request.args.get("volcano")

    SQL = """
    SELECT
        numid as id, name,St_AsText(location),'{}'
    FROM stations
    WHERE EXISTS (SELECT
        1 FROM {}
        WHERE station=stations.id
        AND {}>now()-'8 days'::interval
        LIMIT 1
    )
    """

    args = []
    if volcano:
        SQL += " AND %s=ANY(volcano)"
        args.append(volcano)

    gnss_args = ("GNSS", "gps_data", "read_date")
    tilt_args = ("Tilt", "tilt_data", "read_time")

    with utils.db_cursor() as cursor:
        gnss_sql = SQL.format(*gnss_args)
        cursor.execute(gnss_sql, args)
        gnss = cursor.fetchall()

        tilt_sql = SQL.format(*tilt_args)
        cursor.execute(tilt_sql, args)
        tilt = cursor.fetchall()

    features = gnss + tilt
    df = pd.DataFrame(features, columns=["id", "name", "location", "type"])
    df = df.set_index("id", drop=True)
    gs = geopandas.GeoSeries.from_wkt(df["location"])
    gdf = geopandas.GeoDataFrame(df, geometry=gs, crs="EPSG:4326")
    gdf = gdf.drop(columns="location")
    response = flask.Response(
        gdf.to_json(),
        mimetype="application/json",
    )
    return response


docs.register(get_sensors)
