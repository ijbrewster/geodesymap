import json
import os
import zipfile

from pathlib import Path
from xml.etree import ElementTree

import flask
import pandas

from werkzeug.utils import secure_filename

from osgeo import osr

osr.DontUseExceptions()  # Must be called immediately to avoid warnings.

from . import app
from . import config, utils
from .interferograms import process_kmz_file
from .insar import get_insar, get_insar_image, get_insar_png, get_insar_location


@app.route("/fileOpts/<ftype>")
def get_file_opts(ftype):
    bounds = flask.request.args.get("bounds")
    opts = get_overlay_idents(bounds, ftype)
    return json.dumps(opts)


def check_location(bounds, location):
    left = location["SW"]["lng"]
    right = location["NE"]["lng"]
    if left > right:
        left -= 360

    top = location["NE"]["lat"]
    bottom = location["SW"]["lat"]
    inview = (
        left < bounds["east"]
        and right > bounds["west"]
        and top > bounds["south"]
        and bottom < bounds["north"]
    )
    return inview


def get_overlay_idents(bounds=None, overlay_type="kml"):
    result = {}
    if bounds:
        bounds = json.loads(bounds)
        # Make everything negitive
        if bounds["west"] > 0:
            bounds["west"] -= 360
        if bounds["east"] > 0:
            bounds["east"] -= 360

    file_base = Path(config.DATA_PATH)
    for sensorDir in sorted(file_base.glob(f"*/{overlay_type}")):
        sensor = sensorDir.parts[-2]
        result[sensor] = []
        for sar_item in sensorDir.iterdir():
            if not sar_item.is_dir():
                if sar_item.suffix != '.h5' or not sar_item.name.startswith("timeseries_"):
                    continue  # Not a file of interest

                # sar_item is a timeseries_ h5 file
                if bounds:
                    try:
                        location = get_insar_location(sar_item)
                    except Exception as e:
                        continue
                    if not check_location(bounds, location):
                        continue
                    display_name = (
                        sar_item.stem.replace("timeseries_", "").replace("_", " ").title()
                    )
                    result[sensor].append((display_name, sar_item.name))

            else:
                if bounds:
                    try:
                        sample_file: Path = max(sar_item.glob("*/[!.]*"))
                    except (StopIteration, ValueError):
                        # No files for this path/frame. Odd that we have the directory...
                        continue

                    if sample_file.is_dir():
                        try:
                            location = get_tiles_bounds(sample_file)
                        except FileNotFoundError:
                            continue
                    else:
                        ftype = sample_file.suffix
                        try:
                            if ftype == ".kmz":
                                _, location, _ = process_kmz_file(sensor, str(sample_file))
                            elif ftype == ".tif":
                                location = utils.get_tiff_location(str(sample_file))
                            elif ftype == ".h5":
                                location = get_insar_location(sample_file)
                        except Exception as e:
                            app.logger.exception(e)
                            continue  # bad file

                    if not check_location(bounds, location):
                        continue

                display_name = sar_item.name.replace("path", "Path: ").replace("frame", " Frame: ")
                result[sensor].append((display_name, sar_item.name))

        if not result[sensor]:
            # No files for this sensor
            del result[sensor]
        else:
            result[sensor].sort(key=utils.pathFrameSort)

    return result


def get_tiles_bounds(tile_dir: Path) -> dict:
    meta_xml: Path = tile_dir / "tilemapresource.xml"
    if not meta_xml.is_file():
        raise FileNotFoundError("No metadata xml for tile directory")

    kml_tree = ElementTree.parse(meta_xml)
    kml_root = kml_tree.getroot()
    bounding_box = kml_root.find("BoundingBox")

    minx = float(bounding_box.attrib["minx"])
    miny = float(bounding_box.attrib["miny"])
    maxx = float(bounding_box.attrib["maxx"])
    maxy = float(bounding_box.attrib["maxy"])

    # make all longitudes negitive
    if minx > 0:
        minx -= 360
    if maxx > 0:
        maxx -= 360

    location = {
        "SW": {
            "lat": miny,
            "lng": minx,
        },
        "NE": {
            "lat": maxy,
            "lng": maxx,
        },
    }

    return location


@app.route("/overlay/<img_type>")
def get_overlay(img_type):
    overlay_file = flask.request.args["file"]
    sensor = flask.request.args["sensor"]

    file_req_path = os.path.join(sensor, img_type, overlay_file)
    file_path = Path(os.path.join(config.DATA_PATH, file_req_path)).resolve()
    if not Path(config.DATA_PATH) in file_path.parents:
        flask.abort(404)

    response = {}
    overlay_type = "img"
    if file_path.is_dir():
        overlay_type = "tiles"
        location = get_tiles_bounds(file_path)
        img_name = file_path.name
    else:
        file_type = file_path.suffix
        # Get file information
        if file_type == ".kmz":
            img_name, location, _ = process_kmz_file(sensor, str(file_path))
        elif file_type in (".tif", ".tiff"):
            img_name = os.path.basename(file_req_path)
            location = utils.get_tiff_location(str(file_path))
        elif file_type == ".h5":
            overlay_type = "scaledIMG"
            info = get_insar(str(file_path))
            location = info["coordinates"]
            img_name = info["img_name"]
            file_req_path = info["img_path"]
            img_name = info["img_name"]
            response.update(info)

    response["type"] = overlay_type
    response["coordinates"] = location
    response["img_path"] = file_req_path
    response["img_name"] = img_name

    return response


@app.route("/overlay/<img_type>/<img>")
def get_overlay_image(img_type, img):
    file = flask.request.args["path"]

    file = os.path.realpath(os.path.join(config.DATA_PATH, file))

    if not file.startswith(config.DATA_PATH):
        flask.abort(404)

    file_type = Path(file).suffix
    if file_type in [".tif", ".tiff"]:
        png = utils.tiff_to_png(file)
    elif file_type == ".kmz":
        with zipfile.ZipFile(file) as kmz:
            png = kmz.open(img)
    elif file_type == ".h5":
        png = get_insar_image(file)

    response = flask.send_file(png, mimetype="image/png", download_name=img)
    return response


@app.route("/overlay/files/<ftype>")
def list_overlay_files(ftype="kml"):
    result = {}
    sensor = flask.request.args["sensor"]
    identifier = flask.request.args["identifier"]

    filepath = Path(config.DATA_PATH, sensor, ftype, identifier)
    files = pandas.DataFrame(filepath.glob("*/[!.]*"), columns=["filepath"])

    files[["dates", "file"]] = pandas.DataFrame(
        files["filepath"].apply(lambda x: x.parts[-2:]).to_list()
    )
    try:
        files[["start", "end"]] = files["dates"].str.split("_", expand=True)
    except ValueError:
        files["start"] = files["dates"]
        files["end"] = files["dates"]

    files["drange"] = (
        pandas.to_datetime(files["start"]).dt.strftime("%m/%d/%Y")
        + " - "
        + pandas.to_datetime(files["end"]).dt.strftime("%m/%d/%Y")
    )
    for (start, end), records in files.groupby(["start", "end"]):
        drange = records.iloc[0]["drange"]
        files = records["file"].to_list()
        result[drange] = files

    return json.dumps(result)


@app.route("/overlay/geotiff/<ftype>")
def download_geotiff(ftype):
    file = flask.request.args["file"]
    sensor = flask.request.args["sensor"]

    if ftype == "ifr":
        ftype = "kml"

    source = Path(config.DATA_PATH, sensor, ftype, file).resolve()
    if not str(source).startswith(config.DATA_PATH):
        return flask.abort(404)

    file_type = source.suffix

    # Assume h5 files for insar if not specified and the specified source isn't a directory.
    if file_type == "" and not source.is_dir() and ftype == "insar":
        file_type = ".h5"

    if file_type == ".kmz":
        png, location, viewBoundScale = process_kmz_file(sensor, file, png_as_file=True)
        bounds = [
            location["SW"]["lng"],
            location["NE"]["lat"],
            location["NE"]["lng"],
            location["SW"]["lat"],
        ]
        tiff = utils.png_to_tiff(png, bounds)
    elif file_type == ".h5":
        png, location = get_insar_png(sensor, file)
        bounds = [
            location["SW"]["lng"],
            location["NE"]["lat"],
            location["NE"]["lng"],
            location["SW"]["lat"],
        ]
        tiff = utils.png_to_tiff(png, bounds)
    elif source.is_dir():
        #  Find the actual tiff. Should be at the top level directory
        try:
            tiff = next(source.glob("*.tif*"))
        except StopIteration:
            flask.abort(404)
    elif file_type in [".tif", ".tiff"]:
        tiff = source

    file_parts = file.split("/")
    try:
        download_name = f"{file_parts[0]}_{file_parts[1]}.tiff"
    except IndexError:
        download_name = f"{file}.tiff"

    return flask.send_file(tiff, as_attachment=True, download_name=download_name)


@app.route("/overlay/tiles/<zoom>/<x>/<y>")
def send_tile(zoom, x, y):
    tile_dir = flask.request.args["path"]
    tile_file = Path(tile_dir) / zoom / x / y

    return flask.send_from_directory(config.DATA_PATH, tile_file)


def make_overlay_path(*args):
    data_dir = Path(config.DATA_PATH)
    # Nested if statements so it falls out at the first missing argument,
    # any additional arguments are ignored.
    for arg in args:
        if not arg:
            break
        if arg == "ifr":
            arg = "kml"  # interferograms are stored in the KML directory
        data_dir /= secure_filename(arg)

    data_dir = data_dir.resolve()
    if not str(data_dir).startswith(config.DATA_PATH):
        raise FileNotFoundError

    return data_dir
