import flask
import json
import os
import sqlite3
import tempfile

from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from flask_apispec.extension import FlaskApiSpec

try:
    from . import config
except ImportError:
    import config

try:
    from . import wingdbstub  # noqa: F401
except ImportError:
    pass  # No remote debugging


home_dir = os.path.realpath(os.path.join(os.getcwd(), '..'))
os.environ["GMT_USERDIR"] = home_dir
os.environ['GMT_TMPDIR'] = home_dir
os.environ['PLOTLY_HOME'] = home_dir
os.environ["HOME"] = home_dir

# Load some config values from enviroment variables
ENV_CONFS = ["DATA_PATH", "STATION_IMG_PATH", "REDIS_HOST"]
for conf in ENV_CONFS:
    env_val = os.environ.get(conf)
    if env_val is not None:
        setattr(config, conf, env_val)

TEMP_DIR = tempfile.TemporaryDirectory()


app = flask.Flask(__name__)
app.config["TEMPLATES_AUTO_RELOAD"] = True
app.config["MAX_CONTENT_LENGTH"] = 2 * 1024 * 1024 * 1024  # 2 GB limit
app.config['MAX_FORM_MEMORY_SIZE'] = 10 * 1024 * 1024  # 10MB


# Create an APISpec
@app.route("/api/swagger.json")
def swagger_json():
    spec.options["basePath"] = flask.request.headers.get("X-Script-Name", "/")
    return flask.jsonify(spec.to_dict())


spec = APISpec(
    title="AVO Geodesy API",
    version="1.0.0",
    openapi_version="2.0",
    plugins=[MarshmallowPlugin()],
)

app.config["APISPEC_SPEC"] = spec
app.config["APISPEC_SWAGGER_URL"] = "/api/swagger.json"
app.config["APISPEC_SWAGGER_UI_URL"] = "/api"

docs = FlaskApiSpec(app)

app.secret_key = "Correct Horse Battery Staple Secret Key Code"
# app.use_x_sendfile = True

SQLITE_DB = "flask_session/session.db"
os.makedirs("flask_session", exist_ok=True)

# initalize a sqlite database for storing session data
conn = sqlite3.connect(SQLITE_DB)
cur = conn.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS session_info (sid, key, value)")
conn.commit()
conn.close()

# Make sure the home directory is set to a reasonable location (NOT /root)
if os.environ.get("HOME", "/root").startswith("/root"):
    home_dir = os.path.dirname(__file__)
    home_dir = os.path.realpath(os.path.join(home_dir, ".."))
    os.environ["HOME"] = home_dir
    app.logger.info(f"HOME DIR Set To: {os.environ.get('HOME')}")

# make sure /usr/local/bin is in path so orca can be found
if "/usr/local/bin" not in os.environ["PATH"]:
    os.environ["PATH"] += os.pathsep + "/usr/local/bin"
    app.logger.error("Added /usr/local/bin to PATH")

# Configure ORCA. /usr/local/bin must be on the path *before* this import
import plotly.io as pio  # noqa: E402

pio.orca.config.use_xvfb = True
pio.orca.config.save()

app.jinja_env.filters["jsonify"] = json.dumps

try:
    from . import (
        mapping,
        insar,
        interferograms,
        admin,
        overlays,
        api_overlay,
        utils
    )  # noqa:E402 F401
except ImportError as e:
    app.logger.exception(f"Unable to setup app: {e}")
    # Something is being run as a script file
    pass

utils.load_station_volcs()
