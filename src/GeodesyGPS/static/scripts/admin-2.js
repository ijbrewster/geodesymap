let stationTable;
let baselineTable;
let tsxTable;
$(document).ready(function(){
    $('#editDialog').dialog({
        buttons:{
            'Delete':{
                text: 'Delete',
                click: deleteStation,
                class: 'deleteStation',
                id:'deleteStation'
            },
            'Ok':saveStation,
            'Cancel':function(){$('#editDialog').dialog('close')},
        },
        width:'auto',
        autoOpen:false
    });

    $('#siteEdit').dialog({
        buttons:{
            'Delete':{
                text: 'Delete',
                click: deleteSite,
                class:'deleteStation',
                id:'deleteSite'
            },
            'Ok':saveSite,
            'Cancel':function(){$('#siteEdit').dialog('close');}
        },
        width:'auto',
        autoOpen:false
    });
    
    $('#newTSX').dialog({
        buttons:{
            'Ok':saveTSX,
            'Cancel':function(){$('#newTSX').dialog('close');}
        },
        width:'auto',
        autoOpen:false
    });

    $('#newOrientation').click(newOrientation);

    $(document).on('change', '.xOrient', calcY);
    $(document).on('change','.yOrient',calcX);

    stationTable=new DataTable('#stationList',{
        paging:false,
        scrollY:'calc(100vh - 250px)',
        dom: 'Bfrtip',
        buttons:[
            {
                text: 'Add Station',
                action: newStation
            }
        ],
        order:[
            [5,'asc'],
            [1,'asc']
        ]
    });

    $('#stationList tbody tr').click(editStation);
    $('#siteList tbody tr').click(editSite);
    $('#selectedImage').change(uploadImage);
    $('#contentSelector button').click(showContent);
    $('#addSiteButton').click(newSite);

    $('.filterInput').keyup(applyFilter);

    $('#siteBaseNames').click(showBaselineSelector);
    $('#closeBaseline').click(setBaselines);

    $('#stationVolcs').click(showVolcanoSelector);
    $('#closeVolcanoes').click(setVolcanoes);


    tsxTable=$('#tsxInfo').DataTable({
        paging:false,
        ajax: {
            url:'admin/tsx/items',
            dataSrc: ''
        },
        dom: 'Bfrtip',
        buttons:[
            {
                text: 'New',
                action: newTSXEntry
            }
        ],
        order: [10, 'asc'],
        rowId:"id",
        columns: [
            {
                data:'id',
                visible:false,
                serchable:false,
            },{
                data:'volcano',
                visible:false,
                serchable:false,
            },{
                data:'volc_name',
                name:"Volcano",
                title:"Volcano",
                className:"dt-body-left",
                editor:volcano_select,
                destColumn:'volcano'
            },{
                data:'mode',
                title:"Mode",
                className:"dt-body-left"
            },{
                data:'polarization',
                title:"Polarization",
                className:"dt-body-left"
            },{
                data:'ascending',
                visible:false,
                searchable:false,
            },{
                data:'direction',
                title:'Direction',
                destColumn:'ascending',
                editor:direction_select,
                className:"dt-body-left"
            },{
                data:'orbit',
                title:"Orbit",
                className:"dt-body-right"
            },{
                data:'spot',
                title:"Spot",
                className:"dt-body-right"
            },{
                data:'incidence',
                title:"Incidence",
                className:"dt-body-right"
            },{
                data:'ordername',
                title:"Order Name",
                className:"dt-body-left"
            },{
                data:'aprox_time',
                title:"Aprox Time",
                className:"dt-body-right"
            },{
                data:'targetx',
                title:"Center X",
                className:"dt-body-right"
            },{
                data:'targety',
                title:"Center Y",
                className:"dt-body-right"
            },{
                data:'side',
                title: "Side Length (M)",
                className:"dt-body-right"
            },{
                data:'coords',
                title:"Projection",
                className:"dt-body-left"
            },{
                data:'rotation',
                title:"Rotation",
                className:"dt-body-right"
            },{
                data:'notes',
                title:"Notes",
                className:"dt-body-left"
            }
        ]
    });
    
    initTSXDialog();

    // Activate an inline edit on click of a table cell
    $('#tsxInfo').on('click', 'tbody td', editTSXCell);

    restoreView();
});

function initTSXDialog(){
    const colOpts=tsxTable.column().init().columns;
    const container=$('#newTSXContent').empty();

    
    for(const colDef of colOpts){
        let visible=colDef.visible;
        if(typeof(visible)=="undefined"){
            visible=true;
        }
        
        if(!visible){
            continue;
        }        

        let label=colDef.title;
        let inputID=`newtsx_${colDef.data}`;
        let labelObj=$(`<label for=${inputID}>`).text(label+":");
        container.append(labelObj);
    
        let editor=colDef.editor || text_editor;
        let editWidget=editor();
        editWidget.attr('id',inputID);
        let destName=colDef.destColumn || colDef.data;
        editWidget.attr('name',destName);
        container.append(editWidget);
    }
}

function saveTSX(){
    const columns=[];
    const values=[];
    const id=[]
    $('#newTSXForm').find('[name]').each(function(){
        id.push(0);
        columns.push($(this).attr('name'));
        values.push($(this).val());
    });
    
    submit_record(columns,values,id,'INSERT')
    .done(function(result){
        const err=result['error'];
        if(err){
           alert(err);
        }else{
            $('#newTSX').dialog("close");
            tsxTable.ajax.reload();
        }
    })
    .fail(function(jqXHR,textStatus,errorThrown){
        alert(`<div class="error">Server returned ${textStatus}, ${errorThrown}<div>`);
    })
}

//////////////////// INLINE EDITOR FUNCTIONALITY/////////////////
function editTSXCell(){
    const cell = tsxTable.cell(this);
    const row = tsxTable.row(this);
    const rowID=row.id();    
    const node=cell.node();
    const colOpts=tsxTable.column().init().columns[cell.index()['column']]
    const col_name=colOpts.destColumn || colOpts.data;
    const editor=colOpts.editor || text_editor;
    
    if($(node).hasClass('editing')){return;}
    
    let destCell=cell;
    let sameDest=true;
    if(col_name!==colOpts.data){
        //find the destination column
        const dest_col_idx=tsxTable.column(function(idx, data, node) {
            return tsxTable.column(idx).dataSrc() === col_name;
        });
        
        destCell=tsxTable.cell(row.index(),dest_col_idx);
        sameDest=false;
    }
    
    const value=destCell.data();

    const editWidget=editor(value);
    
    //set code
    function setValues(newVal){
        destCell.data(newVal);
        $(node).removeClass('editing');

        if(!sameDest){
            cell.data(editWidget.data('display'));
        }
    }
    
    // ON BLUR code
    editWidget.on('blur',function(){
        const newVal=editWidget.val();        
        
        if(newVal==value){
            setValues(newVal);
            return;    
        }
        
        submit_record(col_name,newVal,rowID,'UPDATE')
        .done(function(result){
            const err=result['error'];
            if(err){
                $(node).find('.error').remove();
                $(node).append(`<div class="error">${err}<div>`)
            }else{
                setValues(newVal);
            }
        })
        .fail(function(jqXHR,textStatus,errorThrown){
            $(node).find('.error').remove();
            $(node).append(`<div class="error">Server returned ${textStatus}, ${errorThrown}<div>`)
        })
    });
    ////
    
    const editorWidth=$(node).width()-6;
    editWidget.css('width',editorWidth);
    $(node).html(editWidget).addClass('editing');
    editWidget.focus();
}
/////////////////////////////////////////////////////////////


function submit_record(column,value,id,action){
    const url='admin/tsx/items'
    
    if(typeof(column)!=="object"){
        column=[column];
    }
    
    if(typeof(value)!=="object"){
        value=[value];
    }
    
    if(typeof(id)!=="object"){
        id=[id]
    }
    
    const data={};
    
    for(let i=0;i<value.length;i++){
        if(!(id[i] in data)){
            data[id[i]]={};
        }
        
        data[id[i]][column[i]]=value[i];
    }
    
    const request={
        data:JSON.stringify(data),
        action:action
    }
    
    const future=$.post(url,request)
    return future;
}

function text_editor(value){
    const input=$('<input>').val(value);
    return input;
}

function volcano_select(value){
    const select=$('<select>');
    for(const [name,id] of Object.entries(volcanoLookup)){
        let opt=$('<option>').text(name).val(id);
        opt.data('display',name);
        if(id==value){
            opt.attr('selected',true);
            select.data('display',name);
        }
        
        select.on('change',function(){
            const display=select.find('option:selected').data('display');
            select.data('display',display);
        })
        
        select.append(opt);
    }
    
    return select;
}

function bool_val(){
    const value=this.get(0).value;
    const bool=value=='false' || value=='0'?false:true;
    return bool;
}

function direction_select(value){
    const select=$('<select>');
    select.val=bool_val; //return value as a boolean
    
    const asc=$('<option>').val(true).text('Ascending').data('display','Ascending');
    const desc=$('<option>').val(false).text('Descending').data('display','Descending');
    if(value==true){
        asc.attr('selected',true);
        select.data('display','Ascending');
    }else{
        desc.attr('selected',true);
        select.data('display','Descending');
    }
    
    select.append(asc).append(desc);
    select.on('change',function(){
        const display=select.find('option:selected').data('display');
        select.data('display',display);
    })
    
    return select;
}

function newTSXEntry(){
    initTSXDialog();
    $('#newTSX').dialog("open");
}

function setBaselines(){
    const basenames=[];
    const baselines=[]

    $('#baselineOptions .baseSelectBox:checked').each(function(){
        basenames.push($(this).data('name'));
        baselines.push(this.value);
    });

    basenamesDisplay(basenames,baselines);
    setBaseIDs();
    $('#baselineSelector').hide()
}

function setVolcanoes(){
    const volcnames=[];
    const volcids=[]

    $('#volcanoOptions .volcSelectBox:checked').each(function(){
        volcnames.push($(this).data('name'));
        volcids.push(this.value);
    });

    volcanoDisplay(volcnames,volcids);
    setVolcIDs();
    $('#volcanoSelector').hide()
}

function basenamesDisplay(basenames,baselines){
    const dest=$('#siteBaseNames').empty();
    for (let idx=0;idx<basenames.length;idx++){
        let div=$('<div>')
        div.text(basenames[idx]);
        div.data('id',baselines[idx]);
        dest.append(div);
    }

    dest.sortable({
        update:setBaseIDs
    });
}

function volcanoDisplay(volcnames,volcids){
    const dest=$('#stationVolcs').empty();
    for (let idx=0;idx<volcnames.length;idx++){
        let div=$('<div>')
        div.text(volcnames[idx]);
        div.data('id',volcids[idx]);
        dest.append(div);
    }

    dest.sortable({
        update:setVolcIDs
    });
}

function setBaseIDs(){
    const ids=[]
    $('#siteBaseNames div').each(function(){
        const id=$(this).data('id');
        ids.push(id);
    })
    $('#sitebaselinesEdit').val(JSON.stringify(ids));
}

function setVolcIDs(){
    const ids=[]
    $('#stationVolcs div').each(function(){
        const id=$(this).data('id');
        ids.push(id);
    })
    $('#stationvolcsEdit').val(JSON.stringify(ids));
}

function showBaselineSelector(){
    const selected=JSON.parse($('#sitebaselinesEdit').val());
    $('#baselineOptions .baseSelectBox').each(function(){
        const checked=selected==null?false:selected.includes( this.value )
        this.checked = checked;
    });
    $('#baselineSelector').show();
    $('#filterBaselines').focus();
}

function showVolcanoSelector(){
    let volcVal=$('#stationvolcsEdit').val()
    if (volcVal==''){
        volcVal='[]';
    }
    const selected=JSON.parse(volcVal);

    $('#volcanoOptions .volcSelectBox').each(function(){
        const checked=selected==null?false:selected.includes( this.value )
        this.checked = checked;
    });
    $('#volcanoSelector').show();
    $('#filterVolcanoes').focus();
}

function applyFilter(){
    const filter=$(this).val().toUpperCase();

    const table=$(this).siblings().find('.filteringOptions').find('tr');
    for(const row of table){
        let found=false;
        let tds=$(row).find('td')
        for(const cell of tds){
            if($(cell).text().toUpperCase().indexOf(filter)>-1){
                row.style.display="";
                found=true;
                break;
            }
        }
        if(!found){
            row.style.display="none";
        }
    }
}

function editSite(){
    const id=$(this).data('id');
    const data=$(this).data('info');
    openSiteEdit(id,data);
}

function openSiteEdit(id, info){
    for(const [key,val] of Object.entries(info)){
        $('#site'+key+'Edit').val(val);
    }

    const baseIDs=info['baselines'];
    const baseNames=info['basenames'];
    basenamesDisplay(baseNames,baseIDs);

    $('#sitebaselinesEdit').val(JSON.stringify(baseIDs));

    $('#siteIDEdit').val(id);

    $('#siteEdit').dialog('open');
}

function deleteSite(){
    const siteID=$('#siteIDEdit').val();
    if(siteID=='' || siteID=="0"){
        return; // this site does not exist, don't do anything.
    }

    const ack=confirm("Are you sure you want to delete this site? This will not be possible if there are any stations associated with this site.");
    if(!ack){
        return;
    }

    const url=`admin/site/${siteID}`
    $.ajax({
        url:url,
        type:'DELETE'
    }).done(function(){
        location.reload();
    })
    .fail(function(){
        alert("Unable to delete site.");
    })
}

function saveSite(){
    const data=new FormData($('#siteEditContent').get(0));
    const siteID=$('#siteIDEdit').val();
    let type='POST'
    let url=`admin/site/${siteID}`;

    if(siteID==0){
        type='PUT'
        url="admin/site"
    }

    $.ajax({
        url:url,
        data: data,
        processData:false,
        contentType:false,
        type:type,
    }).done(function(){
        location.reload();
    })

}

function newSite(){
    const id=0;
    const data={
        'name':'',
        lat:0,
        lon:0,
        zoom:0,
        baselines:[],
        basenames:[]
    }

    openSiteEdit(id,data);
}

function restoreView(){
    const view=localStorage.getItem("selectedList");
    if(view){
        const item=$('#'+view).get(0);
        showContent.call(item);
    }
}

function showContent(){
    $('#contentSelector button').removeClass('active');
    const target=$(this).addClass('active').data('target');

    localStorage.setItem('selectedList', $(this).attr('id'));

    $('div.contentDiv').removeClass('active');
    $('#'+target).addClass('active');
}

function deleteStation(){
    const stationID=$('#idEdit').val();
    if(stationID=='' || stationID=='0'){
        return; //not a valid station that can be deleted.
    }

    const ack=confirm("Are you sure you want to delete this station? This will also delete all data associated with this station.");
    if(!ack){
        return;
    }

    const url=`admin/station/${stationID}`;
    $.ajax({
        url: url,
        type: 'DELETE'
    })
    .done(function(){
        window.location.reload();
    })
}

function newStation(){
   info={
    'id':0,
    'name':'',
    'lat':'',
    'lon':'',
    'type':'continuous',
    'site':'',
    'has_tilt':false,
    'stationvolcs':[],
    'volcanonames':[]
   }

   openStationEdit(info);
}

function editStation(){
    const info=$(this).data('info');
    openStationEdit(info);
}

function openStationEdit(info){
    for(const [key,value] of Object.entries(info) ){
        $('#'+key+'Edit').val(value);
    }

    const tbody=$('#orientationEdit tbody').empty();

    const stationID=info['id'];
    if(stationID!=0){
        $('#deleteStation').button("enable");
        $.getJSON(`admin/orientations/${stationID}`)
        .done(function(data){
            for(const record of data){
                let row=createTiltRow(record);
                tbody.append(row);
            }
        })
    }
    else{
        $('#deleteStation').button("disable");
    }


    $('#stationPhoto').attr('src',`image/${info['name']}`);

    //Get the current site list
    const curSite=info['siteid'];
    $.getJSON('sites').done(function(data){
        const select=$('#siteidEdit').empty().append('<option value="">Unknown</option>');
        for(const site of data){
            let option=$('<option>');
            option.text(site['name']);
            option.val(site['id']);
            select.append(option);
        }

        select.val(curSite);
        $('#editDialog').dialog('open');
    });

    const volcIDs=info['stationvolcs'];
    const volcNames=info['volcnames'];
    volcanoDisplay(volcNames,volcIDs);

    $('#stationvolcsEdit').val(JSON.stringify(volcIDs));
}

function createTiltRow(data){
    let row=$('<tr class="orientation">');
    row.data('id',data['orientation_id'])

    //delete checkbox
    row.append('<td><input type="checkbox" class="deleteOrientation"></td>');

    // Set on date/time
    let dateTD=$('<td>');
    let dateInput=$('<input type="datetime-local" class="date">');
    dateInput.val(data['seton']);
    dateTD.append(dateInput);
    row.append(dateTD);

    //x orientation
    let xTD=$('<td>');
    let xInput=$('<input type=number min=-360 max=360 class="xOrient">');
    xInput.val(data['x_orientation']);
    xTD.append(xInput);
    row.append(xTD);

    //y orientation
    let yTD=$('<td>');
    let yInput=$('<input type=number min=-360 max=360 class="yOrient">');
    yInput.val(data['y_orientation']);
    yTD.append(yInput);
    row.append(yTD);

    return row;
}

function newOrientation(){
    const data={
        'orientation_id':0,
    }
    const row=createTiltRow(data);
    $('#orientationEdit tbody').prepend(row);
}

function uploadImage(){
    //"this" is the file input
    const formData=new FormData();
    formData.append("image",this.files[0]);

    const stationName=$('#nameEdit').val()
    $.ajax({
        url:`image/${stationName}`,
        data: formData,
        processData:false,
        contentType:false,
        type: 'PUT',
    }).done(function(){
        $('#stationPhoto').attr('src','');
        $('#stationPhoto').attr('src',`image/${stationName}`);
        $('#selectedImage').val('');
    });

}

function compileTilt(){
    const result=[];
    $('#orientationEdit tbody tr').each(function(){
        const item={}
        const row=$(this);
        item['id']=row.data('id');
        item['delete']=row.find('input.deleteOrientation').is(':checked');
        item['seton']=row.find('input.date').val();
        item['x']=row.find('input.xOrient').val();
        item['y']=row.find('input.yOrient').val();
        result.push(item);
    })

    return JSON.stringify(result);
}

function saveStation(){
    const formValues=new FormData($('#stationEditForm').get(0));
    let stationID=$('#idEdit').val();
    const type=stationID=="0"?"PUT":"POST"

    let url='admin/station'
    if(stationID!=0){
        url+='/'+stationID;
    }

    let tiltData=compileTilt();
    formValues.set('tilt',tiltData);

    $.ajax({
        url:url,
        data: formValues,
        processData:false,
        contentType:false,
        type: type,
    }).done(function(data){
        stationID=data['id'];

        $('#editDialog').dialog('close');
        location.reload();
    });
}

function calcY(){
    var x=Number($(this).val());

    var y=x-90;
    if (y<0) {y+=360}
    const yInput=$(this).closest('tr.orientation').find('input.yOrient');
    yInput.val(y.toFixed(1));
    $(this).val(x.toFixed(1))
}

function calcX(){
    var y=Number($(this).val());

    var x=y+90;
    if (x>360) {x-=360}
    const xInput=$(this).closest('tr.orientation').find('input.xOrient');
    xInput.val(x.toFixed(1));

    $(this).val(y.toFixed(1));
}
