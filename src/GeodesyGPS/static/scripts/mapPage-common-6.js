function showMenu() {
    if ($('#locations').is(':visible')) {
        hideMenu();
        return;
    }

    menuMode = true;
    $('#locations').slideDown({
        start: function() {
            $(this).css({
                display: "grid"
            })
        }
    });
}

function boundsToJSON(bounds){
    return {
        south: bounds.getSouth(),
        west: bounds.getWest(),
        north: bounds.getNorth(),
        east: bounds.getEast()
    }
}

function hideMenu() {
    $('#locations').slideUp({
        done: function() {
            $(this).css({
                display: ""
            })
        }
    });
    menuMode = false;
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function getTickFormat(timeSpan) {
    var tickformat = '%Y-%m-%d';
    if (timeSpan < 5) {
        tickformat = "%H:%M:%S"
    } else if (timeSpan <= 36 * 60) {
        tickformat = "%-m/%-d %-H:%M"
    }
    return tickformat;
}

function setVisibility() {
    var type = $(this).data('type');
    var show = $(this).is(':checked');
    $(markerGroups[type]).each(function() {
        if (show)
            this.addTo(map);
        else
            this.remove();
    });

    graphVectors();
}

function parseRangeDates(xaxis_range) {
    var dateFrom;
    var dateTo;

    //not sure why I need this, but sometimes this comes back as a date object, and other times as a string.
    if (typeof(xaxis_range[0]) == "string") {

        dateFrom = new Date(xaxis_range[0].substring(0, 10));
        dateTo = new Date(xaxis_range[1].substring(0, 10) + 'T23:59:59');
    } else {
        dateFrom = new Date(xaxis_range[0]);
        dateTo = new Date(xaxis_range[1]);

        var dtMonth = dateTo.getMonth()
        var dtYear = dateTo.getFullYear()
        var dtDate = dateTo.getDate()
        dateTo.setUTCHours(23, 59, 59, 999)
        dateTo.setUTCDate(dtDate)
    }

    if (isNaN(dateFrom) || isNaN(dateTo)) {
        console.error('Bad date from/to!');
        return [];
    }

    //go from midnight UTC on the date from to end-of-day on dateTo
    dateFrom = new Date(dateFrom.setUTCHours(0, 0, 0, 0));
    //for date to, set to the last millisecond of the specified date
    dateTo.setUTCHours(23, 59, 59, 999);

    return [dateFrom, dateTo];
}

var layoutCount = 0;

function map_stations(stations) {
    for (var station in stations) {
        var station_data = stations[station];

        var title_string = station_data['id'] + " (" + station + ")";
        var point = [station_data['lat'], station_data['lng']];
        if(point[1]>0){
            point[1]-=360;
        }

        let className=`stationIcon ${station_data['type']}`;

        if(station_data['has_tilt']){
            className+=' tilt';
        }

        const staIcon=L.divIcon({
            className: className,
            html:station_data['id'],
            iconSize:[26,26]
        });

        let markerClass=`stationMarker ${station_data['type']}_marker`;
        const marker = L.marker(point,{
            icon:staIcon,
            title: title_string,
            className: markerClass,
            autoPanOnFocus:false
        });

        $(marker).data('id', station_data['id']);
        const site_id = station_data['site'].replace(' ', '').toLowerCase();
        $(marker).data('site', site_id);
        $(marker).data('has_tilt', station_data['has_tilt'])

        markerGroups[station_data['type']].push(marker);

        if (station_data['type'] == 'continuous')
            marker.addTo(map);

        marker.addEventListener('click', showStationGraphs);
    }

}

let stationInfo=null;

function showStationImage(marker){
    if(stationInfo !== null){
        stationInfo.close();
        stationInfo = null;
    }

    if(!$('#showImages').is(':checked')) return;

    const station=$(marker).data('id');
    const staImg=new Image();
    staImg.onerror=hideStationImage;
    staImg.className += 'stationImg'
    staImg.src=`image/${station}`
    staImg.onload=function(){openPopup(marker, staImg);}
}

function openPopup(marker, staImg){
    stationInfo = L.popup({
        className:'stationImgPopup',
        maxWidth:99999,
        maxHeight:99999,
        autoPanPaddingBottomRight:[10,10],
        content:staImg,
        keepInView:true
    });

    marker.bindPopup(stationInfo).openPopup();
}

function hideStationImage(){
    if(stationInfo !== null){
        stationInfo.close();
        stationInfo=null;
    }
}

function closeGraph() {
    //"this" is the close button
    globalVectorArgs=getVectorParameters($(this).closest('div.chart'))
    var graphDiv = $(this).closest('div.chart').hide();
    Plotly.purge(graphDiv.find('div.graphArea')[0]);
    Plotly.purge(graphDiv.find('div.tiltArea')[0]);
    graphDiv.remove();
}

function dom_post(url, args) {
    iframe_id += 1;
    const frameName=`loadFrame${iframe_id}`;
    const target_frame = $('<iframe>',{
        name: frameName,
        style: 'display:none'
    })

    target_frame.on('error', function() { console.log("Frame ERROR"); })
    $("body").append(target_frame)
    target_frame.on('load', function() { console.log("Frame Loaded"); })


    const form = $('<form>',{
        action:url,
        target:frameName,
        method:"POST"
    });

    for (var key in args) {
        if (args.hasOwnProperty(key)) {
            var value = args[key]
            if (typeof(value) == "object") {
                value = JSON.stringify(value)
            }
            var field = $(`<input type="hidden" name="${key}">`)
            field.val(value)
            form.append(field)
        }
    }

    $("body").append(form)
    form[0].submit()

    // remove the frame after 10 minutes. If it takes more than 10 minutes to
    // generate and download the item, then this will cause breakage, but I
    // have to choose *some* time, since I have no way of knowing if/when
    // the download completes.
    setTimeout(function() {
        target_frame.remove();
    }, 600000);

    form.remove()
}

function showMessage(msg, cls) {
    var msgdiv = $('<div class="message" style="display:none">')
    var msgtext = $("<div class='msgtext'>")
    msgdiv.append(msgtext)
    msgdiv.addClass(cls)
    msgtext.html(msg)
    $("body").append(msgdiv);
    msgdiv.slideDown(function() {
        setTimeout(function() {
            msgdiv.slideUp(function() {
                msgdiv.remove();
            });
        }, 5000)
    });
}

var vector_data;

var old_lines = []
var scale_len = null

function clearVectors() {
    //remove the old vectors (if any) from the map
    for (var i = 0; i < old_lines.length; i++) {
        old_lines[i].remove();
    }

    old_lines = [];
}

var vector_station, old_params;

function compObjects(object1, object2) {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);

    if (keys1.length !== keys2.length) {
        return false;
    }

    for (let key of keys1) {
        if (object1[key] !== object2[key]) {
            return false;
        }
    }

    return true;
}

function getVectors() {
    if (vectorGuard === true) {
        vectorGuard = false;
        return;
    }

    var graphDiv = $(this).closest('div.chart');

    if (graphDiv.length == 0) {
        //might be empty
        graphDiv = $('div.chart:visible').first();
    }

    //check if we are on GPS or tilt. If tilt, then don't update
    if (graphDiv.find('.plotlyPlot:visible').hasClass('tiltArea'))
        return;

    var reqParams = getVectorParameters(this);
    var checkParams = Object.assign({}, reqParams);

    //For vector graphing purposes, we don't care about the station, because it
    //can change without affecting the displayed vectors.
    delete checkParams['station'];

    if (typeof(reqParams) === 'undefined') {
        // no request to submit. Don't do anything
        return;
    }

    if (old_lines.length > 0 && compObjects(reqParams, old_params)) {
        $('#vectorLoading').hide();
        return; //we already graphed these vectors
    } else {
        old_params = reqParams;
    }

    scale_len = null;

    //remove the old vectors (if any) from the map
    clearVectors();
    $('#vectorLoading').show();

    $.get('get_vector_data', reqParams)
        .done(function(data) {
            clearVectors();
            vector_data = data;
            scale_len = data['scale'];
            graphVectors();
        })
        .always(function() {
            $('#vectorLoading').hide();
        });

}

function graphVectors() {
    if (typeof(vector_data) == 'undefined') { return; } //nothing to graph
    //draw vectors on google maps.
    var lineSymbol = {
        //path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
    };

    var barSymbol = {
        path: "M -1.75 0 L 1.75 0",
    }

    var xy_data = vector_data['xy']
    if (typeof(xy_data) == 'undefined')
        return; //nothing to plot

    clearVectors();

    var z_data = vector_data['z']
    var type_data = vector_data['type']
    var type_lookup = {}
    $('.siteCheck').each(function() {
        type_lookup[$(this).data('type')] = $(this).is(':checked')
    });

    for (var i = 0; i < xy_data.length; i++) {
        if (!type_lookup[type_data[i]]) { continue; } //this type is not shown.

        var xy_path = xy_data[i];
        for(const idx in xy_path){
            if(xy_path[idx]['lng']>0){
                xy_path[idx]['lng']-=360
            }
        }

        var z_path = z_data[i];
        for(const idx in z_path){
            if(z_path[idx]['lng']>0){
                z_path[idx]['lng']-=360
            }
        }

        //graph the XY vector
        const xy_line=L.polyline(xy_path,{
            color:'#3300FF',
            className:'xyVector'
        }).arrowheads({
            size:'13px',
            yawn:50,
            fill:true,
            frequency:'endonly'
        });

        //and the z(vertical) vector
        const z_line=L.polyline(z_path,{
            color:"#0CDA3B",
            className:'zVector'
        }).arrowheads({
            'yawn':180,
            size:'8px',
            frequency:'endonly'
        });

        xy_line.addTo(map);
        z_line.addTo(map);
        // var z_line = new google.maps.Polyline({
        //     path: z_path,
        //     strokeColor: "#0CDA3B",
        //     icons: [{
        //         icon: barSymbol,
        //         offset: '100%'
        //     }],
        //     map: map,
        //     zIndex: 2
        // });

        //keep a reference to the line objects so we can remove them later
        old_lines.push(xy_line)
        old_lines.push(z_line)
    }
    graphVectorScale();
}

var scale_line;
var scale_marker;
var scale_background;
var scale_marker2;

function graphVectorScale() {
    if (typeof(scale_line) !== 'undefined') {
        scale_line.remove();
        scale_marker.remove();
        scale_background.remove();
        scale_marker2.remove();
    }

    if (scale_len == null)
        return; //nothing to graph

    var bounds = map.getBounds();
    const sw=bounds.getSouthWest();
    const ne=bounds.getNorthEast();
    var bottom_lat = sw.lat;
    var left_edge = sw.lng;
    const top_lat=ne.lat
    const right_edge=ne.lng;

    // Make sure signage of both right and left longitudes
    // are the same for easy station location comparisons.
    const right_lng=right_edge>0?right_edge-360:right_edge;
    const left_lng = left_edge>0?left_edge-360:left_edge;

    let inRange=false
    $(vector_data.xy).each(function(i,xy){
        const lat=xy[0].lat;
        let lng=xy[0].lng;
        if(lng>0){
            lng-=360;
        }

        const latIn= bottom_lat<=lat && lat<=top_lat;
        const lonIn= left_lng<=lng && lng<=right_lng;
        if(latIn && lonIn){
            inRange=true;
            return false
        }
    })

    if(inRange){
        $('#legend').show()
    }
    else{
        $('#legend').hide();
        return;
    }

    var METER_TO_LON = (111320 * Math.cos(bottom_lat * (Math.PI / 180)))

    //convert the length in meters to degrees longitude (based on latitude of scale line)
    var deg_len = scale_len / METER_TO_LON
    var desired_right = left_edge + deg_len;

    var start = [bottom_lat, left_edge];
    var stop = [bottom_lat, desired_right];

    const p1=map.latLngToContainerPoint(start,map.getZoom());
    const p2=map.latLngToContainerPoint(stop,map.getZoom());

    let d=p2.x-p1.x;
    if(p1.y!=p2.y){//unlikely, but possible since the earth isn't flat.
        d = Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p1.y - p2.y) * (p1.y - p2.y))
    }

    $('.scaleBar').css('width', d - 9); //subtract 9 for the arrow head width
    $('.nowrap').css('min-width', d);
    $('#dateSpan').text(`${vector_data['date_from']} - ${vector_data['date_to']}`);
}

function setMapLocation() {
    if (menuMode) {
        hideMenu();
    }

    $('.extraData').hide();
    $('#content').show();
    $('#locations div .tab').removeClass('current');

    var center = { lat: Number($(this).data('lat')), lng: Number($(this).data('lon')) };
    var zoom = $(this).data('zoom');

    //this will de-select the current tab
    window.vectorGuard = true;
    tabGuard=true;
    map.setView(center,zoom);

    //close any plot divs
    $('.chart').remove();
    global_graph_div = null;

    $(this).addClass('current');
    getVectors();
    getEarthquakes();
}

var map_quakes = [];
var infowindow;

function getEarthquakes() {
    var num_quakes = map_quakes.length
    for (var i = 0; i < num_quakes; i++) {
        var quake = map_quakes.pop();
        quake.removeFrom(map);
    }

    $(document).data('quakeCache', []);

    if (!$('#showQuakes').is(':checked')) {
        $('#avoOnly').prop('disabled', true)
        return;
    } else {
        $('#avoOnly').prop('disabled', '')
    }

    var url = 'https://earthquake.usgs.gov/fdsnws/event/1/query.geojson'

    //find the start and end dates from the first graph div
    var chartDiv = $('div.chart').first();
    var dateFrom = chartDiv.find('input.dateFrom').val();
    var dateTo = chartDiv.find('input.dateTo').val();
    if (typeof(dateFrom) == 'undefined' || typeof(dateTo) == 'undefined') {
        dateTo = new Date();
        dateFrom = dateTo - (365 * 24 * 60 * 60 * 1000);
    }

    dateFrom = new Date(dateFrom);
    dateFrom.setUTCHours(0, 0, 0, 0);
    dateFrom = formatISODateString(dateFrom);
    dateFrom = dateFrom.slice(0, -1) + ".000Z";

    dateTo = new Date(dateTo);
    dateTo.setUTCHours(23, 59, 59, 999);
    dateTo = formatISODateString(dateTo)
    dateTo = dateTo.slice(0, -1) + ".999Z";

    var mapBounds = map.getBounds();
    var northEast = mapBounds.getNorthEast();
    var southWest = mapBounds.getSouthWest();

    var lat_from = southWest.lat;
    var lat_to = northEast.lat;
    var lng_from = southWest.lng;
    var lng_to = northEast.lng;

    if (lng_to < lng_from) {
        lng_to += 360;
    }

    var avoOnly = $('#avoOnly').is(':checked')

    var args = {
        'starttime': dateFrom,
        'endtime': dateTo,
        'minlatitude': lat_from,
        'maxlatitude': lat_to,
        'minlongitude': lng_from,
        'maxlongitude': lng_to
    }

    if (avoOnly) {
        args['catalog'] = 'av'
    }

    $.getJSON(url, args)
        .done(function(quake_data) {
            var trianglePath = "M -10 10 L 10 10 L 0 -10 z"
            var quakes = quake_data.features;
            var num_quakes = quakes.length;
            var quake_cache = [];
            for (var i = 0; i < num_quakes; i++) {
                var quake = quakes[i];
                var location = quake.geometry.coordinates;
                location = [location[1], location[0]]
                var magnitude = quake.properties.mag;
                var time = new Date(quake.properties.time)
                time = formatISODateString(time)
                time = time.replace('T', ' ')

                var scale = 3.5 * Math.exp(0.5 * magnitude)

                const marker = L.circleMarker(location,{
                    radius:scale,
                    className: "quakeMarker",
                    autoPanOnFocus:false,
                    magnitude:magnitude,
                    time:time,
                    color:'black',
                    weight:1,
                    fill:true,
                    fillColor:'white',
                    fillOpacity:.75
                })

                marker.addEventListener('mouseover',function(){
                    infowindow = L.popup({
                        content: "Mag: " + this.options.magnitude + "<br>" + "Date: " + this.options.time
                    })

                    marker.bindPopup(infowindow).openPopup()
                });

                marker.addEventListener('mouseout',function(){
                    if(infowindow!=null){
                        infowindow.close();
                    }
                });

                map_quakes.push(marker);
                var cache = {
                    'location': location,
                    'magnitude': magnitude
                }
                quake_cache.push(cache);
                marker.addTo(map);
            }

            $(document).data('quakeCache', quake_cache);
        })
        .fail(function() {
            console.log('Unable to fetch quake data');
            alert('Unable to fetch quake data.');
        })
}

function setSpecialText() {
    if (menuMode) {
        hideMenu();
    }
    $('#content').hide();
    $('.extraData').hide();
    var target = $(this).data('target');
    $(`#${target}`).show();

    $('#locations div .tab').removeClass('current');
    $(this).addClass('current');
}
