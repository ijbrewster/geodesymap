//code for implementing a multi-level select drop-down that uses (mostly) native select look and feel.
(function($){

    function hideMLSelect(event){
        event.stopPropagation();
        $(document).off('click',':not(.multiLevelSelect li)',hideMLSelect);
        $('.multiLevelSelect li.topLevel').removeClass('selected');
    }

    function selectMenuItem(event){
        const top=$(this).closest('.multiLevelSelect');
        const selectCallback=top.data('onSelect');
        const selected=top.find('span.selected');
        const itemText=$(this).text();
        let itemVal=$(this).data('value')
        if(typeof(itemVal)=='undefined') { itemVal=itemText; };

        selected.text(itemText);
        top.val(itemVal);

        // Copy the data of this item to the top level object.
        for(const [key,value] of Object.entries($(this).data())){
            top.data(key,value);
        }

        top.data('item',$(this));

        hideMLSelect(event);

        if(typeof(selectCallback)!=='undefined'){
            selectCallback.call(top.get(0));
        }
    }

    $.fn.MultiLevelSelect=function(config){
        let chromeAgent = navigator.userAgent.indexOf("Chrome") > -1;

        if(!window.onblur){
            window.onblur=hideMLSelect;
        }

        const settings=$.extend({},config);

        return this.each(function(){
            const item=$(this);
            item.addClass('multiLevelSelect');
            item.data('onSelect',settings.select);

            if(chromeAgent){
                item.addClass('chrome');
            }

            item.click(function(event){
                hideMLSelect(event);
                const topLI=$(this).find('li.topLevel');
                topLI.addClass('selected');
                event.stopPropagation();
                $(document).on('click',':not(.multiLevelSelect li)',hideMLSelect);
            })

            item.find('li:not(.topLevel):not(.parent), li.topLevel.selected').click(selectMenuItem);
        });
    }
}(jQuery))
