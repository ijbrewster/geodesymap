
const InSARColors=['#0000FF','#00FF00','#FF0000','#FFFF00','#00FFFF', '#FF00FF'];
let InSARSelects=[];

function getInSAR(event){
    /*
     * Get time series plot for point clicked on map
     */
    // close any station images that might be open
    if(stationInfo !== null){
        stationInfo.close();
        stationInfo = null;
    }

    let latLon=event.latlng;

    const native_event = Object.values(event)
    .filter(function(property) {
        return property instanceof window.MouseEvent;
    })[0];

    if (native_event.altKey){
        setInsarRef.call($('#insar_opts').get(0), event, JSON.stringify(latLon));
        return;
    }

    const add_chart = typeof(native_event) == 'undefined' | native_event===null ? false : native_event.shiftKey;
    if(!add_chart){
        clearInSARSelects();
    }

    const colorIdx=InSARSelects.length%InSARColors.length
    const insarColor=InSARColors[colorIdx];

    const item=L.circleMarker(latLon,{
        radius:6,
        className:"insarMarker",
        interactive:false,
        color:insarColor,
        weight:3,
        opacity:0.8,
        fill:false
    })
    item.addTo(map);

    InSARSelects.push(item);

    // parse the select points into the arguments object
    let queryArgs=$('#insar_opts').data('args') || {};
    let selectPoints=[];
    for(const item of InSARSelects){
        const itemLoc=JSON.stringify(item.getLatLng());
        const itemColor=item.options['color'];
        const itemDict={'color':itemColor};
        Object.assign(itemDict,itemLoc)
        selectPoints.push(itemDict);
    }

    if(selectPoints.length>0){
        queryArgs['insar_selects']=JSON.stringify(selectPoints);
    }
    else if('insar_selects' in queryArgs){
        delete queryArgs['insar_selects'];
    }
    $('#insar_opts').data('args',queryArgs);

    let args=parseInsar();
    if (args===null){
        return;
    }

    const cacheBreak=new Date().getTime();
    args['point']=JSON.stringify(latLon);
    args['_']=cacheBreak;

    console.log(args);
    $.getJSON('getInSarTS',args)
    .done(function(data){
        showInSARts(data,!add_chart, insarColor);
    });
}

function clearInSARSelects(){
        //Remove any existing markers
        while(InSARSelects.length>0){
            const item=InSARSelects.shift();
            item.remove();
            delete item;
        }

        let queryArgs=$('#insar_opts').data('args') || {};
        if('insar_selects' in queryArgs){
            delete queryArgs['insar_selects'];
            $('#insar_opts').data('args',queryArgs);
        }
}

function showInSARts(data, newDiv, color){
    if(typeof(newDiv)==='undefined'){
        newDiv=true;
    }
    let chartDiv=null;
    if (newDiv){
        chartDiv=getChartDiv(null, "InSAR", "", false, false);
        chartDiv.addClass('insar');
    }
    else{
        chartDiv=$('div.chart.insar:visible').last();
        if(chartDiv.length==0){
            chartDiv=getChartDiv(null, "InSAR", "", false, false);
            chartDiv.addClass('insar')
        }
    }
    let chartDest=chartDiv.find('div.graphArea:visible, div.tiltWrapper:visible').last();
    if(!newDiv){
        const newChartDest=$('<div class="graphArea plotlyPlot">');
        chartDest.parent().append(newChartDest);
        chartDest=newChartDest;
    }

    const dates=data['dates']
    const ts_plot={
        'x':dates,
        'y':data['ts'],
        'type':'scatter',
        'mode':'markers',
        'name':'InSAR Timeseries',
        'marker':{
            'color':color,
            'size':12
        }
    };

    const linefit_plot={
        x:[dates[0], dates[dates.length-1]] ,
        y:[data['intercept'], data['lastval']],
        mode:'lines',
        name:'Linefit',
        line:{
            color:'rgb(20,81,124)',
            width:3
        }
    };

    const chart_data=[ts_plot, linefit_plot]

    const layout = {
        title:`Path ${data['path']}, Average Velocity: ${data['averagev']} cm/yr`,
        showlegend: false,
        height:300,
        xaxis: {
            title: 'date',
            type: 'date',
            tickformat: "%Y.%m.%d",
            hoverformat: "%m/%d/%Y"
        },
        yaxis: {
            title: 'line of sight (cm)'
        },
        margin: {
            l: 50,
            r: 50,
            b: 50,
            t: 50,
            pad: 4
        }
    };

    if(insarXRange!==[]){
        layout.xaxis.range=[...insarXRange]
    }

    const config={
        //responsive: true,
        'toImageButtonOptions': {
            format: 'png',
            'height': 800,
            'width': 600,
            'scale': 1.5,
            filename: 'InSAR.png'
        }
    }

    Plotly.react(chartDest[0],chart_data,layout,config);


}

function parseInsar(){
    const file=$('#insar_opts').data('filepath');
    const sensor=$('#insar_opts').data('sensor');
    const type=$('#insar_opts').data('type');

    const args=$('#insar_opts').data('args') || {};

    args['file']=file;
    args['sensor']=sensor;
    args['type']=type;

    return args
}

function setInsarRef(event, ref_point){
    const path=$('#insar_opts').val();

    if(path!=''){
        const args=$('#insar_opts').data('args') || {};
        if(typeof(ref_point)!='undefined' && ref_point!=null){
            args['ref_point']=ref_point
        } else if('ref_point' in args){
            delete args['ref_point'];
        }
        $('#insar_opts').data('args',args);
    }

    $('#insar_opts').data('filepath',path);
    $('#insarDownload').attr('disabled',path=='');

    load_overlay.call($('#insar_opts').get(0));
}

let insarDateChanged=false;
function insarDisplayed(response){

    if(!insarDateChanged){
        clearInSARSelects();
        $('div.chart.insar:visible').remove();
        insarXRange=[]
    } else {
        insarDateChanged=false;
    }

    //set min/max labels for InSAR color scale
    let maxv=response['vmax']; // m/year
    let minv=response['vmin']; // m/year

    //convert to mm/year
    maxv*=1000;
    minv*=1000;

    //round
    maxv=Math.round(maxv);
    minv=Math.round(minv);

    //and convert to cm/year
    maxv/=10;
    minv/=10;

    const start=response['start']
    const end=response['end']

    $('#insar_range').text(`${start} - ${end}`)
    $('#colorTopLabel').text(maxv);
    $('#colorBottomLabel').text(minv);
    $('#colorbarContainer').show();

    $('#insar_data_max').text(`(${(response['data_lim']*100).toFixed(1)})`)

    //listener for clicks on map area
    overlays['insar']['overlay'].addEventListener('click',getInSAR);
}

function downloadInSARTSData(altDownload,graphDiv){
    let args=parseInsar();
    if (args===null){
        return;
    }
    if (altDownload){
        // Download the full dataset .h5
        const queryString=Object.keys(args).map((key) => {
            return encodeURIComponent(key) + '=' + encodeURIComponent(args[key])
        }).join('&');

        const url='downloadInSARTS?'+queryString
        window.location=url
    } else {
        //download just the displayed timeseries
        const plots=$(graphDiv).parent().find('.js-plotly-plot');
        const data=[]
        const dates=plots.get(0).data[0]['x'];
        let csvContent="data:text/csv;charset=utf-8,"
        csvContent+='Date,' //csv header
        for(let i=0;i<plots.length;i++){
            csvContent+=`ts${i+1},`
            data.push(plots.get(i).data[0].y)
        }
        csvContent+='\r\n';

        //write the data
        for(let r=0;r<dates.length;r++){
            csvContent+=dates[r]+','
            for(let d=0;d<data.length;d++){
                csvContent+=data[d][r]+','
            }
            csvContent+='\r\n'
        }

        const encodedUri=encodeURI(csvContent);

        const file_parts=args.file.split('_');

        const file_name=`insar_ts_${file_parts[1]}_${file_parts[2]}_${file_parts[3].replace('.h5','')}.csv`

        $('a')
        .attr('download',file_name)
        .attr('href',encodedUri)
        .get(0)
        .click()

    }
}

let insarXRange=[]
function setInsarDates(xRange){
    if (xRange[0]!==insarXRange[0] || xRange[1]!==insarXRange[1]) {
        insarXRange=[...xRange];
        insarDateChanged=true;
        const xRangeEncoded=JSON.stringify(xRange);
        load_overlay.call($('#insar_opts').get(0), {'dates':xRangeEncoded})
    }
};
