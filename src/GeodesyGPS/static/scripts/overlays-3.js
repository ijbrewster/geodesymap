let OVERLAY_FILES={};
let OverlayDateOpts={};
let selectedOverlayFiles,selectedOverlayDates;

function loadOverlayOpts(type){
    const overlayInfo=overlays[type];
    const select=$('#'+overlayInfo['target']);

    //save current selection (if any)
    const currentSensor=select.data('sensor');
    const currentIdentifier=select.val();
    const currentText=select.find('span.selected').text();


    //clear the select menu
    select.data('sensor','');
    select.data('type',type);
    select.data('args',{});
    const topList=clearMultiLevelSelect(select);

    if(currentIdentifier!==''){
        select.find('span.selected').text(currentText);
        select.val(currentIdentifier);
        select.data('sensor',currentSensor);
    } else {
        select.find('span.selected').text('Loading...');
    }

    const bounds=JSON.stringify(boundsToJSON(map.getBounds()));
    const future=$.getJSON(`fileOpts/${type}`,{bounds:bounds})
    .done(function (data) {
        const noOverlay = $('<li>');
        topList.append(noOverlay);

        noOverlay.text(overlayInfo['noString']);
        noOverlay.data('value', '');

        for (const [sensor, identifiers] of Object.entries(data)) {
            let sensorEntry = $('<li class="sensor parent">');
            sensorEntry.data('sensor', sensor);
            sensorEntry.val('');
            sensorEntry.text(sensor);
            topList.append(sensorEntry);

            let sensorList = $('<ul class="sensorList">');
            sensorList.data('sensor', sensor);
            sensorEntry.append(sensorList);

            for (const [title, ident] of identifiers) {
                let option = $('<li>');
                option.text(title);
                option.data('value', ident);
                option.data('sensor', sensor);
                sensorList.append(option);
            }
        }

        const selectFunc=overlays[type].select || selectOverlayIdentifier

        select.MultiLevelSelect({
            'select': selectFunc,
        });

        if (currentIdentifier !== '') {
            select.find('span.selected').text(currentText);
            select.val(currentIdentifier);
            select.data('sensor', currentSensor);
        } else {
            select.find('span.selected').text(overlayInfo['noString']);
        }
    });

    return future
}

function hideOverlay(type){
    const overlayDef=overlays[type];
    let layerTarget=type;
    let layer=overlayDef['overlay'];
    if(typeof(layer)==='string'){
        layerTarget=layer;
        layer=overlays[layer]['overlay'];
    }

    //remove the layer from the display
    if(layer!==null){
        layer.remove();
        overlays[layerTarget]['overlay']=null;
    }

    //disable the download menu entry
    const dlMenu=overlayDef['downloadEntry'];
    $('#'+dlMenu).attr('disabled',true);

    //clear out the select menu
    const menu=$('#'+overlayDef['target']);
    menu.val('');
    if('filepath' in menu.data()){
        menu.data('filepath',null);
    }
    if('sensor' in menu.data()){
        menu.data('sensor',null);
    }
    menu.find('span.selected').text(overlayDef['noString']);
}

function selectOverlayIdentifier(){
    // will be called with the selected top-level menu as "this"
    const selectMenu=$(this);
    const identifier=selectMenu.val();
    const sensor=selectMenu.data('sensor');
    const type=selectMenu.data('type');

    const downloadMenu=overlays[type]['downloadEntry'];

    // See if any other overlays use this layer, and as such will need
    // to be cleared.
    for( const [dtype,info] of Object.entries(overlays)){
        if(info['overlay']===type){
            // this overlay targets the currently selected layer, and will be replaced.
            hideOverlay(dtype);
        }
    }

    $('#'+downloadMenu).attr('disabled',identifier=='');

    $('#overlayFileSelect').data('lastMenu',this);

    if (identifier!=""){
        // load the file listings for this overlay type
        $.getJSON(`overlay/files/${type}`,{'identifier':identifier, sensor:sensor})
        .fail(function(){
            alert(`Unable to load overlay for ${sensor} ${identifier}`)
        })
        .done(function(data){
            if(!(sensor in OVERLAY_FILES)){
                OVERLAY_FILES[sensor]={}
            }
            OVERLAY_FILES[sensor][identifier]=data;
            overlayIdentifierSelected(sensor,identifier);
        })
    }
    else{

        $('#overlayFileSelect').hide();
        selectOverlayDate();
    }
}

//compile a list of date pair options for the selected overlay
function overlayIdentifierSelected(sensor,pathFrame){
    selectedOverlayFiles=OVERLAY_FILES[sensor][pathFrame];
    selectedOverlayDates=Object.keys(selectedOverlayFiles);
    OverlayDateOpts={};
    $('#overlayDateFromSelect').empty();
    $('#overlayDateToSelect').empty();

    const datalist=$('#ifrDateMarkers').empty()

    let dateIdxVal=selectedOverlayDates.length-1;
    for(let i=0;i<selectedOverlayDates.length;i++){
        let dateStr=selectedOverlayDates[i];

        if(initOverlayDate===dateStr){
            initOverlayDate=null;
            dateIdxVal=i;
        }

        datalist.append(`<option value='${i}' label='${dateStr}'>`);

        let [startDate,endDate]=dateStr.split(' - ');
        if(!(startDate in OverlayDateOpts)){
            OverlayDateOpts[startDate]=[];
            $('#overlayDateFromSelect').append(`<option>${startDate}</option>`);
        }

        OverlayDateOpts[startDate].push(endDate);
    }

    $('#ifrDate')
    .attr('max',selectedOverlayDates.length-1)
    .attr('value',selectedOverlayDates.length-1)
    .val(dateIdxVal);

    $('#overlayFileSelect').show();
    selectOverlayDate();
}

let overlayBounceTimer=null;
function overlayLoadDebounce(){
    if(overlayBounceTimer!==null){
        clearTimeout(overlayBounceTimer);
    }
    overlayMenu=$('#overlayFileSelect').data('lastMenu');
    overlayBounceTimer=setTimeout(function(){load_overlay.call(overlayMenu)},100);
}

function overlayFromDateChanged(update){
    if(typeof(update)=='undefined'){
        update=true;
    }
    const dateFrom=$('#overlayDateFromSelect').val();

    const endSelect=$('#overlayDateToSelect').empty();

    for(const endDate of OverlayDateOpts[dateFrom]){
        endSelect.append(`<option>${endDate}</option>`);
    }

    if(OverlayDateOpts[dateFrom].length==1 && OverlayDateOpts[dateFrom][0]==dateFrom){
        $('#overlayDateToSpan').hide();
    }
    else{
        $('#overlayDateToSpan').show();
    }

    if(update===true){
        overlayToDateChanged();
    }
}

function overlayToDateChanged(){
    const dateFrom=$('#overlayDateFromSelect').val();
    const dateTo=$('#overlayDateToSelect').val();
    const ifrDate=`${dateFrom} - ${dateTo}`;

    const dateIdx=selectedOverlayDates.indexOf(ifrDate);
    if(dateIdx!==-1){
        $('#ifrDate').val(dateIdx);
        selectOverlayDate();
    }
}

function selectOverlayDate(){
    const topMenu=$('#overlayFileSelect').data('lastMenu');

    let identifier=$(topMenu).val();
    let filepath;

    if(identifier==""){
        filepath='';
    } else{
        const dateIDX=$('#ifrDate').val();
        const selectedDate=selectedOverlayDates[dateIDX];

        //display the current date range
        const dates=selectedDate.split(' - ');
        $('#overlayDateFromSelect').val(dates[0]);
        overlayFromDateChanged(false);
        $('#overlayDateToSelect').val(dates[1]);

        // Get the filepath. Format for what the server expects
        const filename=selectedOverlayFiles[selectedDate][0];
        identifier=identifier.replace("Path: ","path").replace(" Frame: ","frame");
        const [datestart,dateend]=selectedDate.split(' - ');
        const [startmonth, startday,startyear]=datestart.split('/');
        const [endmonth,endday,endyear]=dateend.split('/');
        const startDate=`${startyear}${startmonth}${startday}`;
        const endDate=`${endyear}${endmonth}${endday}`;
        let daterange=startDate;
        if(startDate!=endDate){
            daterange+=`_${endDate}`;
        }

        filepath=`${identifier}/${daterange}/${filename}`
    }

    $(topMenu).data('filepath',filepath);


    overlayLoadDebounce();
}

function load_overlay(imgParams){
    overlayBounceTimer=null;

    let overlayFile=$(this).data('filepath');
    let overlaySensor=$(this).data('sensor');
    let overlayType=$(this).data('type');
    const opacity=Number($(this).closest('.overlaySelector').find('.overlayOpacity').val());

    let args=$(this).data('args') || {};
    args['file']=overlayFile;
    args['sensor']=overlaySensor;

    let url=`overlay/${overlayType}`;

    //Interferogram type toggle
    //currently this isn't used, but may be brought back for coherence images
    if (overlayType=='kml' && $('.ifrSelector .switch input').is(':checked')){
        url='something new';
        overlayFile=overlayFile.replace('_color_phase.kmz','_amp.tif');
    }

    let destLayer=overlayType;

    const overlayDest=overlays[overlayType]['overlay'];
    if(typeof(overlayDest)=='string'){
        destLayer=overlayDest;
        hideOverlay(overlayDest);
    }

    if(overlays[destLayer]['overlay'] != null){
        overlays[destLayer]['overlay'].remove();
        overlays[destLayer]['overlay']=null;
    }

    if (overlayFile==''){ return; }

    //this function returns the path to the image, to be used in a google maps ground overlay
    $.getJSON(url,args)
    .done(function(info){
        //url to get the PNG image
        let img_url=new URL(`overlay/${overlayType}/${info['img_name']}`,document.baseURI);

        if(typeof imgParams!=='undefined'){
            for(const [key,value] of Object.entries(imgParams) ){
                img_url.searchParams.append(key,value);
            }
        }


        img_url.searchParams.append('path',info['img_path']);
        const cacheBreak=new Date().getTime();

        img_url.searchParams.append('_',cacheBreak);

        img_url=img_url.href

        const ne=L.latLng([
            info['coordinates']['NE']['lat'],
            info['coordinates']['NE']['lng']
        ]);

        const sw=L.latLng([
            info['coordinates']['SW']['lat'],
            info['coordinates']['SW']['lng']
        ]);

        if(!initOverlayComplete){
            initOverlayComplete=true;
            const overlayCoordinates=L.latLngBounds(sw,ne);
            map.fitBounds(overlayCoordinates);
        }

        if(overlays[destLayer]['overlay'] != null){
            overlays[destLayer]['overlay'].remove();
            overlays[destLayer]['overlay']=null;
        }

        let overlay_layer;

        //Figure out how we want our longitudes here
        const mapBounds=map.getBounds();
        const mapEast=mapBounds.getEast();
        const mapWest=mapBounds.getWest();

        //make ne and sw signs match
        if (mapEast*ne['lng']<0){
            ne['lng'] += (ne['lng']>0) ? -360 : 360;
        }

        if (mapWest*sw['lng']<0){
            sw['lng'] += (sw['lng']>0) ? -360 : 360;
        }

        const coordinates= L.latLngBounds(sw,ne);
        let finalize=true; //should we run callbacks

        if(info['type']=='img'){
            // Basic image overlay, just display the image in an overlay as-is
            overlay_layer= new L.imageOverlay(img_url, coordinates,
                {
                    opacity:opacity,
                    interactive:true,
                    bubblingMouseEvents:false
                }
            )
        } else if(info['type']=='tiles'){
            // Tiled overlay. Requires image to be processed into "tiles"
            // Server side
            //Tile bounds always want "real" longitudes
            if(ne['lng']<-180) ne['lng']+=360
            if(sw['lng']<-180) sw['lng']+=360
            if(ne['lng']>180) ne['lng']-=360
            if(sw['lng']>180) sw['lng']-=360

            const tileBounds = L.latLngBounds(sw,ne);
            overlay_layer=L.tileLayer('overlay/tiles/{z}/{x}/{y}.png?path={path}',{
                type: overlayType,
                path: info['img_path'],
                tms:true,
                maxZoom:20,
                minZoom:6,
                bounds:tileBounds,
                noWrap:false
            });
        } else if(info['type']=='scaledIMG'){
            // Retrieve the image, and scale it up by some factor,
            // using Nearest neighbor scaling. Better for certain 
            // types of images, allows the source to be small, but
            // scale up nicely.
            
            finalize=false; //we finalize ourselves in a callback.
            // Create a canvas element
            const canvas = document.createElement('canvas');
            const ctx = canvas.getContext('2d');

            // Load the image
            const img = new Image();
            img.src=img_url;
            img.onload=function(){
                const scaleFactor=6;
                console.log(`Using scale factor of: ${scaleFactor}`);
                canvas.width=img.width*scaleFactor;
                canvas.height=img.height*scaleFactor;

                ctx.imageSmoothingEnabled=false;
                ctx.drawImage(img,0,0,canvas.width,canvas.height);

                const imgUrl=canvas.toDataURL();
                overlay_layer= new L.imageOverlay(imgUrl, coordinates,
                    {
                        opacity:opacity,
                        interactive:true,
                        bubblingMouseEvents:false
                    }
                )

                finalize_overlay(destLayer,overlay_layer,overlayType, info);
            }

        }

        if(finalize){
            finalize_overlay(destLayer,overlay_layer,overlayType,info);
        }
    })
}

function finalize_overlay(destLayer,overlay_layer,overlayType,info){
        if('heading' in info){ //add a LOS/heading indicator.
            const overlayBounds = overlay_layer.getBounds();
            const topLeftCorner = overlayBounds.getNorthWest();

            // Define the SVG icon with dynamic rotation
            const svgIcon = L.divIcon({
                html: `<img src="static/img/LOSArrow.svg" style="width: 100px; height: 100px; transform: rotate(${info['heading']}deg);" />`,
                className: 'heading-svg-icon',
                iconSize: [100, 100], // Set the size of the icon
                iconAnchor: [0,0]
            });

            // Create the marker with the custom SVG icon at the top-left corner of the overlay
            const svgMarker = L.marker(topLeftCorner, { icon: svgIcon });

            overlay_layer=L.featureGroup([overlay_layer,svgMarker])
        }

        overlays[destLayer]['overlay']=overlay_layer;

        overlays[destLayer]['overlay'].addTo(map);

        console.log("Tile Layer Bounds:", overlay_layer.options.bounds);


        const callback=overlays[overlayType]['load'];
        if(typeof(callback)!=='undefined'){
            callback(info);
        }
}


const SignState = {
    POSITIVE:'positive',
    NEGATIVE:'negative'
}

let signState=null;
const ANTIMERIDIAN_THRESHOLD = 100

function checkOverlaySign(){
    //figure out and store the current sign state
    const center=map.getCenter();
    const curState = (center.lng>0) ? SignState.POSITIVE : SignState.NEGATIVE;

    //check if state has changed. If not, we don't need to do anything
    if(curState===signState) return;

    signState=curState

    // Check if the center is near the antemeridian.
    // We don't need to update overlays when crossing the prime meridian.
    if (Math.abs(center.lng) < ANTIMERIDIAN_THRESHOLD) return;

    const overlaysDisp = Object.keys(overlays).filter(type => {
        const info = overlays[type];
        return info.overlay instanceof L.ImageOverlay;
    });

    if (overlaysDisp.length===0) return;

    //fix overlays
    console.log("Overlays need to be fixed!");
    for (const layerType of overlaysDisp){
        const layer=overlays[layerType].overlay;
        //TODO: set bounds on tile layers, and fix here.
        const layerBounds=layer.getBounds();
        const southWest=layerBounds.getSouthWest();
        const northEast=layerBounds.getNorthEast();
        let adjust=0;

        if(curState===SignState.POSITIVE && southWest.lng<0){
            adjust=360
        } else if (curState===SignState.NEGATIVE && southWest.lng>0){
            adjust=-360;
        }

        if(adjust !==0){
            southWest.lng+=adjust;
            northEast.lng+=adjust;
            layer.setBounds(L.latLngBounds(southWest,northEast));
        }
    }
}
