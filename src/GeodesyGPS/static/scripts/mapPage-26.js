var map;
var global_graph_div = null;
var divid = 0;
var volcano;
let curVolc=null;
var vectorGuard = false;
var tabGuard=false;
let overlayTimer=null;
let initOverlayOpt=null;
const loadFutures=[];
let viewChangeTimer=null;
let globalVectorArgs=null;

const overlays={
    'insar':{
        'overlay':null,
    'target':'insar_opts',
        'load':insarDisplayed,
        'downloadEntry':'insarDownload',
        'noString':'No InSAR TS',
    'select':setInsarRef
    },
    'kml':{
        'overlay':null,
        'target':'ifrPathSelect',
        'downloadEntry':'ifrDownload',
        'noString':'No Interferogram'
    },
    'amp':{
        'overlay':'kml',
        'target':'ampPathSelect',
        'downloadEntry':'ampDownload',
        'noString':'No Amplitude'
    }
};

markerGroups = {
    'pbo': [],
    'continuous': [],
    'campaign': [],
    'seismic': []
}

var iframe_id = 0;
var menuMode = false;

$(document).ready(function() {
    init_document();
});

const sliderHTML=`
<div class="overlayOpacityDiv">
    <label>Opacity:</label>
    <input class="overlayOpacity" type="range" min=0 max=1 step=0.01 value=1>
    <input class="overlayOpacityVal" type=number min=0 max=100 value=100>%
</div>
`

function init_document() {
    //Add an opacity slider element to each overlay option
    $('.overlaySelector').each(function(){
        const element=$(sliderHTML);
        $(this).append(element);
    });

    $(document).on('click', 'button.location', setMapLocation);
    $(document).on('click', 'img.closeBtn', closeGraph);
    $(document).on('click', 'input.baselineOption', generateGraphs);
    $(document).on('click', 'span.dateBtns button', dateRangeClicked);
    $(document).on('click', '.selectButtons button', setDataView);
    $(document).on('click', '.tab.special', setSpecialText);
    $(document).on('click', '.downloadData', downloadData);
    $(document).on('click', setPlotOptions);
    $(document).on('change','input.plotFormat',setRTUOpts);
    $(document).on('change','.RTUSourceOpts',SetRTUSource);
    $(document).on('change','.RTULatLon',RTULatLonChanged);


    $('img.menu').click(showMenu);

    $(document).on('change', 'div.chartHeader input.date', changeDateRangeInput);

    $('.overlayOpacity').on('input',opacitySliderChanged);
    $('.overlayOpacityVal').change(opacityValChanged);

    $('#vectorScale').on('input', function() {
        getVectors()
    });

    $('input.siteCheck').click(setVisibility);
    $('#showQuakes, #avoOnly').click(getEarthquakes);
    $('#mapQuality').change(download_view);

    $('#insar_level').change(function(){
        const args=$('#insar_opts').data('args') || {};
        const val=this.value;

        if(val!=''){
            args['insar_limit']=this.value;
        }
        else if('insar_limit' in args){
            delete args['insar_limit'];
        }

        $('#insar_opts').data('args',args);
        load_overlay.call($('#insar_opts').get(0));
    });

    $('#showNextAcqs').change(showHideAcqs);

    $('#ifrDate').on('input',selectOverlayDate);
    $('#overlayDateFromSelect').change(function(){overlayFromDateChanged(true);});
    $('#overlayDateToSelect').change(overlayToDateChanged);

    //toggle between Interferogram and...something.
    $('.ifrSelector .switch input').change(function(){
        load_overlay.call($('#ifrPathSelect'));
    });


    $('#overlaySelectors').mouseenter(function(){
        if(overlayTimer!==null){
            clearTimeout(overlayTimer);
            overlayTimer=null;
        }
        $('#overlayWrapper').show(200);
    })

    $('#overlaySelectors').mouseleave(function(){
        overlayTimer=setTimeout(function(){
            $('.multiLevelSelect li.topLevel').removeClass('selected');
            $('#overlayWrapper').hide(200);
        },750);
    })

    volcano = getUrlVars()['volcano']

    initMap();
    initFinal();
    getS1Observations();
}

function initMap() {
    const mapDiv=document.getElementById('map')
    map = new L.Map(document.getElementById('map'), {
        center: { lat: 54.1, lng: -165.9 },
        zoom: 11,
        zoomControl:false,
    //worldCopyJump:true,
    continuousWorld:true,
    zoomSnap:0.25
    });

    const Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
        attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
    });

    const google_maps_terrain = L.tileLayer('https://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']}
    );

    //Esri_WorldImagery.addTo(map);
    google_maps_terrain.addTo(map);

    L.control.scale({position:'bottomleft'}).addTo(map);
    L.control.zoom({position:'bottomright'}).addTo(map);

    map.addEventListener('moveend', function() {
        if(!tabGuard){
            $('#locations div .tab').removeClass('current');
        }
        tabGuard=false;
        mapCenter=JSON.stringify(map.getCenter());
        $.getJSON('get_volc', {'center':mapCenter})
        .done(function(volcInfo){
            curVolc=volcInfo;
        });
    });

    map.addEventListener('zoomend', getVectors);

    // Get a list of stations to create markers for
    $.get('list_stations', map_stations);

    //add a listener to re-draw the scale bar if the map moves/zooms
    map.addEventListener('zoomend',mapViewChanged);
    map.addEventListener('moveend', mapViewChanged);
    map.addEventListener('drag', checkOverlaySign);

    //set the inital map view, if requested
    if (typeof(volcano) !== 'undefined') {
        $('#' + volcano.toLowerCase() + '_select').click();
        if(initOverlay!==null) map.whenReady(showInitOverlay);
    } else {
        map.whenReady(function(){
            getVectors();
            mapViewChanged();
            if( initOverlay!==null ) showInitOverlay();
        });
    }

    //notify the map when div size changes
    new ResizeObserver(()=>{map.invalidateSize()}).observe(mapDiv);
}

/**
    * Reloads plots by triggering a click event on the checked
    * baseline option.
    *
    * @param {jQuery} source - The element that triggered the reload,
    *                          expected to be a jQuery object.
    */
function reloadPlots(source){
    console.log("Triggering plot reload");
    const settingsDiv=source.closest('.settingsInner')
    settingsDiv.find('input.baselineOption:checked').click()
}

function RTULatLonChanged(){
    const input=$(this);
    const lat=input.siblings('.RTULatitude') || input;
    const lon=input.siblings('.RTULongitude') || input;
    if(lat.val()=='' || lon.val()=='') return;
    reloadPlots(input);
}

function setRTUOpts(){
    const btn=$(this);
    const rtuOpts=btn.closest('div.settingsInner').find('div.RTUOptions');
    const format=btn.data('format');
    if(format=='RTU'){
        rtuOpts.slideDown();
        const select=rtuOpts.find('.RTUSourceOpts');
        SetRTUSource.call(select);
    } else {
        rtuOpts.slideUp();
        reloadPlots(btn);
    }
}

function SetRTUSource(event, update){
    const select=$(this);
    if(typeof(update)=='undefined'){
        update=true;
    }

    const lat=select.siblings('.RTULatitude')
    const lon=select.siblings('.RTULongitude')
    const latlon=select.siblings('.RTULatLon')
    let val=select.val()
    if(val=='Other'){
        latlon.prop('disabled',false);
        latlon.val('');
        lat.focus();
    } else {
        latlon.prop('disabled',true);
        val=JSON.parse(val);
        lat.val(val[0]);
        lon.val(val[1]);
    }

    if(update){
        //don't update now unless there are values in lat/lon
        if(lat.val()=='' || lon.val()=='') return;
        reloadPlots(select);
    }
}

function setPlotOptions(event){
    if ($(event.target).closest('.plotSettings').length) return; // Do nothing if the click is inside the div
    if ($(event.target).closest('.settingsBtn').length){
        $(event.target).closest('.chartHeader').find('.plotSettings').slideToggle(500);
        return;
    }
    if (!$('.plotSettings').is(':visible')) return; // Do nothing if the div is not visible


    //default: close
    $('.plotSettings').slideUp(500);
}

function opacitySliderChanged(){
    const percent=this.value*100;
    $(this).siblings('.overlayOpacityVal').val(percent);
    setLayerOpacity.call(this);
}

function opacityValChanged(){
    const value=this.value/100;
    const slider=$(this).siblings('.overlayOpacity');
    slider.val(value);
    setLayerOpacity.call(slider.get(0));
}

function setLayerOpacity(){
    // "this" could be either the slider or the number input, we want the slider specifically.
    const selectMenu=$(this).closest('.overlaySelector').find('.selectMenu');
    if(selectMenu.val()==''){
        return; //this overlay isn't displayed, don't change anything
    }

    const type=selectMenu.data('type');
    const opacity=this.value;

    let layer=overlays[type]['overlay'];

    if(layer==null){
        return;
    } else if(typeof(layer)==='string'){
        layer=overlays[layer]['overlay'];
    }

    layer.setOpacity(opacity);
}

function downloadData(event){
    const optKey=event.altKey;
    const graph = $(this).closest('div.chart');
    const graphDiv = graph.find('div.plotlyPlot:visible')[0]
    const baselineCheck=graph.find('input.baselineOption:checked')
    const station=baselineCheck.data('station');
    let baseStation=baselineCheck.data('baselineStation')
    if (baseStation == "None" || baseStation=='ITRF14')
        baseStation = null;

    const dateFrom=graph.find('input.dateFrom').val();
    const dateTo=graph.find('input.dateTo').val();
    

    if($(graphDiv).hasClass("tiltArea")){
        downloadTiltData(station, dateFrom, dateTo);
    }
    else if ($(graph).hasClass('insar')){
        downloadInSARTSData(optKey,graphDiv,dateFrom,dateTo);
    }
    else{
        // probably don't need this check here, as there will have been an error earlier,
        // but better safe.
        if(station==baseStation){
            return;
        }
        
        const plotFormat=graph.find('input.plotFormat:checked').val()
        const RTULat=graph.find('input.RTULatitude').val()
        const RTULon=graph.find('input.RTULongitude').val()

        downloadGPSData(station,baseStation,dateFrom,dateTo,plotFormat,RTULat,RTULon)
    }
}

function downloadGPSData(station,baseline,from,to,format,RTULat,RTULon){
    const params = new URLSearchParams({
        station,
        from,
        to,
        baseline,
        format,
        RTULat,
        RTULon
    });

    const url = `downloadGPSData?${params.toString()}`;

    window.location=url;
}

function downloadTiltData(station,dateFrom,dateTo){
    let url='downloadTiltData?';
    station=encodeURIComponent(station)
    dateFrom=encodeURIComponent(dateFrom)
    dateTo=encodeURIComponent(dateTo)
    url+=`station=${station}&from=${dateFrom}&to=${dateTo}`

    window.location=url
}

function clearMultiLevelSelect(target){
    target.empty();
    const selectedLI=$('<li>');
    target.append(selectedLI);

    selectedLI.addClass('topLevel');

    const selectedSpan=$('<span>');
    selectedLI.append(selectedSpan);

    //selectedSpan.attr('id','selectedInSAR');
    selectedSpan.addClass('selected');
    selectedSpan.text('Select...');
    selectedSpan.data('file','');

    const topList=$('<ul>');
    selectedLI.append(topList);

    return topList;
}

function mapViewChanged(){
    if(viewChangeTimer!=null){
        clearTimeout(viewChangeTimer);
        viewChangeTimer=null;
    }

    viewChangeTimer=setTimeout(RunMapViewChanged,1800);
}

function RunMapViewChanged(){
    if(viewChangeTimer!==null){
        clearTimeout(viewChangeTimer);
        viewChangeTimer=null;
    }

    graphVectorScale();
    for( const [type,info] of Object.entries(overlays)){
    let optFunc=info['init'] || loadOverlayOpts
        loadFutures.push(optFunc(type));
    }
}

function showInitOverlay(){
    setTimeout(function(){
    console.log("Showing initial Overlay")
    const menu=$('.selectMenu').filter(function() {
        return $(this).data('type') === initOverlayType;
    });
    menu.val(initOverlay);
    menu.data('sensor',initOverlaySensor);
    initOverlay=null;
    initOverlaySensor=null;
    initOverlayType=null;

    selectOverlayIdentifier.call(menu);
    }, 0);
}

function initFinal() {
    let siteID='2';

    //Some stuff is different on the public site
    if(IS_PUBLIC){
        siteID='1';
        $('#showImages').prop('checked',true);
    }


    var _paq = window._paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u = "https://apps.avo.alaska.edu/analytics/";
        _paq.push(['setTrackerUrl', u + 'matomo.php']);
        _paq.push(['setSiteId', siteID]);
        var d = document,
            g = d.createElement('script'),
            s = d.getElementsByTagName('script')[0];
        g.type = 'text/javascript';
        g.async = true;
        g.src = u + 'matomo.js';
        s.parentNode.insertBefore(g, s);
    })();


    $(document).on('change', 'select.dataSource', changeDataSource);
    $('#CloseProgress').click(function(){
        $('#DownloadClose').hide();
        $('#DownloadStatusDialog').hide();
    })
}

let s1OBS=[];
function showHideAcqs(){
    for(const poly of s1OBS){
        if($('#showNextAcqs').is(':checked')){
            poly.addTo(map);
        } else{
            poly.remove();
        }
    }
}

const s1infoWindow=L.popup({
    minWidth:150,
    maxHeight:500,
    className:'s1infowindow'
});

function showS1Info(event){
    //"this" should be the polygon object
    const data=$(this).data('info');
    let content=`
    <b>${data['ObservationTimeStart']}</b><br>
    <table border=1 class="s1Info">
    `
    for(const key in data){
        content+=`
        <tr>
            <td>${key}</td>
            <td>${data[key]}</td>
        </tr>
        `
    }

    content+='</table>'
    s1infoWindow.setContent(content);
    s1infoWindow.setLatLng(event.latlng);
    s1infoWindow.openOn(map);
}


const S1COLOR={
    'IW':'red',
    'EW':'green',
    'SM':'black'
}

function getS1Observations(){
    $.getJSON('getNextAcq')
    .done(function(data){
        //remove any existing poly objects
        for(const poly of s1OBS){
            poly.setMap(null);
        }

        s1OBS=[];
        const tbody=$('#nextS1Table tbody').empty();

        let volc='';
        let volcSpan=0;
        let volcCell=null;
        for(const item of data){
            // Create a leaflet polygon object for this item
            if(item['polys']!=null){
                let leafletPoly=L.polygon(item['polys'],{
                    stroke:true,
                    color:S1COLOR[item['style']],
                    weight:2,
                    fill:true,
                    fillColor:S1COLOR[item['style']],
                    fillOpacity:.25,
                    className:'observationPoly',
                    noClip:true,
                });

                $(leafletPoly).data('info',item['data']);

                leafletPoly.addEventListener('click',showS1Info);

                s1OBS.push(leafletPoly);
            }

            let volcName=item['name']
            let tr=$('<tr>');

            if(volcName!=volc){
                if(volcCell!=null){
                    volcCell.attr('rowspan',volcSpan);
                }
                volc=volcName;
                volcCell=$(`<td>${volcName}</td>`);
                volcSpan=1;
                tr.append(volcCell);
            }
            else{
                volcSpan+=1;
            }
            tr.append(`<td>Path ${item['data']['OrbitRelative']}</td>`);
            tr.append(`<td>${item['data']['Mode']}</td>`);
            tr.append(`<td>${item['begin'].replace('T',' ')}</td>`);

            tbody.append(tr);
        }
        volcCell.attr('rowspan',volcSpan);
        showHideAcqs();
    })
}



function changeDataSource() {
    var chartDiv = $(this).closest('.chart');
    chartDiv.find('input.baselineOption:checked').click();
}

function checkTiltData(event, params) {
    if (layoutCount > 0) {
        layoutCount -= 1;
        return;
    }

    //see if we have any data about the x axis range being changed
    if (!event['xaxis.range[0]'] || !event['xaxis.range[1]']) {
        return;
    }

    var xaxis_range = this.layout.xaxis.range;

    var rawFromData = xaxis_range[0];
    var rawToData = xaxis_range[1];
    if (typeof(rawFromData) == "string") {
        rawFromData = rawFromData.replace(' ', 'T');
        rawFromData = parseISODate(rawFromData);
        rawToData = rawToData.replace(' ', 'T');
        rawToData = parseISODate(rawToData);
    }

    var rawFrom = new Date(rawFromData);
    var rawTo = new Date(rawToData);
    var timeSpan = ((rawTo - rawFrom) / 1000) / 60;

    var tickformat = getTickFormat(timeSpan);

    var dates = parseRangeDates(xaxis_range);

    //pull out the dates as NEW date objects so we don't change the view range when changing the date
    var dateFrom = new Date(dates[0]);
    var dateTo = new Date(dates[1]);

    // TODO: See if date range is less than it was before, and if so reload the data
    // Need to store the range used (month, year, or all) and then calculated the range requested.
    // Also need to check the earliest date for which we have loaded data, and if request is earlier than that date,
    // then reload.
    var parentChart = $(this).closest('.chart');

    //set the date input fields to match the displayed data
    var dateFromInput = parentChart.find('input.dateFrom');
    var dateToInput = parentChart.find('input.dateTo');
    var formattedFrom = formatDateString(dateFrom);
    var formattedTo = formatDateString(dateTo);

    parentChart.find('span.dateBtns button').removeClass('active');

    dateFromInput.val(formattedFrom);
    dateToInput.val(formattedTo);

    var tiltArea = parentChart.find('.tiltArea');
    var earliest_loaded = tiltArea.data('tilt_start');
    var latest_loaded = tiltArea.data('tilt_end');

    //look at day boundaries
    earliest_loaded.setUTCHours(0, 0, 0, 0);
    latest_loaded.setUTCHours(23, 59, 59, 999);

    var fetch_data = false
        //we only need to check the resolution if we have data for the range already.
        //that is, if we are zooming in but not panning. Otherwise, when we fetch more,
        //the server will decide the proper resolution to return.
    if (earliest_loaded > dateFrom) {
        //we are trying to view a range from before we have data loaded. See if we can get more.
        var oldest_data = tiltArea.data('start');
        var date_parts = oldest_data.split('-');
        oldest_data = new Date(date_parts[0], date_parts[1] - 1, date_parts[2]);
        if (oldest_data < earliest_loaded) {
            //more available! Get it!
            //all of the baseline option buttons have the station stored, so use the first one
            fetch_data = true
        }
    } else if (latest_loaded < dateTo) {
        //trying  to view data from after where we have loaded. See if more is available
        var newest_data = tiltArea.data('end');
        var date_parts = newest_data.split('-');
        newest_data = new Date(date_parts[0], date_parts[1] - 1, date_parts[2]);
        if (newest_data > latest_loaded) {
            //Yay! More is available!
            fetch_data = true
        }
    }

    if (!fetch_data) {
        //see if we can get better resolution data
        var range = parentChart.find('.tiltArea').data('range');
        if (range != 'day') {
            var one_day = 1000 * 60 * 60 * 24;
            //note: these are just thresholds at which reloading makes sense.
            //i.e. if we are currently viewing all data, it makes sense to reload
            //if we have zoomed in to a year or less. The actual range to be loaded
            //will be determined by the server.
            var day_span = Math.floor((rawTo - rawFrom) / one_day);
            if (range == 'month') {
                var threshold = 0;
            } else if (range == 'year') {
                var threshold = 32;
            } else if (range == 'all') {
                var threshold = 367;
            }

            if (day_span <= threshold) {
                fetch_data = true
            }
        }
    }

    if (fetch_data) {
        var station = parentChart.find('input.baselineOption:first').data('station');
        get_tilt_data(parentChart, station, rawFrom, rawTo);
    } else {
        //scale the Y axis to match the displayed data
        rescaleY(parentChart, dateFrom, dateTo);
        var max_polar = setPolarDateRange($(this), [dateFrom, dateTo]);
        layoutCount += 1;
        Plotly.relayout(this, {
            'polar.radialaxis.range': [0, max_polar],
            'xaxis.tickformat': tickformat,
            'xaxis2.tickformat': tickformat,
            'xaxis3.tickformat': tickformat
        });
        setTitle(parentChart);
    }

}

function setDataView() {
    //"this" is the button that was clicked - GPS vs Tilt
    $(this).closest('.selectButtons').children().removeClass('current');
    $(this).addClass('current');

    var chartDiv = $(this).closest('.chart')

    if(IS_PUBLIC){
        var selected_range = $('span.dateBtns button.current');
        if (selected_range.length > 0) {
            //"click" the selected date range to reload the image
            selected_range.click();
        } else {
            //get the default range for this graph type
            get_graph_image.call(global_graph_div);
        }
    }
    else{
        //hide both, then show the proper one
        chartDiv.find('.tiltTop, .gpsTop').hide();

        var viewTarget = $(this).data('view');

        chartDiv.find(viewTarget).show();
        var targetGraph = chartDiv.find($(this).data('graph'));

        //resize/zoom graphs asynchronously
        setTimeout(function() {
            Plotly.Plots.resize(targetGraph[0]);
            setGraphRange.call(targetGraph[0]);
        }, 5);
    }
}

function registerDoubleClick(event) {
    var context = this;
    setTimeout(function() {
        setZoomRange.call(context, event);
    }, 100);
}

function setZoomRange(event, params) {
    if (layoutCount > 0) {
        layoutCount -= 1;
        return;
    }

    // Check if we have any data about the x-axis range being changed
    if (event!=null && (!event['xaxis.range[0]'] || !event['xaxis.range[1]'])) {
        return;
    }

    var xaxis_range = this.layout.xaxis.range;

    //Set the input fields to match the xaxis range
    var parentChart = $(this).closest('div.chart');
    if(parentChart.hasClass('insar')){
        setInsarDates(xaxis_range);
        return;
    };

    var dateFromInput = parentChart.find('input.dateFrom');
    var dateToInput = parentChart.find('input.dateTo');

    parentChart.find('span.dateBtns button').removeClass('active');

    var dates = parseRangeDates(xaxis_range);
    var dateFrom = new Date(dates[0]);
    var dateTo = new Date(dates[1]);

    //Range and limits here are off by one. Adjust
    dateTo = new Date(dateTo.setDate(dateTo.getDate() - 1));

    var formattedFrom = formatDateString(dateFrom);
    var formattedTo = formatDateString(dateTo);
    dateFromInput.val(formattedFrom);
    dateToInput.val(formattedTo);

    setTitle(parentChart);
    rescaleY(parentChart, dateFrom, dateTo);
    getVectors.call(this);
    getEarthquakes();
}

function setPolarDateRange(tiltArea, d_range, date_data, polar_data, update) {
    update = update ?? true;
    var tiltArea = $(tiltArea);

    //the x axis from the first graph is our date axis. We'll use it,
    //since the polar graph only has r and theta
    var date_data = date_data ?? tiltArea.data('graph_data')[0]['x'];

    // get the actual start/end dates, given that the
    // data may not exactly match the specified range.
    //find the start and end indicies for this date range
    const avg_from = new Date(d_range[0] - 1000 * 60 * 60 * 2) //two hours before start
    const dateFrom = d_range[0];
    const dateTo = d_range[1];
    var start_idx = 0;
    var avg_start_idx = 0;
    var found_avg_start = false;
    var found_start = false;
    var found_end = false;
    var end_idx = date_data.length - 1;

    for (var i = 0; i < date_data.length; i++) {
        var date = parseISODate(date_data[i]);
        if (!found_avg_start && date >= avg_from) {
            found_avg_start = true;
            avg_start_idx = i;
        }

        if (!found_start && date >= dateFrom) {
            found_start = true;
            start_idx = i;
        }

        if (!found_end && date >= dateTo) {
            found_end = true;
            end_idx = i - 1;
            break;
        }
    }

    //get the actual start and end dates at the located indexes for display
    var startDate = formatDateString(new Date(date_data[start_idx]), true);
    var endDate = formatDateString(new Date(date_data[end_idx]), true);

    var full_r = tiltArea.data('polar_r');
    var full_theta = tiltArea.data('polar_theta');
    var polar_idx = tiltArea.data('polar_idx');

    // get the current trace data structure and then remove it.
    // We will modify the data and re-add it later.
    var cur_data =  polar_data ?? tiltArea[0].data[polar_idx];


    var new_r = full_r.slice(start_idx, end_idx);
    var new_theta = full_theta.slice(start_idx, end_idx);

    var avg_r = full_r.slice(avg_start_idx, start_idx + 1);
    var avg_theta = full_theta.slice(avg_start_idx, start_idx + 1);

    var color_basis = new_r.length - 1;
    var colors = []

    //re-zero the new data
    //figure out the zero point
    avg_x_val = 0;
    avg_y_val = 0;
    num_avg_vals = avg_r.length

    for (var i = 0; i < num_avg_vals; i++) {
        var r = avg_r[i];
        var t = avg_theta[i];
        avg_x_val += r * Math.cos(t);
        avg_y_val += r * Math.sin(t);
    }

    var x0 = avg_x_val / num_avg_vals;
    var y0 = avg_y_val / num_avg_vals;

    var max = -1 * Number.MAX_VALUE;
    for (var i = 0; i < new_r.length; i++) {
        colors.push(i / color_basis);

        var xi = (new_r[i] * Math.cos(new_theta[i])) - x0;
        var yi = (new_r[i] * Math.sin(new_theta[i])) - y0;

        var new_r_val = Math.sqrt((xi * xi) + (yi * yi));
        var new_theta_value = Math.atan2(yi, xi);

        if (new_r_val > max)
            max = new_r_val;

        new_r[i] = new_r_val;
        new_theta[i] = new_theta_value;
    }

    //stupid plotly bug. Or me. No idea, really.
    if (new_r.length >= 10000) {
        colors.reverse();
        new_r.reverse();
        new_theta.reverse();
    }



    tiltArea.closest('.chart').find('.polarScale .scaleStart').text(startDate);
    tiltArea.closest('.chart').find('.polarScale .scaleStop').text(endDate);
    cur_data['marker']['colorbar']['ticktext'] = [startDate, endDate];

    if(update){
        const update_obj = {
        'marker.color': [colors],
        'r': [new_r],
        'theta': [new_theta]
    }
    Plotly.restyle(tiltArea[0], update_obj, polar_idx);
    } else {
        const update_obj = {
        'marker.color': colors,
        'r': new_r,
        'theta': new_theta
    }
    return [max,update_obj]
    }

    return max;
}

function setGraphRange() {
    //"this" is the date input that changed. Could be either dateFrom or dateTo.
    var graph = $(this).closest('div.chart');

    var dateFromVal = graph.find('input.dateFrom').val();
    var dateToVal = graph.find('input.dateTo').val();
    var dateFrom = new Date(dateFromVal);
    //dateFrom=new Date(dateFrom.setUTCHours(0,0,0,0));

    //dateTo to end of day
    var dateTo = new Date(dateToVal);
    dateTo = new Date(dateTo.setHours(23, 59, 59, 999));

    var graphDiv = graph.find('div.plotlyPlot:visible')[0]

    if (typeof(graphDiv.data) == 'undefined') {
        if(dateFromVal!=='' && dateToVal!==''){
            const station=graph.find('div.baselineSel div.basebuttons input.baselineOption:first').data('station');
            return get_tilt_data(graph,station,dateFromVal,dateToVal);
        }
        return; //no graph (yet, at least);
    }

    var layout = graphDiv.layout;
    var layout_updates = {}
    for (var i = 1; i <= graphDiv.data.length; i++) {
        var xaxis = i == 1 ? 'xaxis' : 'xaxis' + i
        if (layout.hasOwnProperty(xaxis)) {
            var prop = xaxis + '.range';
            layout_updates[prop] = [dateFrom, dateTo];
        }
    }

    if ($(graphDiv).hasClass("tiltArea")) {
        const max_polar_val = setPolarDateRange(graphDiv, [dateFrom, dateTo]);
        layout_updates['polar.radialaxis.range'] = [0, max_polar_val];
    }

    var y_layouts = rescaleY(graph, dateFrom, dateTo, false);

    layout_updates = Object.assign(layout_updates, y_layouts);
    Plotly.relayout(graphDiv, layout_updates);
    setTitle(graph)

    getVectors.call(this);
    getEarthquakes();
}

function setTitle(parentChart) {
    var graphDiv = parentChart.find('div.plotlyPlot:visible')[0]
    var dateFrom = parentChart.find('input.dateFrom').val();
    var dateTo = parentChart.find('input.dateTo').val();
    var station = parentChart.find('.baselineOption:first').data('station');
    var plot_title = parentChart.find('.chartTitle').text();
    const baselineStation=parentChart.find('.baselineOption:checked').data('baselineStation')

    if ($(graphDiv).hasClass('tiltArea')) {
        plot_title = plot_title.split(' - ')[0] + ' Tilt ';
        plot_title += dateFrom + " to " + dateTo;
        var y = .939;
    } else {
        plot_title += " - " + dateFrom + " to " + dateTo;
        var y = .935;
    }

    var filename = station + " movement ";
    filename += dateFrom.replace(/\//g, '-') + " to ";
    filename += dateTo.replace(/\//g, '-');
    var newConfig = {
        responsive: true,
        'toImageButtonOptions': {
            'format': 'svg',
            'filename': filename,
            'height': 816,
            'width': 700
        }
    }

    const referenceOptions = ["ITRF14", "NOAM (geodvel)"];
    const label = referenceOptions.includes(baselineStation) ? "Reference" : "Baseline";

    var title_dict = {
        'text': plot_title,
        'x': .068,
        'y': y,
        'xanchor': 'left',
        'yanchor': 'bottom',
        'font': {
            'size': 16,
        },
        'subtitle':{
            'text':`${label}: ${baselineStation}`
        }
    }

    Plotly.relayout(graphDiv,{'title':title_dict});
}

const debounceMap = new Map();
const DEBOUNCE_TIME = 300; // Adjust the debounce time as needed (in milliseconds)

function plotGraph(graphDiv, data, layout, config) {
    if (debounceMap.has(graphDiv)) {
        clearTimeout(debounceMap.get(graphDivId));
    }

    const timeout = setTimeout(() => {
        _plotGraph(graphDiv, data, layout, config);
        debounceMap.delete(graphDiv);
    }, DEBOUNCE_TIME);

    debounceMap.set(graphDiv, timeout);
}

function _plotGraph(graphDiv, data, layout, config, callback){
    let promise=null;

    let $graphDiv;
    if(graphDiv instanceof jQuery){
        $graphDiv=graphDiv;
        graphDiv=graphDiv[0];
    } else {
        $graphDiv=$(graphDiv);
    }

    if (typeof(data) == 'undefined') {
        //nothing provided. Load it from the specified div
        data = $graphDiv.data('graph_data');
        layout = $graphDiv.data('graph_layout');
        config = $graphDiv.data('graph_config');
    } else {
        //values provided. Save them to the specified div
        $graphDiv.data('graph_data', data)
        $graphDiv.data('graph_layout', layout)
        $graphDiv.data('graph_config', config)
    }

    if(graphDiv.data){
        data = data || graphDiv.data;
        layout = layout || graphDiv.layout;
        config = config || graphDiv._context; // _context holds the config in Plotly
        promise=Plotly.react(graphDiv,data,layout,config);
    } else {
        // Plot does not exist, use Plotly.newPlot() to create it
        promise = Plotly.newPlot(graphDiv, data, layout, config);
    }

    promise.then(()=>{
        console.log("Checking for GL errors")
        const canvas = $graphDiv.find('canvas')[0];
        const gl = canvas.getContext('webgl');
        const error = gl.getError();

        if (error != 0) {
            Plotly.purge(graphDiv);

            //Will create a new plot rather than trying to update the existing (if any)
            plotGraph(graphDiv, data, lyout, config)
            console.log("*******GL Error:" + error + "*******")
            return;
        }

        const isTilt=$graphDiv.hasClass('tiltArea');

        setTitle($(graphDiv).closest('.chart'));

        console.log("Attaching event listeners to plot");
        //attach event listeners
        if(isTilt){
            graphDiv.on('plotly_relayout',function(event){
                checkTiltData.call(graphDiv, event);
            });
        }

        graphDiv.on('plotly_relayout',function(event){
            setZoomRange.call(graphDiv,event);
        });
        graphDiv.on('plotly_doubleclick',function(event){
            registerDoubleClick.call(graphDiv,event);
        });

        callback?.();
    });
}

function rescaleY(parentChart, dateFrom, dateTo, run, data, layout) {
    run=run??true;

    //try to figure out the Y axis for each graph
    //decompose the date from and date to into the format of the data
    var fromMonth = dateFrom.getUTCMonth() + 1;
    if (fromMonth < 10)
        fromMonth = "0" + fromMonth;
    var fromDay = dateFrom.getUTCDate();
    if (fromDay < 10)
        fromDay = "0" + fromDay;
    var dateFromString = dateFrom.getUTCFullYear() + '-' + fromMonth + "-" + fromDay;

    var toMonth = dateTo.getUTCMonth() + 1;
    if (toMonth < 10)
        toMonth = "0" + toMonth;

    var toDay = dateTo.getUTCDate();
    if (toDay < 10)
        toDay = "0" + toDay;
    var dateToString = dateTo.getUTCFullYear() + '-' + toMonth + "-" + toDay;
    dateToString += 'T23:59:59'

    let graphs;
    let isTilt=false;
    if(parentChart!==null){
        graphs = parentChart.find('div.plotlyPlot:visible')[0]
        isTilt=graphs.classList.contains("tiltArea")
    } else {
        graphs={
            data:data,
            layout: layout
        }
    }

    var dateData = graphs.data[0]['x']; //same for all plots, so just use the first

    var startIdx = 0; //technically should be end of list, since if nothing is greater than the start value, then the start value is past the end,
    //however we'll go with zero so we don't wind up with an empty list, even if the zoom range is beyond the end of the list.
    var startValue = dateData.find(function(element) {
        return element >= dateFromString;
    });
    if (typeof(startValue !== 'undefined'))
        startIdx = dateData.indexOf(startValue);

    //this will return the value or undefined, so I need to find the index of the value and handle undefined.
    var stopIdx = dateData.length - 1; //minus 1 because index is 0 based, length is 1 based
    var stopValue = dateData.find(function(element) {
        return element >= dateToString
    });

    if (typeof(stopValue) !== 'undefined') //no number greater than this in list, this is the greatest!
        stopIdx = dateData.indexOf(stopValue); //we know this is in the list, because we found it above.

    var layouts = {};
    //apparently the data and axis are just associated by order
    var all_yaxis = filtered_keys(graphs.layout, /yaxis?/).sort();

    for (var i = 0; i < graphs.data.length; i++) {
        //possible that some subgraphs don't have a yAxis (polar, for example)
        if (!graphs.data[i].hasOwnProperty('y'))
            continue;

        //remove the start value from each graph, so they start at zero
        //but not for tilt data
        if(!isTilt){
            const startVal=graphs.data[i]['y'][startIdx]
            for (let j=0; j<graphs.data[i]['y'].length; j++) {
                graphs.data[i]['y'][j]-=startVal;
            }
        }
        
        var yData = graphs.data[i]['y'].slice(startIdx, stopIdx + 1);
        var yAxis = all_yaxis.shift();

        var max = -1 * Number.MAX_VALUE;
        var min = Number.MAX_VALUE;
        
        for (let j = 0; j < yData.length; j++) {
            if (yData[j] > max) { max = yData[j]; }
            if (yData[j] < min) { min = yData[j]; }
        }

        //leave a small buffer on either side when displaying
        max += 1;
        min -= 1;

        layouts[yAxis + '.range'] = [min, max];
    }

    //See if we should display the "no data" label
    var lastDataDate = new Date(dateData[dateData.length - 1])

    if(parentChart!==null){
        if (lastDataDate < dateFrom) {
            parentChart.find('div.plotlyPlot:visible').addClass("noData");
        } else {
            parentChart.find('div.plotlyPlot:visible').removeClass("noData");
        }	
    }

    if (run && parentChart!==null) {
        Plotly.update(graphs, {
            'y': graphs.data.map(trace => trace.y)
        },
        layouts);
        
        //layoutCount+=1;
        //Plotly.restyle(graphs, {
            
        //}, [...Array(graphs.data.length).keys()]);
        //layoutCount += 1;
        //Plotly.relayout(graphs, layouts);
    } else
        return layouts
}

function filtered_keys(obj, filter) {
    var key, keys = [];
    for (key in obj) {
        if (obj.hasOwnProperty(key) && filter.test(key)) {
            keys.push(key);
        }
    }
    return keys;
}

function changeDateRangeInput() {
    var context = this;

    //since we manually changed the date range, de-select the preset buttons
    $('button.active').removeClass('active');

    setTimeout(function() { setGraphRange.call(context); }, 5);
}

function dateRangeClicked() {
    if ($(this).hasClass('custom')) {
        //only for public site
        var selector = $('#rangeSelector');
        selector.show();
        return;
    }

    setDateRange.call(this);
}

function setDateRange(btn,startDate,endDate, update){
    update=update ?? true
    btn=btn ?? this

    btn=$(btn) //probably $(this)
    var chartDiv = btn.closest('div.chart');

    endDate=endDate ?? new Date();


    if(!IS_PUBLIC) {
        var graphDiv = chartDiv.find('.plotlyPlot:visible');
    }

    if (startDate == null) {
        var type = btn.data('type');
        var step = btn.data('step'); //may be null

        if (type == 'all') {
            if(IS_PUBLIC){
                startDate=null;
            }
            else{
                startDate = new Date(graphDiv.data('start'));
            }
        } else if (type == 'year') {
            startDate = new Date();
            startDate.setYear(endDate.getFullYear() - Number(step));
        } else if (type == 'month') {
            startDate = new Date();
            startDate.setMonth(endDate.getMonth() - Number(step));
        } else if (type == 'day') {
            startDate = new Date();
            startDate.setDate(endDate.getDate() - Number(step));
        }
    }

    if(IS_PUBLIC){
        if(update)
            get_graph_image.call(global_graph_div, null, startDate, endDate);

        //save the start and end date for easy retreval
        global_graph_div.data('start_date', startDate);
        global_graph_div.data('end_date', endDate);
    }
    else{
        chartDiv.find('input.dateFrom').val(formatDateString(startDate));
        const dateToEntry = chartDiv.find('input.dateTo').val(formatDateString(endDate));

        if(update) {
            setTitle(chartDiv);
            setTimeout(function() { setGraphRange.call(dateToEntry[0]); }, 5);
        }
    }

    chartDiv.find('span.dateBtns button').removeClass('active');
    btn.addClass('active');

}

function get_graph_image(event, dfrom, dto) {
    $('#loadMsg').show();
    $('#graphImage').hide();
    if (typeof(dfrom) == 'undefined') {
        var dateFromObj = new Date(); //current date/time
        //one year ago by default
        dateFromObj = new Date(dateFromObj - (365 * 24 * 60 * 60 * 1000));
    } else {
        var dateFromObj = dfrom;
    }

    if (dateFromObj != null) {
        dateFromObj.setUTCHours(0, 0, 0, 0);
        var dateFrom = formatISODateString(dateFromObj);
        dateFrom = dateFrom.slice(0, -1) + ".000Z";
    } else {
        var dateFrom = null;
    }


    if (typeof(dto) == 'undefined') {
        var dateToObj = new Date();
    } else {
        var dateToObj = dto;
    }

    dateToObj.setUTCHours(23, 59, 59, 999);
    var dateTo = formatISODateString(dateToObj)
    dateTo = dateTo.slice(0, -1) + ".999Z";

    var station = $(this).data('id');
    var site = $(this).data('site');

    var has_tilt = $(this).data('has_tilt'); //to provide a tilt option or not
    var get_tilt = $('div.selectButtons button.current').hasClass('selectTilt');
    var base = $(`#${site}_select`).data('baselines')[0];

    var query = []
    query.push('station=' + encodeURIComponent(station));
    query.push('baseline=' + encodeURIComponent(base));
    query.push('dfrom=' + encodeURIComponent(dateFrom));
    query.push('dto=' + encodeURIComponent(dateTo));
    query.push('tilt=' + encodeURIComponent(get_tilt));

    url = `gen_graph?${query.join("&")}`;

    if (global_graph_div === null) {
        chartDiv = createChartDiv(station, site);
        $('#content').append(chartDiv);
        global_graph_div = chartDiv;
    } else {
        $('.dateBtns .tab').removeClass('active');
        $('.tab.1y').addClass('active');
        //save the start and end date for easy retreval
        if (dateFromObj === null) {
            global_graph_div.data('start_date', dateFromObj);
        } else {
            global_graph_div.data('start_date', formatDateString(dateFromObj));
        }

        global_graph_div.data('end_date', formatDateString(dateToObj));
    }

    global_graph_div.data('baseline', base);
    global_graph_div.data('site', site);

    // set the url for the permalink
    $('#permalink').attr('href', url);

    global_graph_div.data('id', station);
    global_graph_div.data('site', site);
    global_graph_div.data('has_tilt', has_tilt);

    var img = document.getElementById('graphImage');
    $(img).parent().find('div.error').remove();
    img.src = url;

    //show/hide the tilt tab as appropriate
    if (has_tilt) {
        chartDiv.find('.selectButtons').show();
    } else {
        chartDiv.find('.selectButtons').hide();
    }

    if ($('button.selectGPS').hasClass('selected'))
        getVectors();
    else {
        clearVectors();
    }
}

function getChartDiv(event, station, site, show_tilt, showSelectors){
    if(typeof(show_tilt)==='undefined'){
        show_tilt=false;
    }

    if(typeof(showSelectors)==='undefined'){
        showSelectors=true;
    }

    const available_charts = $('div.chart:hidden')
    const visible_charts = $('div.chart:visible')
    const add_chart = typeof(event) == 'undefined' | event===null ? false : event.shiftKey;
    if (add_chart && visible_charts.length == 3) {
        alert("Sorry, but due to browser limitations, only three sets of graphs can be shown at once");
        return;
    }

    let chartDiv = null;
    if (!add_chart && (visible_charts.length > 0 || available_charts.length > 0)) { //re-use the existing div
        if (visible_charts.length > 0)
            chartDiv = visible_charts.last();
        else //must have no visible, but one or more hidden.
            chartDiv = available_charts.first();

        chartDiv.find('div.chartHeader').remove();

        //purge any existing plotly plots
        chartDiv.find('div.js-plotly-plot').each(function(){
            Plotly.purge(this);
        });

        // make sure the main plot is the visible one
        chartDiv.find('div.tiltTop').hide();
        chartDiv.find('div.gpsTop').show();

        // remove all but the first graphArea, if there are multiple
        // I belelieve this is no longer needed - only one is ever created/used
        // but left in for safety as it doesn't hurt anything.
        let graphAreas=chartDiv.find('div.graphArea:visible');
        const toRemove=graphAreas.slice(1);
        for(let i=0;i<toRemove.length;i++){
            const extraDiv=toRemove[i];
            extraDiv.remove();
        }

        //if this used to be an insar plot, remove that distinction
        chartDiv.removeClass('insar');

        chartDiv.prepend(createChartHeader(station, site, showSelectors));
    } else if (add_chart && available_charts.length > 0) {
        chartDiv = available_charts.first();
        chartDiv.find('div.chartHeader').remove();
        chartDiv.prepend(createChartHeader(station, site, showSelectors));
    } else { //create a new div. Not trying to add, and nothing available to replace
        chartDiv = createChartDiv(station, site, showSelectors);
        $('#content').append(chartDiv);
    }

    if(add_chart && visible_charts.length>0){
        const base_chart=visible_charts.first();
        const base_start=base_chart.find('input.dateFrom').val();
        const base_stop=base_chart.find('input.dateTo').val();
        const base_reference=base_chart.find('input.baselineOption:checked').data('baselineStation')
        const base_format=base_chart.find('input.plotFormat:checked').val()
        const rtu_option=base_chart.find('select.RTUSourceOpts').val();
        let disabled=rtu_option!='Other';
        const rtu_lat=base_chart.find('input.RTULatitude').val()
        const rtu_lon=base_chart.find('input.RTULongitude').val()

        chartDiv.find('input.dateFrom').val(base_start);
        chartDiv.find('input.dateTo').val(base_stop);

        //find and check the appropriate baseline selector
        chartDiv.find('input.baselineOption').filter(function(){
            return $(this).data('baselineStation')==base_reference
        }).prop('checked',true)

        //and the appropriate format
        if(base_format=='RTU')
            chartDiv.find('div.RTUOptions').show()

        chartDiv.find(`input.plotFormat[value="${base_format}"]`).prop('checked',true)
        const selectElem=chartDiv.find('select.RTUSourceOpts');
        selectElem.val(rtu_option);
        if (selectElem.val() !== rtu_option) {
            selectElem.val('Other'); // Set to a known safe value
            disabled=false;
        }
        chartDiv.find('input.RTULatLon').prop('disabled',disabled);

        //fill the lat/lon fields
        chartDiv.find('input.RTULatitude').val(rtu_lat)
        chartDiv.find('input.RTULongitude').val(rtu_lon)
    }

    //show/hide the tilt tab as appropriate
    if (show_tilt) {
        chartDiv.find('.selectButtons').show();
    } else {
        chartDiv.find('.selectButtons').hide();
    }
    chartDiv.data('has_tilt', show_tilt);

    return chartDiv
}

function showStationGraphs(event) {
    clearInSARSelects();
    showStationImage(this);

    if(IS_PUBLIC){
        return get_graph_image.call(this,event);
    }

    //get the TRUE event from the fake GoogleMaps event
    event = Object.values(event)
        .filter(function(property) {
            return property instanceof window.MouseEvent;
        })[0];

    var station = $(this).data('id');
    var site = $(this).data('site');
    var has_tilt = $(this).data('has_tilt');

    chartDiv=getChartDiv(event,station,site,has_tilt);

    //trigger generation of the default set of graphs
    let baseline=chartDiv.find('input.baselineOption:checked') || chartDiv.find('input.baselineOption').first()
    baseline.click();

    chartDiv.find('.selectButtons button.selected').click(); //make sure the proper graph is displayed

}

function createChartHeader(station, site, showSelectors) {
    if(typeof showSelectors=='undefined'){
        showSelectors=true;
    }

    divid += 1;
    var chartHeader = $('<div class=chartHeader>');

    const leftDiv=$('<div class="headerSection headerLeft">')
    const settingsOuter=$('<div class="headerSection plotSettings">')
    const centerDiv=$('<div class="settingsInner">');
    const rightDiv=$('<div class="headerSection headerRight">')

    settingsOuter.append(centerDiv);
    chartHeader.append(leftDiv);
    chartHeader.append(settingsOuter);
    chartHeader.append(rightDiv);

    rightDiv.append("<img src='static/img/Hamburger_icon.svg' class='settingsBtn noPrint'/>")

    if(!IS_PUBLIC){
        //data download button
        var downloadButton = $('<button class="downloadData">Download Data</button>')
        leftDiv.append(downloadButton)

        //title
        var chartTitle = $('<span class="chartTitle">');
        centerDiv.append(chartTitle);
        chartTitle.append('<b><span class="stationName">' + station +
            "</span></b>"
        );

        //close button
        rightDiv.append("<img src='static/img/RedClose.svg' class='closeBtn noPrint'/>")

        if(showSelectors){
            //baseline selector title
            var baselineSelector = $('<div class="baselineSel">');
            var radioButtons = $("<div class='inlineblock basebuttons'>");
            baselineSelector.append(radioButtons)

            //baseline selector options
            var baseOptions = $('#' + site + "_select").data('baselines');

            if (!baseOptions.includes("ITRF14"))
                baseOptions.push("ITRF14"); //append a "none" option to the baseline options

            if(!baseOptions.includes("NOAM (geodvel)"))
                baseOptions.push("NOAM (geodvel)");

            for (var idx in baseOptions) {
                var baseline = baseOptions[idx];
                var button = $('<input type="radio" class="baselineOption">');
                button.prop('name', divid + '_base');
                button.data('station', station);
                button.data('baselineStation', baseline);
                radioButtons.append(button);
                radioButtons.append(" ");
                radioButtons.append(baseline);
                radioButtons.append(" ");
            }

            baselineSelector.find('input[type=radio]:first').prop('checked', true);

            centerDiv.append("<span class='rightAlign'>Reference:</span>");
            centerDiv.append(baselineSelector);
        }
    }

    //data switch buttons
    var data_sel_div = $('<div class="selectButtons">');
    data_sel_div.append('<button class="selectGPS tab current" data-view=".gpsTop" data-graph=".graphArea">GPS</button>');
    data_sel_div.append('<button class="selectTilt tab"  data-view=".tiltTop" data-graph=".tiltArea">Tilt</button>');
    leftDiv.prepend(data_sel_div);

    //date selector for GPS
    var gpsDate = $('<div class="gpsDateSelect">');
    var dateBtns = $('<span class="dateBtns">');

    if(showSelectors) {
        if(IS_PUBLIC){
            //date selector buttons
            dateBtns.append("<button class='tab all' data-type='all'>all</button>");
            dateBtns.append("<button class='tab 1y active' data-type='year' data-step=1>1y</button>");
            dateBtns.append("<button class='tab 6m' data-type='month' data-step=6>6m</button>");
            dateBtns.append("<button class='tab 1m' data-type='month' data-step=1>1m</button>");
            dateBtns.append("<button class='tab 1w' data-type='day' data-step=7>1w</button>");
            dateBtns.append("<button class='tab custom' data-type='day' data-step=7>Custom</button>");
        }
        else{
            centerDiv.append("<span class=rightAlign>Date Range:</span>");
            gpsDate.append("<input type=text size=10 class='dateFrom date' maxlength=10/>")
            gpsDate.append("-");
            gpsDate.append("<input type=text size=10 class='dateTo date' maxlength=10/>");

            //date selector buttons
            dateBtns.append("<button data-type='all'>all</button>");
            dateBtns.append("<button class='default' data-type='year' data-step=1>1y</button>");
            dateBtns.append("<button data-type='month' data-step=6>6m</button>");
            dateBtns.append("<button data-type='month' data-step=1>1m</button>");
            dateBtns.append("<button data-type='day' data-step=7>1w</button>");
        }

        gpsDate.append(dateBtns);
    }
    leftDiv.append('<div class=loading style="display:none">');

    centerDiv.append(gpsDate);
    if(showSelectors && !IS_PUBLIC){
        //Format selector
        centerDiv.append("<span class=rightAlign>Plot Format:</span>")
        const formatSelector=$('<div>',{
            class:"plotFormatSelectors"
        });

        centerDiv.append(formatSelector)
        //NEU Format
        formatSelector.append($('<input>',{
            type:"radio",
            class:"plotFormat formatNEU",
            name:`format${divid}`,
            'data-format':'NEU',
            id:`formatNEU${divid}`,
            value:'NEU',
            checked:true
        }));
        formatSelector.append($('<label>',{
            for:`formatNEU${divid}`,
            text:'NEU'
        }))
        formatSelector.append($('<input>',{
            type:"radio",
            class:"plotFormat formatRTU",
            name:`format${divid}`,
            'data-format':'RTU',
            value:'RTU',
            id:`formatRTU${divid}`
        }));
        formatSelector.append($('<label>',{
            for:`formatRTU${divid}`,
            text:'RTU'
        }));

        const rtuOptions=$('<div>',{
            class:'RTUOptions',
            style:'display:none'
        });
        centerDiv.append(rtuOptions);

        rtuOptions.append('Source:')
        const sourceOpts=$('<select>',{
            class:'RTUSourceOpts'
        });

        for (const [key, value] of Object.entries(stationVolcs[station])) {
            sourceOpts.append($('<option>',{
                text:key,
                value:value
            }));
        }

        sourceOpts.append($('<option>',{
            text:'Other'
        }));
        rtuOptions.append(sourceOpts);
        rtuOptions.append($('<input>',{
            type:'text',
            size:6,
            class:'RTULatitude RTULatLon',
            placeholder:'lat'
        }));
        rtuOptions.append($('<input>',{
            type:'text',
            size:6,
            class:'RTULongitude RTULatLon',
            placeholder:'lon'
        }));

        SetRTUSource.call(sourceOpts,null,false);
    }

    return chartHeader;
}

function createChartDiv(station, site,showSelectors) {
    if(typeof(showSelectors)==='undefined'){
        showSelectors=true;
    }

    var chartDiv = $('<div class="chart">');

    var chartHeader = createChartHeader(station, site, showSelectors);
    chartDiv.append(chartHeader);

    if(!IS_PUBLIC){
        //graph div for GPS plots
        var gpsTop = $('<div class=gpsTop>')
        chartDiv.append(gpsTop);
        const graphArea=$('<div>',{
            class:"graphArea plotlyPlot"
        })
        gpsTop.append(graphArea);

        //attach event listeners
        graphArea[0].addEventListener('plotly_relayout', setZoomRange);
        graphArea.on('plotly_doubleclick',registerDoubleClick);

        var tiltTop = $('<div class=tiltTop style="display:none">')
        chartDiv.append(tiltTop)
        var tiltWrapper = $('<div class=tiltWrapper>');
        tiltTop.append(tiltWrapper);
        tiltWrapper.append('<div class="tiltArea plotlyPlot"><br>')
        var colorScale = $('<div class="polarScale" style="display:none">');
        colorScale.append('<div class="scaleStart">');
        colorScale.append('<div class="scaleStop">');
        tiltWrapper.append(colorScale);
    }
    else{
        //graph div for GPS plots
        var graphArea = $('<div class="graphArea">');
        chartDiv.append(graphArea);

        //permalink
        graphArea.append("<div class='permalink'><a id='permalink' target=_blank >Permalink to this graph</a></div>");
        //loading indicator
        var loading = $('<div class=loadMsg id="loadMsg">');
        loading.append("Loading image. Please wait...");
        loading.append("<br>");
        loading.append('<div class=loading>');
        graphArea.append(loading);

        var img = $("<img id='graphImage' class='graphImg'>");
        img[0].onload = img_load_success;
        img[0].onerror = img_load_err;
        graphArea.append(img);
        var tiltTop = $('<div class=tiltTop style="display:none">')
        chartDiv.append(tiltTop)
        var tiltWrapper = $('<div class=tiltWrapper>');
        tiltTop.append(tiltWrapper);
        tiltWrapper.append('<div class="tiltArea"><br>')

        var rangeSelector = $('<div id=rangeSelector>');
        var rangeCals = $('<div class="rangeCals">');
        rangeSelector.append(rangeCals);
        var calFrom = $('<div class="cal" id="calFrom">');
        rangeCals.append(calFrom);
        var calTo = $('<div class="cal" id="calTo">');
        rangeCals.append(calTo);
        rangeSelector.hide();

        var calOpts = {
            changeMonth: true,
            changeYear: true
        }

        calFrom.datepicker(calOpts);
        calTo.datepicker(calOpts);

        var setRangeBtn = $('<button id="setRange">')
        setRangeBtn.text('Set');
        setRangeBtn.click(setCustomRange);

        var cancelRangeBtn = $('<button id=cancelRange>');
        cancelRangeBtn.text('Cancel');
        cancelRangeBtn.click(function() {
            $('#rangeSelector').hide();
        })

        var rangeBtns = $('<div class="rangeBtns">');
        rangeBtns.append(cancelRangeBtn);
        rangeBtns.append(setRangeBtn);
        rangeSelector.append(rangeBtns);

        chartHeader.append(rangeSelector);
    }

    return chartDiv;
}

function setCustomRange() {
    var btn = $('button.custom.tab');
    var dateFrom = $('#calFrom').datepicker("getDate");
    if (dateFrom === null) {
        alert("Must select start date to do a custom range!");
        return;
    }

    var dateTo = $('#calTo').datepicker("getDate");
    if (dateTo === null) {
        alert("Must select end date to do a custom range!");
        return;
    }

    if (dateFrom >= dateTo) {
        alert("Start date must be prior to end date!");
        return;
    }

    $('#rangeSelector').hide();

    return setDateRange(btn, dateFrom, dateTo);
}

function img_load_err(err) {
    $('#loadMsg').hide();
    $(this).hide();
    $(this).parent().append("<div class=error>Unable to load data</div>");
}

function img_load_success(event) {
    $('#loadMsg').hide();
    $(this).show();
}

dlUpdateTimer=null;

function download_view() {
    if($('#mapQuality').val()=='select'){ return; }

    const quality=$('#mapQuality').val();
    $('#mapQuality').val('select');

    if (quality.startsWith('tiff')){
        const type=quality.split('-')[1];
        generateGeoTIFF(type);
        return;
    }

    showMessage("PDF Generation Requested.<br>Requested file(s) will download once ready. Please wait...")
    $('#DownloadStatus').html('Waiting...');
    $('#DownloadClose').hide();
    $('#downloadLink').hide();
    $('#DownloadStatusDialog').show();
    if(dlUpdateTimer!==null){
        clearInterval(dlUpdateTimer);
    }

    dlUpdateTimer = setInterval(function(){
        $.get('map/getGenStat')
        .done(function(resp){
            $('#DownloadStatus').html(resp);
            if(resp=="DONE"){
                $('#DownloadClose').show();
                $('#downloadLink').show();
                const MSG="Complete<br><br>If your download does not begin automatically"
                $('#DownloadStatus').html(MSG);
                $.get('map/resetGenStat')
                clearInterval(dlUpdateTimer);
                dlUpdateTimer=null;

                location='map/get_image';
            }
        });
    },1000);

    generateMapImage(quality);
    saveCharts();
}

function saveCharts() {
    $('div.chart:visible .plotlyPlot:visible').each(function(idx, element) {
        var div = $(element);

        let graphHeight=700;
        if(div.hasClass('tiltArea')){
            graphHeight=800;
        }
        else if (div.closest('.chart').hasClass('insar')){
            graphHeight=300;
        }

        var data = element.data;
        var layout = element.layout;

        var args = {
            height: graphHeight,
            data: data,
            layout: layout
        }

        dom_post('map/gen_graph', args)
    });
}

function generateGeoTIFF(type){
    let sensor,file,ref_point,max_level;
    const url=`overlay/geotiff/${type}`

    if(type=='insar'){
        sensor=$('#insar_opts').data('sensor');
        file=$('#insar_opts').val();
        ref_point=$('#insar_opts').data('args')['ref_point'];
        max_level=$('#insar_level').val();
    } else if( type == 'ifr' ){
        sensor=$('#ifrPathSelect').data('sensor');
        file=$('#ifrPathSelect').data('filepath');
    } else if( type == 'amp'){
        sensor=$('#ampPathSelect').data('sensor');
        file=$('#ampPathSelect').data('filepath');
    } else{
        alert(`Can't get GeoTIFF of type "${type}"`);
        return;
    }

    if(sensor=='' || !file){
        alert('No overlay of selected type displayed');
        return;
    }

    const dest=`${url}?sensor=${sensor}&file=${file}&ref_point=${ref_point}&insar_limit=${max_level}`
    const downloadwin=window.open(dest);
    downloadwin.focus();
    downloadwin.onblur=function(){downloadwin.close();}
}

function generateMapImage(quality) {
    var mapBounds = boundsToJSON(map.getBounds());
    let quakes = $(document).data('quakeCache');

    if(typeof(quakes)==='undefined'){
        quakes=[];
    }
    quakes=JSON.stringify(quakes);

    console.log(mapBounds);
    var params = getVectorParameters();
    if(typeof(params)=='undefined') params=globalVectorArgs;
    params['map_bounds'] = JSON.stringify(mapBounds);
    params['quakes'] = quakes;
    params['quality']=quality;
    params['insar'] = JSON.stringify([$('#insar_opts').val(), $('#insar_opts').data('sensor')]);
    params['ifr']= JSON.stringify( [ $('#ifrPathSelect').data('filepath'), $('#ifrPathSelect').data('sensor')] );
    params['amp']= JSON.stringify( [ $('#ampPathSelect').data('filepath'), $('#ampPathSelect').data('sensor')] );
    
    const selectedDataTypes = $('.siteCheck:checked:visible')
        .map((_, checkbox) => $(checkbox).data('type'))
        .get();
    params['station_types']=JSON.stringify(selectedDataTypes)

    Object.assign(params,parseInsar());

    $.post('map/download',params)
    .done(function(resp){
        console.log(resp)
        const filename=resp['MapImage']
        const url=`map/get_image?file=${filename}`;
        let link=$('<a>')
        link.text("Click Here");
        link.attr('href',url);
        $('#downloadLink').html(link);
    })
    .fail(function(jqXHR, textStatus, errorThrown){
        alert('Unable to generate map image');
        if (dlUpdateTimer!==null){
            clearInterval(dlUpdateTimer);
            dlUpdateTimer=null;
        }
    });
}

function get_tilt_data(dest, station, from, to) {
    $(document).off('plotly_afterplot', 'div.tiltArea');
    dest.find('div.loading').show();
    var argFrom = from;
    var argTo = to;

    if (typeof(from) == "object") {
        argFrom = formatFakeISODateString(from)
    }

    if (typeof(to) == "object") {
        argTo = formatFakeISODateString(to)
    }

    $.get('get_tilt_data', {
        'station': station,
        'from': argFrom,
        'to': argTo
    })
    .fail(function(err) {
        $(document).on('plotly_afterplot', 'div.tiltArea', function() {
            $(this).closest('div.chart').find('div.loading').hide();
        });

        if (err.status == 404) {
            dest.find('div.tiltWrapper').addClass('noData');
        }
        //hide the loading div
        dest.find('div.loading').hide();

        const tiltDiv = dest.find('div.tiltArea');
        Plotly.purge(tiltDiv[0]);
    })
    .done(function(data) {

    //TODO: FIX THIS
        $(document).on('plotly_afterplot', 'div.tiltArea', function() {
            $(this).closest('div.chart').find('div.loading').hide();
        });

        dest.find('div.tiltWrapper').removeClass('noData');

        if (typeof(from) == "object") { from = formatDateString(from) }
        if (typeof(to) == "object") { to = formatDateString(to) }

        var tiltDiv = dest.find('div.tiltArea');

    //if we already have a plot, purge it.
        if (data['dates'].length == 0) {
            Plotly.purge(tiltDiv[0]);
            return;
        }

        tiltDiv.data('tilt_start', parseISODate(data['search_start'])); //earliest data loaded for this station
        tiltDiv.data('tilt_end', parseISODate(data['search_end'])); //newest data loaded for this station
        tiltDiv.data('end', data['stop_date']); //newest data available for this station
        tiltDiv.data('start', data['start_date']); //earliest data available for this station
        var range = data['range'];

        tiltDiv.data('range', range);

        var x_trace = makeTiltDataDict(data['dates'], data['x_tilt'], 'red', 1);
        //x_trace['text']=data['dates']
        var y_trace = makeTiltDataDict(data['dates'], data['y_tilt'], 'blue', 2);
        //y_trace['text']=data['dates']
        var temp_trace = makeTiltDataDict(data['dates'], data['temps'], 'black', 3);

        if (range === 'day') {
            var line_dict = { 'width': .5 }
            x_trace.mode = 'lines';
            x_trace['line'] = line_dict;
            y_trace.mode = 'lines';
            y_trace['line'] = line_dict;
            temp_trace.mode = 'lines';
            temp_trace['line'] = line_dict;
        }

        var plot_start = new Date(Date.parse(data['dates'][0].replace(' ', 'T')));
        var plot_end = new Date(Date.parse(data['dates'][data['dates'].length - 1].replace(' ', 'T')));
        plot_start = formatDateString(plot_start, true)
        plot_end = formatDateString(plot_end, true)

        var polar_trace1 = {
            type: 'scatterpolargl',
            r: data['radius'],
            theta: data['theta'],
            thetaunit: "radians",
            mode: 'markers',
            marker: {
                color: data['timestamps'],
                colorscale: [
                    ['0.0', '#ACD6E6'],
                    ['1.0', '#1A0391']
                ],
                size: 7,
                colorbar: {
                    thickness: 15,
                    len: .3,
                    ypad: 0,
                    lenmode: "fraction",
                    x: 1,
                    xanchor: "right",
                    y: .685,
                    yanchor: "bottom",
                    tickvals: [0, 1],
                    ticktext: [plot_start, plot_end],
                    tickmode: 'array',
                    tickfont: { size: 9.6 }
                }
            },
            cliponaxis: false,
        }

        var x_axis = {
            type: 'scatterpolargl',
            r: data['x_radius'],
            theta: data['x_theta'],
            mode: 'lines',
            line: {
                color: "black",
            },
        }

        var y_axis = {
            type: 'scatterpolargl',
            r: data['y_radius'],
            theta: data['y_theta'],
            mode: 'lines',
            line: {
                color: "black",
            },
        }

        var graph_data = [x_trace, y_trace, temp_trace];
        var layout = generateSubgraphLayout(
    graph_data,
    ['', 'X (Microradians)', 'Y (Microradians)', 'Degrees C']
    );

        //add another row
        layout.grid.rows = 4;

        //set the hover format
        layout['xaxis2']['hoverformat'] = "%m/%d/%y %H:%M:%S";
        layout['xaxis3']['hoverformat'] = "%m/%d/%y %H:%M:%S";
        layout['xaxis4']['hoverformat'] = "%m/%d/%y %H:%M:%S";

        layout['annotations'] = [{
            xref: 'paper',
            yref: 'paper',
            x: .02,
            xanchor: 'left',
            y: .2125,
            yanchor: 'top',
            text: "<b>Temperature</b>",
            showarrow: false,
            bgcolor: 'rgba(255, 255, 255, 0.25)',
            font: { size: 12 }
        },
        {
            xref: 'paper',
            yref: 'paper',
            x: .5,
            xanchor: 'center',
            y: .988,
            yanchor: 'bottom',
            text: "<b>" + station + " Tilt (microradians)</b>",
            showarrow: false,
            bgcolor: 'rgba(255, 255, 255, 0.25)',
            font: { size: 12 }
        },
        {
            xref: 'paper',
            yref: 'paper',
            x: .02,
            xanchor: 'left',
            y: .4325,
            yanchor: 'top',
            text: "<b>Y Tilt</b>",
            showarrow: false,
            bgcolor: 'rgba(255, 255, 255, 0.25)',
            font: { size: 12 }
        },
        {
            xref: 'paper',
            yref: 'paper',
            x: .02,
            xanchor: 'left',
            y: .6525,
            yanchor: 'top',
            text: "<b>X Tilt</b>",
            showarrow: false,
            bgcolor: 'rgba(255, 255, 255, 0.25)',
            font: { size: 12 }
        }
        ]

        layout['polar'] = {
            'domain': {
                'y': [0.69, 0.964],
                'x': [0, 1],
            },
            'angularaxis': {
                tickvals: [90, 0, 270, 180, data['x_axis'], data['y_axis']],
                ticktext: ['<b>North</b>', '<b>E</b>', '<b>South<b>', '<b>W</b>', '<b>+X</b>', '<b>+Y<b>'],
                linewidth: 1
            },
            'radialaxis':{
                'autorange':false
            }
        }

        //Position the graphs
        row_height = .22;
        seg_sep = 0.015;
        var axis = ['yaxis4', 'yaxis3', 'yaxis2'];
        for (var i = 0; i < axis.length; i++) {
            var start = row_height * i + (seg_sep / 2);
            var stop = row_height * (i + 1) - (seg_sep / 2);
            layout[axis[i]]['domain'] = [start, stop];
        }

        config = {
            responsive: true,
            'toImageButtonOptions': {
                'format': 'svg',
                'height': 800,
                'width': 600,
                'scale': 1.2,
                filename: station + " Tilt " + from.replace(/\//g, '-') + " to " + to.replace(/\//g, '-')
            }
        }

        //We don't want to change the range, if any, so set it to the current value
        try {
            var x_range = tiltDiv[0].layout.xaxis.range;
            layout.xaxis2.range = x_range;
            layout.xaxis3.range = x_range;
            layout.xaxis4.range = x_range;

            //for the polar plot
            var range_dates = parseRangeDates(x_range);
            var range_from = formatDateString(range_dates[0], true);
            var range_to = formatDateString(range_dates[1], true);
            //graph_data[polar_idx].marker.colorbar.ticktext = [range_from, range_to];
        } catch (TypeError) {
            var x_range = layout.xaxis4.range;
        }

        x_range = parseRangeDates(x_range);
        var x_span = ((x_range[1] - x_range[0]) / 1000) / 60
        var tickformat = getTickFormat(x_span);

        layout.xaxis2.tickformat = tickformat;
        layout.xaxis3.tickformat = tickformat;
        layout.xaxis4.tickformat = tickformat;

        //Rename some axis to make plotly happy :(
        for (var i = 0; i < 3; i++) {
            var xaxis = i == 0 ? 'xaxis' : `xaxis${i + 1}`;
            var yaxis = i == 0 ? 'yaxis' : `yaxis${i + 1}`;
            layout[xaxis] = layout[`xaxis${i+2}`];
            if (i != 2) {
                layout[xaxis]['matches'] = 'x3';
            }
            layout[yaxis] = layout[`yaxis${i+2}`];
        }

    //save some stuff for modifying the polar graph
    tiltDiv.data('polar_r', data['radius']);
    tiltDiv.data('polar_theta', data['theta']);

    if (typeof(x_range) !== 'undefined') {
        const [max_polar, update_obj] = setPolarDateRange(
            tiltDiv,
            x_range,
            data['dates'],
            polar_trace1,
            false
        );

        layout.polar = layout.polar || {};
        layout.polar.radialaxis = layout.polar.radialaxis || {};
        layout.polar.radialaxis.range = [0, max_polar];

        for (const [key,item] of Object.entries(update_obj)) {
            updateNestedObject(polar_trace1, key, item);
        }
    }

    graph_data.push(x_axis);
    graph_data.push(y_axis);
    graph_data.push(polar_trace1);

    //store the index of the actual polar trace
    var polar_idx = graph_data.length - 1; //zero based index
    tiltDiv.data('polar_idx', polar_idx);

        delete layout.xaxis4;
        delete layout.yaxis4;

        plotGraph(tiltDiv, graph_data, layout, config);
    });
}


function makeTiltDataDict(x, y, color, idx) {
    var trace = {
        x: x,
        y: y,
        type: 'scattergl',
        mode: 'markers',
        marker: {
            size: 2,
            color: color
        },
    }

    if (typeof(idx) !== 'undefined') {
        trace['yaxis'] = 'y' + idx;
        trace['xaxis'] = 'x' + idx;
    }

    return trace;
}

function generateGraphs() {
    //"this" is the button that was clicked
    var station = $(this).data('station');

    var optionsDiv = $(this).closest('div.baselineSel');
    var dest = optionsDiv.closest('div.chart');
    dest.show();

    //find the baseline selection to use
    var baseStation = optionsDiv.find('input[type=radio]:checked').data('baselineStation');
    if (baseStation == "ITRF14")
        baseStation = null;

    if (station == baseStation) {
        alert("Trying to plot " + station + " relative to a baseline of itself. This probably doesn't make much sense. Please select a different baseline.");
        return;
    }

    //find the format to use
    const settingsDiv=dest.find('div.settingsInner');
    const plotFormat=settingsDiv.find('input.plotFormat:checked').val()
    const RTULat=settingsDiv.find('input.RTULatitude').val()
    const RTULon=settingsDiv.find('input.RTULongitude').val()

    var reqParams = {
        'station': station,
        'baseline': baseStation,
        'format':plotFormat,
        'RTULat':RTULat,
        'RTULon':RTULon
    }

    Plotly.purge(dest.find('.graphArea')[0]);
    Plotly.purge(dest.find('.tiltArea')[0]);

    $.get('get_graph_data', reqParams, function(data) {
        graphResults(data, dest);
        if (dest.data('has_tilt')){
            const dateFrom = dest.find('input.dateFrom').val();
            const dateTo = dest.find('input.dateTo').val();
            get_tilt_data(dest, station, dateFrom, dateTo);
        }
    });

}

function getVectorParameters(baseObj) {
    // "this" should be *something* within a chart...
    if (typeof(baseObj) !== 'undefined')
        var graphDiv = $(baseObj).closest('div.chart');
    else {
        var graphDiv = [] //length 0
    }

    if (graphDiv.length == 0) {
        //might be empty
        graphDiv = $('div.chart:visible').first();
    }

    var baseline = graphDiv.find('div.baselineSel input.baselineOption:checked');

    //if nothing checked, use the first in the list for the selected volcano
    //note: this won't work well if the map has been moved manually
    if (baseline.length === 0) {
        if (global_graph_div != null) {
            // Only the case for the public graph
            var baseStation = global_graph_div.data('baseline');
            var station = global_graph_div.data('site');
        } else {
            try {
                var baseStation = $('button.location.current').data('baselines')[0];
                var station = $('button.location.current').data('site');
            } catch (error) {
                //use the last selected station if nothing else works
                try {
                    var station = vector_station;
                    var station_button_id = station.toLowerCase().replace(' ', '');
                    var baseStation = $('#' + station_button_id + '_select').data('baselines')[0];
                } catch (error) {
                    return; //can't get the baseline to map against, so can't get the vectors
                }
            }
        }
    } else {
        var baseStation = baseline.data('baselineStation');
        var station = baseline.data('station')
    }

    vector_station = station;

    if (global_graph_div != null) {
        var dateFrom = global_graph_div.data('start_date');
        var dateTo = global_graph_div.data('end_date');
    } else {
        var dateFrom = graphDiv.find('input.dateFrom').val();
        var dateTo = graphDiv.find('input.dateTo').val();
        var newData = graphDiv.find('select.dataSource').val()
    }


    if (typeof(dateFrom) == 'undefined' || typeof(dateTo) == 'undefined' || dateFrom == '' || dateTo == '') {
        //default to one year if we don't have a date set
        var today = new Date();
        var dateTo = formatDateString(today);
        var oneYear = today.setFullYear(today.getFullYear() - 1)
        var dateFrom = formatDateString(new Date(oneYear));
    }

    if (IS_PUBLIC){
        var scale=9;
    }
    else{
        var scale = $('#vectorScale').val();
    }

    var zoom_level = map.getZoom() || map.getCameraPosition().zoom;

    reqParams = {
        date_from: dateFrom,
        date_to: dateTo,
        station: station,
        baseline: baseStation,
        zoom: zoom_level,
        scale: scale,
        newData: newData
    }

    return reqParams;
}

function graphResults(data, dest) {
    //find the div for the east/west graph
    var graphDiv = dest.find('div.graphArea');

    var baseline = dest.find('.baselineOption:checked')
    var station = baseline.data('station');
    var baselineStation = baseline.data('baselineStation');
    const format=dest.find('.plotFormat:checked').val()


    if (data['dates'].length == 0) {
        //make sure there is no graph in this div
        Plotly.purge(graphDiv[0]);
        graphDiv.parent().show()
        graphDiv.html('<h2 class="noDataWarning">No data found for given parameters</h2>');
        return;
    } else {
        graphDiv.find('.noDataWarning').remove();
    }

    //store the earliest date for future reference
    graphDiv.data('start', data['dates'][0])

    var ew_trace = makePlotDataDict(data['dates'], data['EW'], data['EWE'], data['rapid'])
    var ns_trace = makePlotDataDict(data['dates'], data['NS'], data['NSE'], data['rapid'], 2)
    var ud_trace = makePlotDataDict(data['dates'], data['UD'], data['UDE'], data['rapid'], 3)

    var graph_data = [ew_trace, ns_trace, ud_trace]

    let labels=['EAST (cm)', 'NORTH (cm)', 'VERTICAL (cm)']
    if (format=='RTU'){
        labels=['TRANSVERSE (cm)', 'RADIAL (cm)', 'VERTICAL (cm)']
    }

    //If no dateFrom has been entered, default to one year. Otherwise, use the date range entered.
    let dateFrom = dest.find('input.dateFrom');
    if (dateFrom.val() == ""){
        const defaultBtn=dest.find('span.dateBtns button.default');
        setDateRange(defaultBtn,null,null,false);
    }
    else {
        //if we have a preset selected, click it to adjust the dates to the proper values
        //for that preset. Otherwise, just use the dates as entered.
        var selectedPreset = dest.find('span.dateBtns button.active');
        if (selectedPreset.length > 0)
            setDateRange(selectedPreset,null,null,false);
    }

    //Get the date range (set above), and update the layout(s)

    dateFrom = dest.find('input.dateFrom').val();
    const dateTo=dest.find('input.dateTo').val();
    const layout = generateSubgraphLayout(graph_data, labels, dateFrom, dateTo)

    const y_layouts=rescaleY(
        null,
        parseLocalDate(dateFrom),
        parseLocalDate(dateTo),
        false,
        graph_data, 
        layout
    );

    for (const [key,item] of Object.entries(y_layouts)) {
        updateNestedObject(layout, key, item);
    }

    let filename = station + " movement ";
    filename += dateFrom.replace(/\//g, '-') + " to ";
    filename += dateTo.replace(/\//g, '-');

    plotGraph(graphDiv, graph_data, layout, {
        responsive: true,
        'toImageButtonOptions': {
            format: 'svg',
            'height': 816,
            'width': 700,
            filename: filename
        }
    });
}

function makePlotDataDict(x, y, error, color, idx) {
    var trace = {
        x: x,
        y: y,
        error_y: {
            type: 'data',
            array: error,
            visible: true,
            color: 'rgb(100,50,50)'
        },
        type: 'scattergl',
        mode: 'lines+markers',
        marker: {
            size: 4,
            cmin: 0,
            cmax: 1,
            colorscale: [
                [0, 'rgb(55,128,256)'],
                [1, 'rgb(256,128,55)']
            ],
            color: color
        },
        line: {
            color: 'rgba(55,128,191,.25)'
        },
    }

    if (typeof(idx) !== 'undefined') {
        trace['yaxis'] = `y${idx}`;
        trace['xaxis'] = `x${idx}`;
    }

    return trace;
}

function generateSubgraphLayout(data, titles, dateFrom, dateTo) {
    var path_idx = window.location.pathname.lastIndexOf("/");
    var path = path_idx > 0 ? window.location.pathname.substring(0, path_idx) : ''
    var img_path = `${window.location.origin}${path}/static/img`;
    var layout = {
        "paper_bgcolor": 'rgba(255,255,255,1)',
        "plot_bgcolor": 'rgba(255,255,255,1)',
        showlegend: false,
        margin: {
            l: 50,
            r: 25,
            b: 25,
            t: 80,
            pad: 0
        },
        grid: {
            rows: 3,
            columns: 1,
            pattern: 'independent',
            'ygap': 0.05,
        },
        'font': { 'size': 12 },
        "images": [{
            "source": `${img_path}/uaf_gi_logo.png`,
            "xref": "paper",
            "yref": "paper",
            "x": 1,
            "y": 1.008,
            "sizex": .27,
            "sizey": .27,
            "xanchor": "right",
            "yanchor": "bottom"
        }, {
            "source": `${img_path}/avo_logo.png`,
            "xref": "paper",
            "x": .72,
            "y": 1.008,
            "sizex": .1062,
            "sizey": .27,
            "xanchor": "right",
            "yanchor": "bottom"
        }],
    }

    //figure the x-axis bounds
    //x-axis is same for all plots
    let x_range;
    if(typeof(dateFrom)=='undefined' || typeof(dateTo)=='undefined'){
        var x_data = data[0]['x'];
        x_start = new Date(x_data[0])
        x_range = [x_data[0], x_data[x_data.length - 1]]
    } else {
        x_range=[parseLocalDate(dateFrom), parseLocalDate(dateTo)];
    }

    x_range = parseRangeDates(x_range)

    for (var i = 1; i <= titles.length; i++) {
        var section_title = titles[i - 1];
        if (section_title == '') {
            continue;
        }

        var y_axis = 'yaxis' + i;
        var x_axis = 'xaxis' + i;

        layout[y_axis] = {
            zeroline: false,
            title: {
                text:section_title,
            },
            'gridcolor': 'rgba(0,0,0,.3)',
            'showline': true,
            'showgrid': false,
            'linecolor': 'rgba(0,0,0,.5)',
            'mirror': true,
            'ticks': "inside"
        }

        layout[x_axis] = {
            automargin: true,
            autorange: false,
            range: x_range,
            type: 'date',
            tickformat: "%Y.%m.%d",
            hoverformat: "%m/%d/%Y",
            'gridcolor': 'rgba(0,0,0,.3)',
            'showline': true,
            'showgrid': false,
            'mirror': true,
            'linecolor': 'rgba(0,0,0,.5)',
            'ticks': "inside"
        }

        if (i != titles.length) {
            layout[x_axis]['matches'] = `x${titles.length}`;
            layout[x_axis]['showticklabels'] = false;
        }
    }

    return layout;
}
