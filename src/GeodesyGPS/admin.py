import json

import flask

from psycopg2.extras import RealDictCursor
from psycopg2 import sql

from . import app, utils


@app.route("/admin")
@utils.require_admin
def admin():
    """
    Draw and return the main admin page
    """
    STATION_LIST = """
    SELECT
        stations.id,
        stations.name as name,
        latitude::float as lat,
        longitude::float as lon,
        elevation::float alt,
        type,
        coalesce(sites.name, 'Unknown') as site,
        sites.id as siteid,
        coalesce((SELECT true
                  FROM tilt_data
                  INNER JOIN tilt_orientation
                  ON tilt_data.station=tilt_orientation.station
                  WHERE tilt_data.station=stations.id
                  LIMIT 1),
                false) as has_tilt,
        array_to_json(volcano) as stationvolcs,
        coalesce(
            (SELECT array_agg(s.volcano_name ORDER BY t.ord)
                FROM volcano s
                JOIN unnest( volcano ) WITH ORDINALITY t(id,ord)
                ON (t.id=s.volcano_id)
            ), '{}' ) volcnames
    FROM stations
    LEFT JOIN sites ON stations.siteref=sites.id
    ORDER BY site, name
    """

    SITE_LIST = """
    SELECT
        id,
        name,
        lat,
        lon,
        zoom,
        array_to_json(baselines) as baselines,
        coalesce(
            (SELECT array_agg(s.name ORDER BY t.ord)
                FROM stations s
                JOIN unnest( baselines ) WITH ORDINALITY t(id,ord)
                USING (id)
            ), '{}' )basenames
    FROM sites
    ORDER BY name;
    """

    VOLC_LIST = """
    SELECT
        volcano_id as id,
        volcano_name as name
    FROM volcano
    WHERE volcano_id=volcano_parent_id
    AND observatory='avo'
    ORDER BY volcano_name
    """

    with utils.db_cursor(RealDictCursor) as cursor:
        cursor.execute(SITE_LIST)
        sites = cursor.fetchall()

        cursor.execute(STATION_LIST)
        stations = cursor.fetchall()

        cursor.execute(VOLC_LIST)
        volcanoes = cursor.fetchall()

    volcano_lookup = {x["name"]: x["id"] for x in volcanoes}

    return flask.render_template(
        "admin.html",
        stations=stations,
        sites=sites,
        volcanoes=volcanoes,
        volcanoLookup=volcano_lookup,
    )


@app.route("/sites")
def sites():
    SITE_LIST = """
    SELECT
        id,
        name,
        lat,
        lon,
        zoom,
        baselines
    FROM sites
    ORDER BY name;
    """

    with utils.db_cursor(RealDictCursor) as cursor:
        cursor.execute(SITE_LIST)
        sites = cursor.fetchall()

    return flask.jsonify(sites)


@app.route("/admin/site/<siteID>", methods=["POST"])
@utils.require_admin
def update_site(siteID):
    SQL = """
    UPDATE sites SET
        name=%(name)s,
        lat=%(lat)s,
        lon=%(lon)s,
        zoom=%(zoom)s,
        baselines=%(baselines)s::uuid[]
    WHERE id=%(id)s
    RETURNING id
    """

    args = dict(flask.request.form)
    args["baselines"] = json.loads(args["baselines"])

    record_id = utils.process_form_sql(SQL, args)
    return flask.jsonify(
        {
            "id": record_id,
        }
    )


@app.route("/admin/site/<siteID>", methods=["DELETE"])
@utils.require_admin
def delete_site(siteID):
    with utils.db_cursor() as cursor:
        try:
            cursor.execute("DELETE from sites WHERE id=%s", (siteID,))
        except Exception as e:
            return str(e), 409

        cursor.connection.commit()
    return "", 204


@app.route("/admin/site", methods=["PUT"])
@utils.require_admin
def add_site():
    SQL = """
    INSERT INTO sites (
        name,
        lat,
        lon,
        zoom,
        baselines
    )
    VALUES (
        %(name)s,
        %(lat)s,
        %(lon)s,
        %(zoom)s,
        %(baselines)s::uuid[]
    )
    RETURNING id
    """

    args = dict(flask.request.form)
    args["baselines"] = json.loads(args["baselines"])

    record_id = utils.process_form_sql(SQL, args)
    return flask.jsonify(
        {
            "id": record_id,
        }
    )


@app.route("/admin/station", methods=["PUT"])
@utils.require_admin
def add_station():
    SQL = """
    INSERT INTO stations (
        name,
        latitude,
        longitude,
        elevation,
        disp_name,
        type,
        siteref,
        location,
        volcano)
    VALUES (
        %(name)s,
        %(latitude)s,
        %(longitude)s,
        %(altitude)s,
        %(name)s,
        %(type)s,
        %(siteid)s,
        ST_POINT(%(longitude)s, %(latitude)s, 4326),
        %(volcano)s::varchar[])
    RETURNING id"""

    return save_station(SQL)


@app.route("/admin/station/<stationID>", methods=["POST"])
@utils.require_admin
def update_station(stationID):
    SQL = """
    UPDATE stations SET
        name=%(name)s,
        latitude=%(latitude)s,
        longitude=%(longitude)s,
        elevation=%(altitude)s,
        disp_name=%(name)s,
        type=%(type)s,
        siteref=%(siteid)s,
        location=ST_POINT(%(longitude)s, %(latitude)s, 4326),
        volcano=%(volcano)s::varchar[]
    WHERE id=%(id)s
    RETURNING id"""

    return save_station(SQL, stationID)


def save_station(SQL, stationID=None):
    args = dict(flask.request.form)

    setVolc = args.get("volcano")
    if not setVolc:
        setVolc = "[]"
    args["volcano"] = json.loads(setVolc)

    tilt_orientations = args.pop("tilt", "[]")
    orientations = json.loads(tilt_orientations)

    record_id = utils.process_form_sql(SQL, args)

    if record_id is not None:
        # two options: update/add, or delete
        for orientation in orientations:
            if orientation["delete"]:
                delete_orientation(orientation["id"])
            else:
                orientation["station"] = record_id
                save_orientation(orientation["id"], orientation)

    return flask.jsonify({"id": record_id})


@app.route("/admin/station/<stationID>", methods=["DELETE"])
@utils.require_admin
def delete_station(stationID):
    """
    DELETE a specified station record

    ARGUMENTS
    ---------
    stationID: str
        The id of the record to be deleted

    """

    with utils.db_cursor() as cursor:
        cursor.execute("DELETE FROM gps_data WHERE station=%s", (stationID,))
        cursor.execute("DELETE FROM stations WHERE id=%s", (stationID,))
        cursor.connection.commit()

    return "", 204


#################
# Tilt station orientation functions. Set up in REST style for no particular reason
#################


@app.route("/admin/orientations/<recordID>", methods=["DELETE"])
@utils.require_admin
def delete_orientation(recordID):
    """
    DELETE a specified orientation record

    ARGUMENTS
    ---------
    recordID: str
        The id of the record to be deleted

    """
    with utils.db_cursor() as cursor:
        cursor.execute("DELETE FROM tilt_orientation WHERE id=%s", (recordID,))
        cursor.connection.commit()

    return "", 204


def save_orientation(recordID, args):
    """
    SAVE changes to an orientation record, or create a new one.

    If the recordID value is 0, a new record will be created.
    Otherwise, the record with the specified ID will be updated.
    Specifying a non-existant ID will result in a 500 error.
    X and Y orientation should be seperated by 90 degrees.

    ARGUMENTS
    ---------
    recordID: uuid | int
        The UUID of the record to be updated, or 0 for a new record
    args: dict
        dictionary of record information containing the following keys:

        date: str
            The date on which this orientation takes effect
        station: str
            The station to which the orientation applies
        x: float
            Orientation of the x axis, in decimal degrees
        y: float
            Orientation of the y axis, in decimal degrees
    """

    if recordID == "0" or recordID == 0:
        SQL = """
        INSERT INTO tilt_orientation
            (station,seton,x_orientation,y_orientation)
        VALUES
            (%(station)s, %(seton)s, %(x)s, %(y)s)
        RETURNING id
        """
    else:
        SQL = """
        UPDATE tilt_orientation
        SET seton=%(seton)s,
            x_orientation=%(x)s,
            y_orientation=%(y)s
        WHERE id=%(id)s
        RETURNING id
        """

        if "id" not in args:
            args["id"] = recordID

    with utils.db_cursor() as cursor:
        cursor.execute(SQL, args)
        record_id = cursor.fetchone()
        if record_id is None:
            # Nothing returned. Something went wrong with the query
            return False
        else:
            cursor.connection.commit()
            return True


@app.route("/admin/orientations/<stationID>")
@utils.require_admin
def orientations(stationID=None):
    """
    List orientations for a given station, or all stations if None

    ARGUMENTS
    ---------
    stationID: str (optional, default: None)
        The station to retrieve tilt orientations for, or all stations if None

    RETURNS
    -------
    orientations: list
        JSON encoded list of station orientations.
    """
    LIST_SQL = """
        SELECT
            disp_name,
            to_char(seton at time zone 'UTC','YYYY-MM-DD HH24:MI') as seton,
            x_orientation,
            y_orientation,
            tilt_orientation.id as orientation_id,
            stations.id as station_id
        FROM tilt_orientation
        INNER JOIN stations
        ON stations.id=tilt_orientation.station """

    if stationID is not None:
        LIST_SQL += "WHERE stations.id=%s "
        args = (stationID,)
    else:
        args = ()

    LIST_SQL += "ORDER BY disp_name,seton"
    with utils.db_cursor(RealDictCursor) as cursor:
        cursor.execute(LIST_SQL, args)
        orientations = cursor.fetchall()

    return flask.jsonify(orientations)


@app.route("/admin/tsx/items")
def get_tsx_items():
    LIST_SQL = """
    SELECT
        id,
        volcano,
        (SELECT volcano_name FROM volcano WHERE volcano_id=volcano) as volc_name,
        mode,
        polarization,
        ascending,
        CASE
            WHEN ascending=true THEN 'Ascending'
        ELSE
            'Descending'
        END as direction,
        orbit,
        spot,
        incidence,
        ordername,
        to_char(aprox_time,'HH:MI') aprox_time,
        targetx,
        targety,
        side,
        coords,
        rotation,
        notes
    FROM tsx
    ORDER BY ordername,ascending
    """
    with utils.db_cursor(RealDictCursor) as cursor:
        cursor.execute(LIST_SQL)
        items = [dict(x) for x in cursor]

    return flask.jsonify(items)


@app.route("/admin/tsx/items", methods=["POST"])
def post_tsx_items():
    #  One of create, edit, or remove
    action = flask.request.form["action"]
    items = json.loads(flask.request.form["data"])
    print(items, action)

    # Build the SQL query
    with utils.db_cursor() as cursor:
        for row_id, fields in items.items():
            if action == "UPDATE":
                set_statements = [
                    sql.SQL("{}={}").format(sql.Identifier(key), sql.Literal(value))
                    for key, value in fields.items()
                ]
                set_clause = sql.SQL(", ").join(set_statements)
                query = sql.SQL("UPDATE tsx SET {} WHERE id={}").format(
                    set_clause, sql.Literal(row_id)
                )
            if action == "INSERT":
                query = sql.SQL("INSERT INTO tsx ({}) VALUES ({})").format(
                    sql.SQL(", ").join(map(sql.Identifier, fields.keys())),
                    sql.SQL(", ").join(map(sql.Literal, fields.values())),
                )
            if action == "DELETE":
                query = sql.SQL("DELETE FROM tsx WHERE id in ({})").format(
                    sql.SQL(", ").join(map(sql.Literal, fields.keys()))
                )

            cursor.execute(query)
            cursor.connection.commit()

    return flask.jsonify({"error": None})
