import logging
import pickle

import lz4.frame
import redis

from cachetools import Cache, cached
from cachetools.keys import hashkey
from functools import wraps


class RedisCache(Cache):

    def __init__(self, host="localhost", port=6379, db=0, ttl=600, maxsize=128):
        super().__init__(maxsize)
        self.ttl = ttl
        self.host = host
        self.port = port
        self.db = db
        self.ttl = ttl
        self.redis_client = self._create_redis_client()

    def _create_redis_client(self):
        try:
            return redis.StrictRedis(host=self.host, port=self.port, db=self.db)
        except redis.ConnectionError as e:
            logging.error(f"Failed to connect to Redis: {e}")
            return None

    def _reconnect_if_needed(self):
        if self.redis_client is None:
            logging.info("Attempting to reconnect to Redis...")
            self.redis_client = self._create_redis_client()

    def _format_key(self, key):
        """Convert the key to a string."""
        if isinstance(key, (tuple, list)):
            return str(key)  # Convert tuples/lists to string
        return key

    def _get_from_redis(self, key):
        self._reconnect_if_needed()
        if self.redis_client is None:
            logging.warning("Redis client is not available. Bypassing cache.")
            return None
        try:
            return self.redis_client.get(key)
        except redis.ConnectionError as e:
            raise RuntimeError(f"Redis connection error: {e}")

    def __getitem__(self, key):
        key = self._format_key(key)
        compressed_value = self._get_from_redis(key)
        if compressed_value is None:
            raise KeyError(key)
        return pickle.loads(lz4.frame.decompress(compressed_value))

    def __setitem__(self, key, value):
        key = self._format_key(key)
        pickled_value = pickle.dumps(value)
        compressed_value = lz4.frame.compress(pickled_value)
        self._reconnect_if_needed()
        if self.redis_client is None:
            logging.warning("Redis client is not available. Bypassing cache.")
            return
        try:
            if self.ttl is not None:
                self.redis_client.setex(key, self.ttl, compressed_value)
            else:
                self.redis_client.set(key, compressed_value)
        except redis.ConnectionError as e:
            logging.error(f"Redis connection error: {e}")
            self.redis_client = None  # Set to None to indicate connection issues

    def __delitem__(self, key):
        key = self._format_key(key)
        self._reconnect_if_needed()
        if self.redis_client is None:
            logging.warning("Redis client is not available. Bypassing cache.")
            return
        try:
            self.redis_client.delete(key)
        except redis.ConnectionError as e:
            logging.error(f"Redis connection error: {e}")
            self.redis_client = None  # Set to None to indicate connection issues

    def __contains__(self, key):
        key = self._format_key(key)
        self._reconnect_if_needed()
        if self.redis_client is None:
            logging.warning("Redis client is not available. Bypassing cache.")
            return False
        try:
            return self.redis_client.exists(key)
        except redis.ConnectionError as e:
            logging.error(f"Redis connection error: {e}")
            self.redis_client = None  # Set to None to indicate connection issues
            return False

    def pop(self, key, default=None):
        key = self._format_key(key)
        self._reconnect_if_needed()
        if self.redis_client is None:
            logging.warning("Redis client is not available. Bypassing cache.")
            return default
        try:
            compressed_value = self._get_from_redis(key)
            if compressed_value is None:
                return default
            self.redis_client.delete(key)
            return pickle.loads(lz4.frame.decompress(compressed_value))
        except redis.ConnectionError as e:
            logging.error(f"Redis connection error: {e}")
            self.redis_client = None  # Set to None to indicate connection issues
            return default


def redis_cached(cache, key=None, lock=None):
    """
    Essentially the same as the cachetools cached decorator, but includes
    the function name as an argument to the key function.
    """

    def decorator(func):

        # Cutom key function that prepends the function name
        # to the key being used.
        def namespaced_key(*args, **kwargs):
            if key:
                # If a custom key function is provided, call it with the function name as the first argument
                return key(func.__name__, *args, **kwargs)
            else:
                # Default behavior: use the custom_key function with the function name
                return hashkey(func.__name__, *args, **kwargs)

        @wraps(func)
        @cached(
            cache=cache,
            key=namespaced_key,  # Use the wrapped key function
            lock=lock,
        )
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return decorator
