import math
import time

import flask
import numpy

from datetime import datetime, timedelta, date, timezone
from datetime import time as dttime

from dateutil.parser import parse
from dateutil.tz import tzlocal
from scipy import stats

from . import utils, app


def load_db_data(station, date_from=None, date_to=None, ref_station=None, ref_point=None):
    """
    Load GPS data from the postgresql database

    Either one of ref_station and ref_point must be specified. If both are specified,
    ref_station will be used.

    ARGUMENTS
    ---------
    station: str
        The station to load GPS data for
    date_from: date (optional)
        The date to load GPS data from. If None, load all data
    date_to: date (optional)
        The date to load GPS data to
    ref_station: str (optional)
        The station to reference GPS data against
    ref_point: str (optional)
        A fixed point to reference the GPS data against.

    RETURNS
    -------
    graph_data: dict
        Dictionary containing lists of GPS movement data for the various components.
    """

    if not any([ref_station, ref_point]):
        raise TypeError("At least one of ref_station, ref_point must be specified")

    graph_data = {
        "NS": [],
        "NSE": [],
        "EW": [],
        "EWE": [],
        "UD": [],
        "UDE": [],
        "dates": [],
        "date_obj": [],
        "rapid": [],
    }

    if ref_station:
        g2 = """
        gps_data g2
        ON g1.read_date::date=g2.read_date::date
        WHERE g1.station=%s AND g2.station=%s
        """
    elif ref_point:
        g2 = """
        (SELECT
            0 as dn,
            0 as de,
            0 as dalt,
            0 as sn,
            0 as se,
            0 as su
        ) g2 ON 1=1
        WHERE g1.station=%s
        """
    # offsets are divided by 1000 to convert meters to km
    # this is simply to keep units consistant with the old method,
    # which calulated distance between stations in km rather than change in
    # meters. Might be something to look at changing.
    # Errors are multipled by 100 to convert meters to cm.
    rel_sql = f"""
        SELECT
        g1.read_date::date read_date,
        to_char(g1.read_date,'YYYY-MM-DD') as text_date,
        (g1.dn-g2.dn)/1000::float ns_dist,
        (g1.de-g2.de)/1000::float ew_dist,
        (g1.dalt-g2.dalt)::float ud_dist,
        sqrt(g1.sn^2+g2.sn^2)*100::float as lat_e,
        sqrt(g1.se^2+g2.se^2)*100::float as lon_e,
        sqrt(g1.su^2+g2.su^2)*100::float as alt_e,
        g1.rapid::integer rapid
        FROM
        gps_data g1
        INNER JOIN {g2}
        """

    args = []
    with utils.db_cursor() as cursor:
        cursor.execute("SELECT id FROM stations WHERE name=%s", (station,))
        try:
            station_id = cursor.fetchone()[0]
            args.append(station_id)

            if ref_station is not None:
                cursor.execute("SELECT id FROM stations WHERE name=%s", (ref_station,))
                ref_station_id = cursor.fetchone()[0]
                args.append(ref_station_id)
            else:
                ref_station_id = None

        except TypeError:
            return graph_data  # Empty set, reference station not found.

        if date_from is not None:
            rel_sql += " AND g1.read_date>=%s"
            args.append(date_from)
        if date_to is not None:
            rel_sql += " AND g1.read_date<=%s"
            args.append(date_to)

        rel_sql += """
ORDER BY g1.read_date"""

        SQL_HEADER = (
            "date_obj",
            "dates",
            "NS",
            "EW",
            "UD",
            "NSE",
            "EWE",
            "UDE",
            "rapid",
        )

        cursor.execute(rel_sql, args)

        # Transpose records into component lists.
        for row in cursor:
            for idx, key in enumerate(SQL_HEADER):
                graph_data[key].append(row[idx])

    return graph_data


def _get_vector_data(station, baseline_station, date_from, date_to, zoom_level, scale):
    """
    Calculate GPS movement vectors for a site.

    ARGUMENTS
    ---------
    station: str
        The station to use when determining the "site", or the site itself.
        Will return an empty dataset if not found.
    baseline_station: str
        The baseline station to reference GPS movement against, or '' or 'None' for no baseline.
    date_from: date
        Start date for data of interest
    date_to: date
        End date for data of interest
    zoom_level: int
        Map zoom level (used to determine lat/lon length of vectors)
    scale: int (optional, default 10)
        Scaling factor to shrink/enlarge vectors by

    RETURNS
    -------
    vector_data: dict
        vector data for all stations located at the site.
    """
    from . import mapping

    start = time.time()

    if baseline_station == "None" or baseline_station == 'ITRF14':
        baseline_station = ""

    # Magic numbers to determine the length of the vector and how far to offset it from the
    # station point such that small vectors are not hidden behind the station marker.
    SCALE_FACTOR = (round(4.096e9 * math.exp(-0.69314718056 * zoom_level)) / 10) * scale
    OFFSET = round(2048000 * math.exp(-0.69314718056 * zoom_level))
    CONVERSION_FACTOR = (60 * 60 * 24 * 365) / 1000

    # Determine the site for which to get vectors for all stations.
    # Station can be either station id, station name, or site name.
    try:
        site = utils.stations[station]["site"]
    except KeyError:
        # Site id is not the key here. Try to do a lookup by id
        try:
            site = next(x["site"] for x in utils.stations.values() if x["id"] == station)
        except StopIteration:
            # Couldn't find station by ID either. Maybe this is a site, not a station?
            if station.lower() in (x["site"].lower() for x in utils.stations.values()):
                site = station
            else:
                # Apparently not a site either.
                return {}

    site = site.lower()

    # Get a list of "stations of interest"
    soi = [
        (key, value["id"])
        for key, value in utils.stations.items()
        if value["site"].lower() == site and value["id"] != baseline_station
    ]

    xy_data = []
    z_data = []
    type_data = []
    vector_data = {
        "xy": xy_data,
        "z": z_data,
        "type": type_data,
    }

    if date_from is not None:
        REQ_RANGE = date_to - date_from

    TWO_YEARS = timedelta(days=730.5) - timedelta(
        days=60
    )  # 365.25 days/year, minus two months "wiggle"

    # We want at least half the requested range of data or two years of data,
    # whichever is less, so we can say we have a good representative sample for the station.
    try:
        MIN_RANGE = min(TWO_YEARS, REQ_RANGE / 2)
    except (TypeError, UnboundLocalError):
        MIN_RANGE = TWO_YEARS

    for sta_key, sta in soi:
        lat = utils.stations[sta_key]["lat"]
        lon = utils.stations[sta_key]["lng"]
        sta_type = utils.stations[sta_key]["type"]

        data = mapping.get_graph_data(
            False, sta, baseline_station, date_from=date_from, date_to=date_to
        )

        if date_from is None:
            date_from = data["date_obj"][0]

        try:
            data_range = data["date_obj"][-1] - data["date_obj"][0]
        except IndexError:
            continue  # No data for this station/time range

        if data_range < MIN_RANGE:
            continue  # Not enough data for this station/time period

        timestamps = [datetime.combine(x, dttime()).timestamp() for x in data["date_obj"]]

        # Calculate the slope of each component (in cm/sec - VERY small)
        nsslope, *_ = stats.linregress(timestamps, data["NS"])
        ewslope, *_ = stats.linregress(timestamps, data["EW"])
        udslope, *_ = stats.linregress(timestamps, data["UD"])

        # Convert values to meters/year (so we can divide by meters/degree to get degrees/year)
        # top converts to cm/year, divide by 100 to get meters/year
        nsslope *= CONVERSION_FACTOR
        ewslope *= CONVERSION_FACTOR
        udslope *= CONVERSION_FACTOR

        # Apply the scale factor/offset to the up-down slope. Bearing is 0º North
        udslope = udslope * SCALE_FACTOR
        if udslope < 0:
            udslope -= OFFSET
        else:
            udslope += OFFSET

        # Use the slope of each component as the "magnitude" of said component for the final vector
        # Multiply the resulting vector length by a scaling factor to make differences visible
        # on the google map plot, and add a fixed offset to keep the arrows from getting hidden
        # behind the station markers.
        vis_len = (math.sqrt(nsslope**2 + ewslope**2) * SCALE_FACTOR) + OFFSET
        bearing = math.atan2(nsslope, ewslope)  # from east, in radians.

        # Convert negitive bearings to positive so the math works.
        if bearing < 0:
            bearing += 2 * math.pi

        # Figure out the latitude/longitude change to get the same length
        # Break the legth back into components. Not quite the same as our
        # Original east-west and north-south components due to the added offset
        dx = vis_len * math.cos(bearing)
        dy = vis_len * math.sin(bearing)

        dlon = dx / (111320 * math.cos(math.radians(lat)))
        dlat = dy / 110540

        # Get the start/stop latitude/longitude so we can plot this line on Google Maps
        xy_line = (
            {
                "lat": lat,
                "lng": lon,
            },
            {
                "lat": lat + dlat,
                "lng": lon + dlon,
            },
        )
        xy_data.append(xy_line)

        # up/down movement just extends in the north/south direction. Lon doesn't change
        z_line = (
            {
                "lat": lat,
                "lng": lon,
            },
            {"lat": lat + (udslope / 110540), "lng": lon},
        )

        z_data.append(z_line)
        type_data.append(sta_type)

    scalebar_slope = 0.001  # 5 cm/year
    scalebar_length = scalebar_slope * SCALE_FACTOR  # meters/year/(meters/degree)
    vector_data["scale"] = scalebar_length
    vector_data["date_from"] = date_from.strftime("%m/%d/%y")
    vector_data["date_to"] = date_to.strftime("%m/%d/%y")
    app.logger.info(f"Generated vectors in {time.time()-start} seconds")

    return vector_data


def _get_tilt_data(date_from, date_to, station_name, raw=False):
    """
    Get tilt data for a requested station and date range. Will select
    sub-sampled data for "larger" date ranges to keep performance up.

    ARGUMENTS
    ---------
    date_from: date
        The date from which to retrieve data, or None to get all historical data
    date_to: date
        The date to which to retrieve data, or None to use current date.
    station_name: str
        The station for which to retrieve tilt data
    raw: bool (optional, default: False)
        True to return data as a python dictionary, False to return as JSON encoded string.

    RETURNS
    -------
    tilt_data: dict | string
        The requested tilt data, either as a dictionary or a JSON encoded string,
        depending on the value of the raw argument.
    """
    if date_to is None:
        date_to = datetime.now()

    if date_from is not None:
        drange = date_to - date_from
        days = drange.days

        # Go to just dates for pulling the data
        date_from = date_from.date()
    else:
        days = 368  # Really, anything over 367

    date_to = date_to.date()

    with utils.db_cursor() as cursor:
        cursor.execute("SET TIME ZONE 'UTC'")
        cursor.execute("SELECT id FROM stations WHERE name=%s", (station_name,))
        station = cursor.fetchone()

        if not station:
            return flask.abort(404)

        station = station[0]
        cursor.execute(
            """SELECT
        to_char(min(read_time),'YYYY-MM-DD'),
        to_char(max(read_time),'YYYY-MM-DD'),
        min(read_time)
        FROM tilt_data
        WHERE station=%s""",
            (station,),
        )
        start_date, stop_date, data_start = cursor.fetchone()

        # Figure out how much data to send.
        time_col = "readtime"
        if days >= 367:
            date_range = "all"  # over a year, get hourly data.
            date_from = parse(start_date)
            date_to = date.today()
            target_days = 0
            bucket = "tilt_8_hour"
        elif days > 32:
            date_range = "year"
            target_days = min(365, days * 2)
            bucket = "tilt_1_hour"
        elif days > 0:
            date_range = "month"
            target_days = 32
            bucket = "tilt_5_min_sample"
        else:  # 1 or less days, get full res data
            date_range = "day"
            target_days = 1
            bucket = "tilt_data"
            time_col = "read_time"

        # Extend the date parameters to fall on nice boundries
        if target_days:
            extra_days = target_days - days
            if extra_days > 2:
                end_extend = start_extend = math.floor(extra_days / 2)
                end_remain = (parse(stop_date).date() - date_to).days
                if end_remain < 0:
                    end_remain = 0
                start_remain = (date_from - data_start.date()).days
                if start_remain < 0:
                    start_remain = 0

                if end_remain < end_extend:
                    start_extend += end_extend - end_remain
                    end_extend = end_remain

                # Possible that both are true, in which case the extra will wind up on the end.
                if start_remain < start_extend:
                    end_extend += start_extend - start_remain
                    start_extend = start_remain

                if start_extend > 0:
                    date_from -= timedelta(days=start_extend)
                if end_extend > 0:
                    date_to += timedelta(days=end_extend)

        date_to = datetime.combine(date_to, dttime.max, timezone.utc)
        date_from = datetime.combine(date_from, dttime.min, timezone.utc)

        args = [station, date_to, date_from]

        sql = f"""SELECT
            to_char({time_col},'YYYY-MM-DD"T"HH24:MI:SSZ') as dates,
            tilt_x::float as x_tilt,
            tilt_y::float as y_tilt,
            rot_x::float as rot_x,
            rot_y::float as rot_y,
            date_part('epoch', {time_col}) as timestamps,
            temperature::float
            FROM {bucket}
            WHERE station=%s
            AND {time_col}<=%s
            """
        if date_range != "all":
            sql += f"""AND {time_col}>=%s
                """
        else:
            args.pop()

        sql += f"""ORDER BY {time_col}"""

        # Get orientation changes
        cursor.execute(
            """SELECT
            x_orientation::integer,
            y_orientation::integer
        FROM tilt_orientation
        WHERE station=%s
        ORDER BY seton""",
            (station,),
        )
        orientation_changes = cursor.fetchall()

        t_start = time.time()
        cursor.execute(sql, args)
        app.logger.info(
            f"Queried {cursor.rowcount} rows for {target_days} days in: {time.time()-t_start} "
            "after {t_start-start} seconds"
        )
        # pre-allocate the memory arrays so we don't have to move things around a bunch.
        # TODO: This basically is just transposing records into lists for each column.
        # Investigate if a pandas function could do this more efficently.
        results = {
            "search_start": date_from.strftime("%Y-%m-%dT%H:%M:%S"),
            "search_end": date_to.strftime("%Y-%m-%dT%H:%M:%S"),
            "start_date": start_date,
            "stop_date": stop_date,
            "dates": [0] * cursor.rowcount,
            "x_tilt": [0] * cursor.rowcount,
            "y_tilt": [0] * cursor.rowcount,
            "rot_x": numpy.empty([cursor.rowcount], dtype=float),
            "rot_y": numpy.empty([cursor.rowcount], dtype=float),
            "timestamps": numpy.empty([cursor.rowcount], dtype=float),
            "range": date_range,
            "temps": [0] * cursor.rowcount,
        }

        # Fill the result data structure with the data
        for idx, item in enumerate(cursor):
            results["dates"][idx] = item[0]
            results["x_tilt"][idx] = item[1]
            results["y_tilt"][idx] = item[2]
            results["rot_x"][idx] = item[3]
            results["rot_y"][idx] = item[4]
            results["timestamps"][idx] = item[5]
            results["temps"][idx] = item[6]

        # Get the "average" values for the rotated/corrected x and y to use
        # as a zero point on the polar graph.
        avg_start = datetime.combine(date_from, datetime.min.time())
        avg_start = avg_start.replace(tzinfo=tzlocal())
        if avg_start - timedelta(hours=2) < data_start:
            avg_start = data_start + timedelta(hours=2)
        else:
            avg_start = date_from

        cursor.execute(
            """SELECT
            avg(rot_x)::float as x,
            avg(rot_y)::float as y
        FROM
            (SELECT
                rot_x,
                rot_y
            FROM tilt_data
            WHERE station='c08883c0-fbe5-11e9-bd6e-aec49259cebb'
            AND read_time>=%(start_date)s::timestamp - '2 hours'::interval
            ORDER BY read_time
            LIMIT 120) s1
        """,
            {
                "station": station,
                "start_date": avg_start,
            },
        )

        zero_x, zero_y = cursor.fetchone()

        # Normalize the "timestamps" from 0 to 1 for color scale
        if len(results["dates"]) == 0:
            return flask.abort(404)  # tilt data not found

        # Normalize the timestamps from 0 to 1
        results["timestamps"] -= results["timestamps"][0]  # 0-> some min, since they are ordered
        results["timestamps"] /= results["timestamps"].max()

        # Process vectors for polar plot
        # # Base "0" at first point.
        try:
            results["rot_x"] -= zero_x
            results["rot_y"] -= zero_y
        except TypeError:
            pass  # No zero values found, just use the unadalterated data

        results["radius"] = numpy.sqrt(results["rot_x"] ** 2 + results["rot_y"] ** 2).tolist()

        # Get the angle, converting from 0=north to 0=east coordinates
        results["theta"] = numpy.arctan2(results["rot_y"], results["rot_x"])
        results["theta"][results["theta"] < 0] += 2 * math.pi
        results["theta"] = results["theta"].tolist()

        # Don't need the individual rot_x/rot_y any more
        del results["rot_y"]
        del results["rot_x"]

        # Calculate "lines" for the x/y axis
        max_val = math.ceil(max(results["radius"]))
        step_val = math.ceil(max_val / 100)
        results["x_radius"] = list(range(0, max_val + step_val, step_val))
        results["y_radius"] = results["x_radius"]

        # orientation is relative to north, but the graph is relative to east, so we need to add 90º
        x_orientation = 90 - orientation_changes[-1][0]  # Use the most recent orientation
        if x_orientation > 360:
            x_orientation -= 360
        elif x_orientation < 0:
            x_orientation += 360

        y_orientation = 90 - orientation_changes[-1][1]  # Use the most recent orientation
        if y_orientation > 360:
            y_orientation -= 360
        elif y_orientation < 0:
            y_orientation += 360

        results["x_axis"] = x_orientation
        results["x_theta"] = [x_orientation] * len(results["x_radius"])

        results["y_theta"] = [y_orientation] * len(results["y_radius"])
        results["y_axis"] = y_orientation

        results["timestamps"] = results["timestamps"].tolist()

    if raw:
        return results
    # else/default
    return flask.jsonify(results)


def rotate_ne_rt(n, e, ba):
    """
    Taken from obspy.signal

    Rotates horizontal components of a seismogram.

    The North- and East-Component of a seismogram will be rotated in Radial
    and Transversal Component. The angle is given as the back-azimuth, that is
    defined as the angle measured between the vector pointing from the station
    to the source and the vector pointing from the station to the North.

    :type n: :class:`~numpy.ndarray`
    :param n: Data of the North component of the seismogram.
    :type e: :class:`~numpy.ndarray`
    :param e: Data of the East component of the seismogram.
    :type ba: float
    :param ba: The back azimuth from station to source in degrees.
    :return: Radial and Transversal component of seismogram.
    """
    if len(n) != len(e):
        raise TypeError("North and East component have different length.")
    if ba < 0 or ba > 360:
        raise ValueError("Back Azimuth should be between 0 and 360 degrees.")
    ba = math.radians(ba)
    r = -e * math.sin(ba) - n * math.cos(ba)
    t = -e * math.cos(ba) + n * math.sin(ba)
    return r, t
