import json

import cachetools
import math
import os
import pickle
import shutil
import sqlite3
import time

from contextlib import contextmanager
from collections.abc import Generator

import pandas
import requests

from cachetools.func import ttl_cache, lru_cache
from datetime import timedelta
from xml.etree import ElementTree
from functools import wraps
from io import BytesIO
from pathlib import Path

import flask
import numpy
import osgeo
import PIL
import tempfile

import mattermostdriver
import psycopg2
from dateutil.parser import parse
from psycopg2.extras import RealDictCursor
from psycopg2.extensions import cursor as Cursor

from osgeo import gdal

gdal.DontUseExceptions()

from osgeo_utils.gdal2tiles import main as img2tiles

import pymysql

try:
    from . import wingdbstub
except ImportError:
    pass


# Needs to be declared for decorators before doing other module imports
def h5_file_key(func_name, file, dates=None):
    if not isinstance(file, str):
        file = file.filename

    file = Path(file).name
    return cachetools.keys.hashkey(func_name, file, dates)


try:
    from . import app, SQLITE_DB, config, overlays
except ImportError:
    from __init__ import app, SQLITE_DB
    import config, overlays

from osgeo import osr


def require_admin(func):
    """
    A decorator to be used to require admin priviliges to access a page.
    Must be placed *after* the @app.route decorator to function.
    """

    @wraps(func)
    def inner(*args, **kwargs):
        # This isn't much of a check at the moment, but could be easily
        # expanded to be whatever, and do whatever if the user is not admin.
        # For example, check for a SSO token or cookie, and if not found
        # redirect to a login page.
        if flask.session.get("public", True):
            return flask.abort(401)

        return func(*args, **kwargs)

    return inner


class MYSQLCursor:
    def __init__(self, DB, user=config.GDDB_USER, password=config.GDDB_PASS):
        self._conn = None
        self._db = DB
        self._user = user
        self._pass = password
        self._server = config.GDDB_HOST

    def __enter__(self) -> pymysql.cursors.Cursor:
        self._conn = pymysql.connect(
            user=self._user, password=self._pass, database=self._db, host=self._server
        )
        return self._conn.cursor()

    def __exit__(self, *args, **kwargs):
        self._conn.rollback()
        self._conn.close()


VOLCANOES = {}
VOLC_IDS = {}
BY_PARENT = None


@ttl_cache(ttl=86400)  # Cache for a day
def get_volcs():
    global BY_PARENT
    with MYSQLCursor(DB="geodiva") as cursor:
        cursor.execute(
            """SELECT volcano_id, volcano_parent_id, volcano_name,latitude,longitude
            FROM volcano
            WHERE observatory='avo'"""
        )

        volcs = pandas.DataFrame(
            cursor, columns=['id', 'parent_id', 'name', 'latitude', 'longitude']
        )

    parent_mask = volcs['id'] == volcs['parent_id']
    parent_volcs = volcs[parent_mask]
    BY_PARENT = volcs.set_index('parent_id').sort_index()

    parent_ids = parent_volcs.set_index('name')['id'].to_dict()
    VOLC_IDS.update(parent_ids)
    loc_by_name = parent_volcs.set_index('name')[['latitude', 'longitude']]
    loc_by_name = loc_by_name.apply(list, axis=1).to_dict()
    VOLCANOES.update(loc_by_name)

    return volcs


class DBSession:
    """Global session object implemented as a SQLLite DB so it can be shared
    across processes Works like a dict, mostly, but still needs more methods
    implemented to flesh it out.
    """

    def __init__(self, sid):
        self._sid = sid

    def __getitem__(self, key):
        value = self.get(key)
        if value is None:
            raise KeyError

        return value

    def __setitem__(self, key, value):
        value = pickle.dumps(value)
        cur = self.conn.execute(
            "SELECT 1 FROM session_info WHERE sid=? AND key=?", (self._sid, key)
        )
        exists = cur.fetchone()

        args = (value, self._sid, key)
        if not exists:
            self.conn.execute("INSERT INTO session_info (value, sid, key) VALUES (?,?,?)", args)
        else:
            self.conn.execute("UPDATE session_info SET value=? WHERE sid=? AND key=?", args)

        self.conn.commit()

    def get(self, key, default=None):
        cur = self.conn.execute(
            "SELECT value FROM session_info WHERE sid=? AND key=?", (self._sid, key)
        )
        val = cur.fetchone()

        if val:
            val = pickle.loads(val[0])
        else:
            val = default

        return val

    def __enter__(self):
        self.conn = sqlite3.connect(SQLITE_DB)
        return self

    def __exit__(self, exc_type=None, exc_val=None, exc_tb=None):
        self.conn.commit()
        self.conn.close()


def getDiskValue(key, default=None, sid=None):
    if sid is None:
        sid = flask.session.get("_sid")

    if sid is None:
        app.logger.error("NO SID FOUND IN SESSION!")
        return None
    with DBSession(sid) as session:
        value = session.get(key, default)

    return value


def setDiskValue(key, value, sid=None):
    if sid is None:
        sid = flask.session.get("_sid")

    if sid is None:
        app.logger.error("NO SID FOUND IN SESSION!")
        return None

    with DBSession(sid) as f:
        f[key] = value


@contextmanager
def db_cursor(cursor_factory=None) -> Generator[Cursor, None, None]:
    """
    Context manager for creating a database cursor.

    Parameters
    ----------
    cursor_factory : Optional[type]
        A factory function for creating cursor objects. Defaults to None.

    Yields
    ------
    psycopg2.extensions.cursor
        A cursor object for executing database operations.

    Example
    -------
    >>> with db_cursor() as cursor:
    ...     cursor.execute("SELECT * FROM your_table")
    ...     result = cursor.fetchall()
    ...     print(result)
    """
    conn = psycopg2.connect(
        host=config.PSQL_HOST,
        database=config.PSQL_DB,
        cursor_factory=cursor_factory,
        user=config.PSQL_USER,
        password=config.PSQL_PASS,
        connect_timeout=3,  # It really should connect quickly, or never
    )
    cursor = conn.cursor()
    try:
        yield cursor
    finally:
        try:
            conn.rollback()
        except AttributeError:
            pass  # No connection
        conn.close()


def load_seismic_stations():
    URL = "https://volcanoes.usgs.gov/vsc/api/instrumentApi/data?lat1=-90&lat2=90&long1=-180&long2=180"
    res = requests.get(URL)
    if res.status_code != 200:
        return {}

    ret_json = res.json()
    # Pandas might be overkill for this, but it makes it easy.
    instruments = pandas.DataFrame(ret_json['instruments'])
    categories = pandas.DataFrame(ret_json['categories'])
    seismo_id = categories[categories['category'] == 'Seismometer'].iloc[0]['catId']
    seismometers = instruments[instruments['catId'] == seismo_id].copy()
    seismometers.loc[:, 'sensor_type'] = 'seismic'
    seismometers.loc[:, 'has_tilt'] = True
    seismometers.loc[:, 'disp_name'] = seismometers['station']
    seismometers.loc[:, 'idx'] = seismometers['station'] + '_SEIS'
    seismometers.loc[:, 'site'] = seismometers['site'].fillna('Unknown')

    cols = [
        'disp_name',
        'lat',
        'long',
        'sensor_type',
        'station',
        'site',
        'has_tilt',
        'stationId',
        'idx',
    ]
    seismometers = seismometers[cols]
    seismometers = seismometers.rename(
        columns={
            'long': 'lng',
            'sensor_type': 'type',
            'station': 'id',
            'stationId': 'sta_id',
        }
    )
    seismometers = seismometers.set_index(['idx'])
    result = seismometers.to_dict(orient="index")

    return result


def load_stations(max_age=10):
    # Load station list from DB
    SQL = """
    SELECT
        disp_name,
        t1.latitude::float as lat,
        t1.longitude::float as lng,
        type,
        t1.name as id,
        coalesce(sites.name, 'Unknown') as site,
        coalesce((SELECT true
                FROM tilt_data
                INNER JOIN tilt_orientation
                ON tilt_data.station=tilt_orientation.station
                WHERE tilt_data.station=t1.id
                LIMIT 1),
                false) as has_tilt,
        t1.id as sta_id
    FROM stations t1
    LEFT JOIN sites ON t1.siteref=sites.id
    WHERE EXISTS (SELECT 1
                FROM gps_data
                WHERE station=t1.id
                AND read_date>now()-%s
                LIMIT 1);
    """

    try:
        max_age = timedelta(days=365.25 * max_age)
    except (TypeError, ValueError):
        max_age = timedelta(days=365.25 * 100)  # just some large random time in the past

    try:
        with db_cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(SQL, (max_age,))
            stas = {row["disp_name"]: dict(row) for row in cursor}
    except Exception as e:
        app.logger.exception(f"Unable to load stations from db. Error: {e}")
        return {}

    seismic = load_seismic_stations()
    stas.update(seismic)

    return stas


def load_locations():
    SQL = """
    SELECT
        name,
        lat as center_lat,
        lon as center_lon,
        zoom,
        (SELECT array_agg(s.name ORDER BY t.ord)
         FROM stations s
         JOIN unnest( baselines ) WITH ORDINALITY t(id,ord)
         USING (id)
        ) as baselines
    FROM sites
    ORDER BY name
    """

    try:
        with db_cursor(RealDictCursor) as cursor:
            cursor.execute(SQL)
            locations = {x.pop("name"): dict(x) for x in cursor}
    except psycopg2.OperationalError:
        locations = {}

    return locations


class FetchingDict(dict):
    def load(self):
        self.update(load_stations(None))

    def __getitem__(self, key):
        if not self:
            print("Loading stations from DB")
            self.update(load_stations(None))
        return super().__getitem__(key)


stations = FetchingDict()
station_volcs = {}


def load_station_volcs():
    app.logger.info("Calculating station volcs")
    if not stations:
        stations.load()

    for station, sta_info in stations.items():
        volc = point_to_volc(sta_info['lat'], sta_info['lng'])
        opts = BY_PARENT.loc[[VOLC_IDS[volc]]]
        opts = opts.dropna(subset=['latitude', 'longitude'])
        result = {
            row['name']: json.dumps([row['latitude'], row['longitude']])
            for _, row in opts.iterrows()
        }
        station_volcs[station] = result


def get_extents(src, proj: str = None, target_proj: str = None) -> list:
    """
    Get the bounds of a file in proj coordinates

    ARGUMENTS
    ---------
    src: gdal dataset
        The source file, as an opened gdal dataset
    proj: str, optional
        The projection of the source image. Retrieved from source if not
        specified
    target_proj: str, optional
        The projection to return coordinates in. Same as source if not
        specified.

    RETURNS
    -------
    bounds: list
        the bounds of the file, as [minX, minY, maxX, maxY]
    """
    ulx, xres, xskew, uly, yskew, yres = src.GetGeoTransform()
    lrx = ulx + (src.RasterXSize * xres)
    lry = uly + (src.RasterYSize * yres)

    src_srs = osr.SpatialReference()
    src_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    if proj is not None:
        epsg_code = int(proj.replace("EPSG:", ""))
        src_srs.ImportFromEPSG(epsg_code)
    else:
        src_srs.ImportFromWkt(src.GetProjection())

    tgt_srs = src_srs.CloneGeogCS()
    if target_proj is not None:
        epsg_code = int(target_proj.replace("EPSG:", ""))
        tgt_srs.ImportFromEPSG(epsg_code)

    transform = osr.CoordinateTransformation(src_srs, tgt_srs)
    # top-left, top-right,bottom-right,bottom-left
    corners = ((ulx, uly), (lrx, uly), (lrx, lry), (ulx, lry))
    trans_corners = transform.TransformPoints(corners)

    ulx, uly, _ = trans_corners[0]
    urx, ury, _ = trans_corners[1]
    lrx, lry, _ = trans_corners[2]
    llx, lly, _ = trans_corners[3]

    # figure out which X is to the left.
    # Make both upper and lower coordinates
    # negitive for easy comparison
    comp_upper = ulx
    comp_lower = llx

    if comp_upper > 0:
        comp_upper -= 360

    if comp_lower > 0:
        comp_lower -= 360

    if comp_upper < comp_lower:
        minx = ulx
    else:
        minx = llx

    comp_upper = urx
    comp_lower = lrx
    if comp_upper > 0:
        comp_upper -= 360

    if comp_lower > 0:
        comp_lower -= 360

    if comp_upper > comp_lower:
        maxx = urx
    else:
        maxx = lrx

    miny = min(lly, lry)
    maxy = max(uly, ury)

    if minx > maxx:
        minx -= 360

    return [minx, miny, maxx, maxy]


def haversine_np(lon1, lat1, lon2, lat2) -> numpy.ndarray:
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)

    Works with both numpy arrays and scalars, or a mix - lon/lat 1
    can be numpy arrays, while lon/lat 2 are scaler values, and it will
    calculate the distance from lon/lat 2 to each point in the lon/lat 1
    arrays.

    Less precise than vincenty, but fine for short distances,
    and works on vector math

    """
    lon1, lat1, lon2, lat2 = map(numpy.radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = (
        numpy.sin(dlat / 2.0) ** 2
        + numpy.cos(lat1) * numpy.cos(lat2) * numpy.sin(dlon / 2.0) ** 2
    )

    c = 2 * numpy.arcsin(numpy.sqrt(a))
    km = 6367 * c
    return km


def png_to_tiff(png, bounds):
    """Convert a PNG into a GeoTIFF

    ARGUMENTS
    ----------
    png: str or file
        The png file to be converted, either as a file-like object or path to
        the file.
    bounds: list or tuple
        The bounds of the image, in [minx, maxy, maxx, miny] (top-left,
        lower-right) format

    RETURNS
    -------
    tiff: BytesIO
        The GeoTIFF, as a BytesIO object
    """

    with tempfile.TemporaryDirectory() as tempDir:
        png_path = os.path.join(tempDir, "img.png")
        geotiff_path = os.path.join(tempDir, "img.tiff")
        try:
            png.seek(0)
        except AttributeError:
            pass  # Hopefully a file path, which will still work below.

        png_image = PIL.Image.open(png)
        if png_image.mode == "RGBA":
            img_data = numpy.array(png_image)
            # black_pixels = (img_data == [0, 0, 0, 255]).all(axis=2)
            # if black_pixels.any():
            #    img_data[black_pixels] = [1, 1, 1, 255] #almost black.

            png_image = PIL.Image.fromarray(img_data).convert(
                "RGB"
            )  # Drop the alpha channel, if any
        png_image.save(png_path)

        # Make sure we have registered drivers
        osgeo.gdal.AllRegister()

        # convert png to a geotiff
        dsIn = osgeo.gdal.Open(png_path)
        osgeo.gdal.Translate(
            geotiff_path,
            dsIn,
            outputBounds=bounds,
            outputSRS="EPSG:4326",
            creationOptions="ALPHA=YES",
        )

        with open(geotiff_path, "rb") as tiff_file:
            geotiff = BytesIO(tiff_file.read())

    geotiff.seek(0)
    return geotiff


@lru_cache()
def get_tiff_location(geotiff: str, tgt_proj: int = None) -> dict:
    # Get the image bounds, in lat/lon projection
    ds = osgeo.gdal.Open(geotiff)
    corner_dict = osgeo.gdal.Info(ds, format="json")["cornerCoordinates"]

    src_srs = osr.SpatialReference()
    src_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    src_srs.ImportFromWkt(ds.GetProjection())

    tgt_srs = src_srs.CloneGeogCS()

    if tgt_proj is not None:
        tgt_srs.ImportFromEPSG(tgt_proj)
    else:
        tgt_srs.ImportFromEPSG(4326)

    transform = osr.CoordinateTransformation(src_srs, tgt_srs)

    # top-left, top-right,bottom-right,bottom-left
    corners = (
        (corner_dict["upperLeft"][0], corner_dict["upperLeft"][1]),
        (corner_dict["upperRight"][0], corner_dict["upperRight"][1]),
        (corner_dict["lowerRight"][0], corner_dict["lowerRight"][1]),
        (corner_dict["lowerLeft"][0], corner_dict["lowerLeft"][1]),
    )

    trans_corners = transform.TransformPoints(corners)

    ulx, uly, _ = trans_corners[0]
    urx, ury, _ = trans_corners[1]
    lrx, lry, _ = trans_corners[2]
    llx, lly, _ = trans_corners[3]

    #  Make sure all longitudes are negitive so they plot properly in leaflet
    if ulx > 0:
        ulx -= 360
    if urx > 0:
        urx -= 360
    if lrx > 0:
        lrx -= 360
    if llx > 0:
        llx -= 360

    ds = None
    del ds

    location = {
        "SW": {
            "lat": round(lly, 4),
            "lng": round(llx, 4),
        },
        "NE": {
            "lat": round(ury, 4),
            "lng": round(urx, 4),
        },
    }

    return location


def tiff_to_png(geotiff: str) -> BytesIO:
    """
    Convert a geoTIFF to png format

    ARGUMENTS
    ---------
    geotiff: str
        The path to the geotiff file to be converted

    RETURNS
    -------
    img: BytesIO
        The image, in png format
    """
    t_start = time.time()

    osgeo.gdal.DontUseExceptions()

    # Make sure we are in the correct projection
    ds = osgeo.gdal.Open(geotiff)

    src_srs = osr.SpatialReference()
    src_srs.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    src_srs.ImportFromWkt(ds.GetProjection())

    kwargs = {
        "dstSRS": "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs +over +lon_wrap=-180",
        "multithread": True,
        "warpOptions": ["NUM_THREADS=ALL_CPUS"],
        "creationOptions": ["NUM_THREADS=ALL_CPUS"],
        "srcSRS": src_srs,
    }

    ds = osgeo.gdal.Warp("/vsimem/in_memory_output.tif", geotiff, **kwargs)

    print("Warped image in", time.time() - t_start)

    band = ds.GetRasterBand(1)
    dmin, dmax, _, _ = band.ComputeStatistics(False)
    nodata = band.GetNoDataValue()

    png_opts = {
        "outputType": osgeo.gdalconst.GDT_Byte,
        "scaleParams": [[dmin, dmax, 0, 65535]],
        "noData": nodata,
        "bandList": [1, 1, 1],
    }

    mem_file_path = "/vsimem/pngimage.png"
    osgeo.gdal.Translate(mem_file_path, ds, **png_opts)

    print("Converted to PNG in", time.time() - t_start)
    ds = None

    stats = osgeo.gdal.VSIStatL(mem_file_path, osgeo.gdal.VSI_STAT_SIZE_FLAG)
    mem_file = osgeo.gdal.VSIFOpenL(mem_file_path, "rb")

    img_file = BytesIO()
    img_file.write(osgeo.gdal.VSIFReadL(1, stats.size, mem_file))
    osgeo.gdal.VSIFCloseL(mem_file)

    img_file.seek(0)

    print("Read file back in in", time.time() - t_start)
    return img_file


def process_form_sql(SQL, args=None):
    if args is None:
        args = dict(flask.request.form)

    for arg, value in args.items():
        if value == "":
            args[arg] = None

    with db_cursor() as cursor:
        cursor.execute(SQL, args)
        record_id = cursor.fetchone()
        cursor.connection.commit()

    if record_id is not None:
        record_id = record_id[0]

    return record_id


def pathFrameSort(x):
    """Sort by the path/frame"""
    if isinstance(x, (tuple, list)):
        x = x[0]

    split_values = x.split()
    try:
        path = int(split_values[1])
        frame = int(split_values[3])
    except (IndexError, ValueError):
        return x

    return (path, frame)


def has_overlay_hierarchy(directory_path):
    """
    Function to check if a directory contains at least one subdirectory structure
    matching the specific hierarchy:
    - Contains one or more "type" subdirectories
    - Each "type" subdirectory contains one or more "orbit" subdirectories
    - Each "orbit" subdirectory contains one or more "date" subdirectories
    - Each "date" subdirectory contains one or more "object" (files or folders)

    An overlay directory will match this pattern (otherwise my code would reject it),
    so we can use to filter a list of directories to only ones containing overlays.
    """
    if not directory_path.is_dir():
        return False

    # Check for at least one "type" directory
    type_dirs = list(directory_path.glob("*/"))
    if not type_dirs:
        return False

    for type_dir in type_dirs:
        if not type_dir.is_dir():
            continue

        # Check for at least one "orbit" directory
        orbit_dirs = list(type_dir.glob("*/"))
        if not orbit_dirs:
            continue

        for orbit_dir in orbit_dirs:
            if not orbit_dir.is_dir():
                continue

            # Check for at least one "date" directory
            date_dirs = list(orbit_dir.glob("*/"))
            if not date_dirs:
                continue

            for date_dir in date_dirs:
                if not date_dir.is_dir():
                    continue

                # Check for at least one "object" (file or folder)
                objects = list(date_dir.glob("*"))
                if not objects:
                    continue

                return True  # Found at least one matching hierarchy

    return False  # Did not find any matching hierarchy


def process_tiff(tiff_in, tiff_out, proj=None):
    gdal.AllRegister()
    ds = gdal.Open(tiff_in)

    band = ds.GetRasterBand(1)
    band.ComputeStatistics(False)
    max_val = band.GetMaximum()
    min_val = band.GetMinimum()

    scale_param = None

    expand_opt = None
    bandlist_opt = None if ds.RasterCount > 1 else [1, 1, 1]
    if (
        ds.RasterCount == 1
        and band.GetColorInterpretation() == gdal.GCI_PaletteIndex
        and band.GetColorTable() is not None
    ):
        expand_opt = "RGB"
        bandlist_opt = None
    elif gdal.GetDataTypeName(band.DataType) == "Float32":
        scale_param = [(min_val, max_val, 0, 65535)]

    opts = gdal.TranslateOptions(
        outputType=gdal.GDT_Byte,
        noData=0,
        scaleParams=scale_param,
        rgbExpand=expand_opt,
        bandList=bandlist_opt,
    )

    scaled_ds = "/vsimem/scaled_image.tiff"
    gdal.Translate(scaled_ds, ds, options=opts)

    if proj is None:
        proj = "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=-180 +x_0=0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext +no_defs"

    warp_opts = gdal.WarpOptions(
        dstSRS=proj,
        multithread=True,
        warpOptions=["NUM_THREADS=ALL_CPUS"],
        creationOptions=["NUM_THREADS=ALL_CPUS"],
    )

    ds_out = gdal.Warp(tiff_out, scaled_ds, options=warp_opts)

    ds = None
    try:
        gdal.Unlink(scaled_ds)
    except RuntimeError:
        pass

    return ds_out


def tiff_to_tiles(tiff: str, meta: dict, tempdir):
    try:
        img_file = meta["filepath"]
        clean_file = os.path.join(tempdir.name, "sar_image_clean.tiff")
        web_dir = overlays.make_overlay_path(
            meta["sensor"], meta["type"], meta["identifier"], meta["date"]
        )

        os.makedirs(web_dir, exist_ok=True)

        tile_dest = web_dir / Path(img_file).stem.replace(".", "_")
        if tile_dest.is_dir():
            try:
                shutil.rmtree(str(tile_dest))
            except OSError:
                # Sometimes I get a "directory not empty" error, which makes no sense given that
                # this is supposed to delete the directory and contents. Trying again often works.
                shutil.rmtree(str(tile_dest))

        img_bounds = get_tiff_location(img_file)

        ds_out = process_tiff(img_file, clean_file)
        if meta.get('post', True):
            format_mm_post(tempdir, ds_out, meta, img_bounds)

        tile_files = [clean_file]
        #  See if we need to create two images rather than just one (crossing the dateline)
        transform = ds_out.GetGeoTransform()
        min_longitude = transform[0]
        max_longitude = transform[0] + transform[1] * ds_out.RasterXSize

        #  We are in a mercator projection, with the antimeridian at 0, so
        #  this logic should properly indicate if we cross.
        if min_longitude < 0 and max_longitude > 0:
            print("Crossing! Need to split!")
            dateline_pixel = int((0 - transform[0]) / transform[1])
            y_size = ds_out.RasterYSize
            left_options = gdal.TranslateOptions(srcWin=[0, 0, dateline_pixel, y_size])
            right_options = gdal.TranslateOptions(
                srcWin=[
                    dateline_pixel + 1,
                    0,
                    ds_out.RasterXSize - dateline_pixel,
                    y_size,
                ]
            )

            left_file = os.path.join(tempdir.name, "left.tiff")
            right_file = os.path.join(tempdir.name, "right.tiff")

            gdal.Translate(left_file, ds_out, options=left_options)
            gdal.Translate(right_file, ds_out, options=right_options)
            tile_files = [left_file, right_file]

        ds_out = None

        # Figure out a reasonable max zoom level
        pixelSizeX = transform[1]

        # Constants
        equator_length = 40075016.686
        pixels_per_tile = 256
        optimal_zoom_level = math.log((equator_length / pixels_per_tile) / pixelSizeX, 2)

        # Make sure we aren't aiming for too high a zoom level.
        # Aim for a couple of levels higher than "ideal"
        max_zoom_level = min(math.ceil(optimal_zoom_level) + 2, 17)

        # Use a minumum "max" zoom level though, so we at least have a few options
        max_zoom_level = max(max_zoom_level, 14)
        zoom = f"6-{max_zoom_level}"

        tile_dir = os.path.join(tempdir.name, "mapTiles")

        cpu_count = os.cpu_count()

        for in_file in tile_files:
            tiles_argv = [
                "SAR.py",
                "-z",
                zoom,
                "-x",
                "-e",
                "-w",
                "none",
                f"--processes={cpu_count}",
                in_file,
                tile_dir,
            ]

            try:
                img2tiles(tiles_argv, called_from_main=True)
            except TypeError:
                __spec__ = None
                img2tiles(tiles_argv)

        full_tiff = os.path.join(tile_dir, "sar_image.tif")
        shutil.copy(img_file, full_tiff)

        if len(tile_files) > 1:
            #  Fix the metadata xml file
            meta_xml = Path(tile_dir) / "tilemapresource.xml"

            kml_tree = ElementTree.parse(meta_xml)
            kml_root = kml_tree.getroot()
            bounding_box = kml_root.find("BoundingBox")
            minx = img_bounds["SW"]["lng"]
            maxx = img_bounds["NE"]["lng"]

            # Make values "normal" for this
            if minx < -180:
                minx += 360
            if maxx < -180:
                maxx += 360

            bounding_box.attrib["maxx"] = str(maxx)
            bounding_box.attrib["minx"] = str(minx)

            kml_tree.write(meta_xml)

        shutil.move(tile_dir, tile_dest)
        app.logger.info("Tile directory creation complete")
    finally:
        tempdir.cleanup()


def connect_to_mattermost():
    mattermost = mattermostdriver.Driver(
        {
            "url": config.MATTERMOST_URL,
            "token": config.MATTERMOST_TOKEN,
            "port": config.MATTERMOST_PORT,
        }
    )

    mattermost.login()
    channel_id = mattermost.channels.get_channel_by_name_and_team_name(
        config.MATTERMOST_TEAM, config.MATTERMOST_CHANNEL
    )["id"]
    return (mattermost, channel_id)


def mm_upload(message, meta, image=None):

    connection, channel_id = connect_to_mattermost()

    post_payload = {
        "channel_id": channel_id,
    }

    # First, upload the thumbnail, if any
    if image:
        img_date = meta['dateOBJ'].strftime('%Y%m%d %H:%M')
        try:
            img_date += f"_{meta['dateOBJ2'].strftime('%Y%m%d %H:%M')}"
        except KeyError:
            pass

        img_name = f"sar_{meta['sensor']}_{meta['identifier']}_{img_date}.png"

        with open(image, "rb") as img:
            upload_result = connection.files.upload_file(
                channel_id=channel_id, files={"files": (img_name, img)}
            )

        matt_id = upload_result["file_infos"][0]["id"]
        post_payload["file_ids"] = [matt_id]

    if message:
        post_payload["message"] = message

    connection.posts.create_post(post_payload)


TYPES = {
    'amp': 'Amplitude',
    'ifr': 'Interferogram',
}


def format_mm_post(file_dir, ds, meta, bounds):
    volc = bounds_to_volc(bounds)
    # if date is a range, use start date
    date_parts = meta["date"].split('_')
    date_obj = parse(date_parts[0])
    message_date = date_obj.strftime('%Y-%m-%d')

    meta["dateOBJ"] = date_obj

    if len(date_parts) > 1:
        date2_obj = parse(date_parts[1])
        meta['dateOBJ2'] = date2_obj
        message_date += f" - {date2_obj.strftime('%Y-%m-%d')}"

    link = (
        f"https://apps.avo.alaska.edu/geodesy/map?overlay={meta['identifier']}"
        f"&type={meta['type']}&sensor={meta['sensor']}"
        f"&dfrom={date_obj.strftime('%Y-%m-%d')}"
    )

    message = f"""### New SAR overlay uploaded

**Closest Volc:** {volc}
**Type:** {TYPES[meta["type"]]}
**Sensor:** {meta["sensor"]}
**Identifier:** {meta["identifier"]}
**Date:** {message_date}

**Web Link:** [View in web interface]({link})
"""
    # Convert tiff to png
    png_file = str(Path(file_dir.name) / "clean.png")
    driver = gdal.GetDriverByName("png")
    if driver:
        driver.CreateCopy(png_file, ds, 0)

    max_size = (1024, 768)
    with PIL.Image.open(png_file) as png:
        png.thumbnail(max_size, resample=PIL.Image.LANCZOS)
        png.save(png_file)

    mm_upload(message, meta, png_file)


def bounds_to_volc(bounds):
    center_lat = sum([bounds["SW"]["lat"], bounds["NE"]["lat"]]) / 2
    center_lon = sum([bounds["SW"]["lng"], bounds["NE"]["lng"]]) / 2
    if center_lon < -180:
        center_lon += 360

    return point_to_volc(center_lat, center_lon)


def point_to_volc(center_lat, center_lon):
    if not VOLCANOES or len(BY_PARENT) == 0:
        get_volcs()

    volc_coords = numpy.asarray(list(VOLCANOES.values()))
    volc_lats = volc_coords[:, 0]
    volc_lons = volc_coords[:, 1]
    distances = haversine_np(center_lon, center_lat, volc_lons, volc_lats)

    min_idx = numpy.argmin(distances)
    volc_name = list(VOLCANOES.keys())[min_idx]
    return volc_name


########################################
def wgs2xyz(lat, lon, alt):
    """
    Converts Latitude, Longitude, and Altitude (LLA) to Earth-Centered, Earth-Fixed (ECEF) coordinates.

    :param lat: Latitude in degrees
    :param lon: Longitude in degrees
    :param alt: Altitude in meters
    :return: (X, Y, Z) in meters
    """
    # WGS84 ellipsoid constants
    a = 6378137.0  # Semi-major axis (meters)
    e2 = 0.00669437999014  # First eccentricity squared

    # Convert degrees to radians
    lat = numpy.radians(lat)
    lon = numpy.radians(lon)

    # Compute prime vertical radius of curvature
    N = a / numpy.sqrt(1 - e2 * numpy.sin(lat) ** 2)

    # Compute ECEF coordinates
    X = (N + alt) * numpy.cos(lat) * numpy.cos(lon)
    Y = (N + alt) * numpy.cos(lat) * numpy.sin(lon)
    Z = ((1 - e2) * N + alt) * numpy.sin(lat)

    return X, Y, Z


def calc_neu_vel(lat, lon, height, plate):
    X, Y, Z = wgs2xyz(lat, lon, height)

    plate = plate.lower()

    #
    #     This table is from Argus et al. (2010)
    #     Units of angular velocity (omegaX,Y,Z) are in rads per million years
    #     Units of covariance matrix are 10^-10 rads^2 per million yr^2
    #
    #     plate    omegaX      omegaY     omegaZ
    #     anta -0.00115899 -0.00152263  0.00328637
    #     arab  0.00741655  0.00194326  0.00847657
    #     aust  0.00724980  0.00560919  0.00583401
    #     eura -0.00053595 -0.00244848  0.00359444
    #     noam  0.00027649 -0.00337936 -0.00018938
    #     indi  0.00580027  0.00130548  0.00720402
    #     nazc -0.00147409 -0.00766879  0.00801227
    #     nubi  0.00044618 -0.00282666  0.00345576
    #     pcfc -0.00196175  0.00498483 -0.01060919
    #     soam -0.00123752 -0.00141244 -0.00064365
    #     soma -0.00035868 -0.00341258  0.00441288
    #
    # Geocenter correction for ITRF2008 and covariance
    #  Sigmas on geocenter rounded up to 0.2 mm/yr (from 0.05)
    #
    #     geocenter_xyz = 0.001*[ 0.17; 0.26; -1.04 ];
    #     geocenter_cov = (0.001^2)*[ 0.04 0.0 0.0 ; 0.0 0.04 0.0 ; 0.0 0.0 0.04 ];
    #
    # Geocenter correction for ITRF2005 and covariance
    #
    #     geocenter_xyz = 0.001*[ 0.08; 0.27; -1.12 ];
    #     geocenter_cov = (0.001^2)*[ .056 -.002 -.020 ; -.002 .120 .009 ; -.020 .009 .092 ];
    #
    #     geocenter = 0.001*[ 0.08; 0.27; -1.12 ];
    #     geocov = (0.001^2)*[ .056 -.002 -.020 ; -.002 .120 .009 ; -.020 .009 .092 ];
    #
    #
    #     plate omegaX   omegaY  omegaZ
    #                 a       b        c       d       e       f
    #
    #

    pvel = {}

    pvel['anta'] = numpy.array([-0.00115899, -0.00152263, 0.00328637])
    pvel['arab'] = numpy.array([0.00741655, 0.00194326, 0.00847657])
    pvel['aust'] = numpy.array([0.00724980, 0.00560919, 0.00583401])
    pvel['eura'] = numpy.array([-0.00053595, -0.00244848, 0.00359444])
    pvel['noam'] = numpy.array([0.00027649, -0.00337936, -0.00018938])
    pvel['indi'] = numpy.array([0.00580027, 0.00130548, 0.00720402])
    pvel['nazc'] = numpy.array([-0.00147409, -0.00766879, 0.00801227])
    pvel['nubi'] = numpy.array([0.00044618, -0.00282666, 0.00345576])
    pvel['pcfc'] = numpy.array([-0.00196175, 0.00498483, -0.01060919])
    pvel['soam'] = numpy.array([-0.00123752, -0.00141244, -0.00064365])
    pvel['soma'] = numpy.array([-0.00035868, -0.00341258, 0.00441288])

    # Calculate plate velocity at XYZ
    #
    # This code uses the matrix version of the cross product:
    #                         |  0  z -y | | omx |
    #          om (cross) r = | -z  0  x | | omy | = Ri * om
    #                         |  y -x  0 | | omz |
    #
    # ===============================

    R = numpy.matrix([[0, Z, -Y], [-Z, 0, X], [Y, -X, 0]])

    # convert from rad/Myr to mm/yr
    xyz_vel = R.dot(pvel[plate]) * 1e-6 * 1000  # * math.pi/180 #* 6378137

    # convert to local enu velocity
    # ===============================
    lat = math.radians(lat)
    lon = math.radians(lon)

    R = numpy.matrix(
        [
            [-math.sin(lat) * math.cos(lon), -math.sin(lat) * math.sin(lon), math.cos(lat)],
            [-math.sin(lon), math.cos(lon), 0],
            [math.cos(lat) * math.cos(lon), math.cos(lat) * math.sin(lon), math.sin(lat)],
        ]
    )

    neu_vel = R.dot(xyz_vel.T)

    return neu_vel
