FROM python:3.9-bookworm
RUN apt-get update\
  && apt-get -y install \
  postgresql \
  libgdal-dev \
  gdal-bin \
  python3-gdal \
  libgl1-mesa-glx \
  gmt-gshhg \
  gmt \
  libgmt-dev \
  ghostscript 

RUN mkdir -p /app/geodesy
WORKDIR /app/geodesy
COPY ./requirements.txt .
RUN pip install --upgrade pip wheel
RUN pip install $(grep -ivE "gdal" requirements.txt)
RUN pip install gdal==`gdal-config --version`
ENV PYTHONUNBUFFERED 1
COPY ./src/ .